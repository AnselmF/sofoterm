========
Sofoterm
========

This is a web-based community event calendar.

It has been in use for Heidelberg's sofo-hd.de calendar for more than a
decade and *could* relatively easily be generalised to work for other
cities (or whatever), too, but it is enough work that we'll only do
it if there are people who'd like to actually deploy it elsewhere.

Just contact red@sofo-hd.de if you are interested.

Selling points would include:

* Based on twisted and sqlite, it's trivial to deploy (at least on
  Debian)
* Cool, pure-XML templates
* Knows about RSS and iCal
* Time-proven design
* Written in Python with test coverage in the 90ies (generally:-)

See also the docs folder (files there currently in German; don't worry,
we'd translate them; comments in the code are in English).


Slight hack alert: sofoterm was originally based on nevow.  That was
never (properly) ported to python3, so these days it's using plain
twisted web, but with a shim trying to preserve some nice points of
nevow and in particular this HTML form management library formal.  This
isn't properly packaged (yet); we instead pull it from a subversion VCS.
After a clone, do ``./pull-formal`` (which only works if you have
subversion installed; sorry about that).  We'll work with that project's
upstream for a less insane setup (it looked fine when this was still in
subversion rather than git).
