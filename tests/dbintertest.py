"""
Tests for our silly DB interface.
"""

import sqlite3
import tempfile
import time

from sofoterm.model import dbinter

import testhelpers


class ConnectionTest(testhelpers.VerboseTest):
	def testCommitting(self):
		conn = dbinter.getConnection(":memory:")
		conn.execute("CREATE TABLE foo (x INTEGER)")
		conn.commit()

		with conn.autoCursor() as curs:
			curs.execute("INSERT INTO foo (x) VALUES (3)")
		self.assertEqual(
			set(r[0] for r in conn.execute("SELECT x FROM foo")),
			{3})

		try:
			with conn.autoCursor() as curs:
				curs.execute("INSERT INTO foo (x) VALUES (4)")
				curs.execute("INSERT INTO notexiting (x) VALUES (4)")
		except sqlite3.OperationalError:
			# expected.
			pass

		self.assertEqual( # insert of 4 must be rolled back.
			set(r[0] for r in conn.execute("SELECT x FROM foo")),
			{3})

	def testVersionMismatch(self):
		with tempfile.NamedTemporaryFile() as f:
			# create environment including the version
			conn = dbinter.getConnection(f.name)
			conn.commit()
			conn.close()

			try:
				dbinter.MODEL_VERSION += 1
				self.assertRaisesWithMsg(dbinter._VersionMismatch,
					"5",
					dbinter.getConnection,
					(f.name, True))
			finally:
				dbinter.MODEL_VERSION -= 1

	def testCacheClearing(self):
		conn = dbinter.getConnection(":memory:")
		conn.stconf.setKeyValue("dumpDir", "/whereever")
		self.assertEqual(conn.stconf.dumpDir , "/whereever")
		self.assertEqual(
			list(conn.execute("select * from config where key='dumpDir'")),
			[('dumpDir', '/whereever')])
		conn.execute(
			"update config set value='/elsewhere' where key='dumpDir'")
		self.assertEqual(conn.stconf.dumpDir , "/whereever")

		conn._processReload(conn)
		self.assertEqual(conn.stconf.dumpDir , "/elsewhere")
		self.assertTrue(conn.lastUpdate-time.time()<1)


class TableDefTest(testhelpers.VerboseTest):
	def _getFooTable(self):
		class FooTable(dbinter.TableDef):
			c_000_id = "INTEGER PRIMARY KEY"
			c_010_bdate = "DATE"
		return FooTable

	def testSimpleTableDef(self):
		table = self._getFooTable()("foo")
		self.assertEqual(table.schema, 
			[('id', 'INTEGER PRIMARY KEY'), ('bdate', 'DATE')])
	
	def testInheritance(self):
		FooTable = self._getFooTable()
		class BarTable(FooTable):
			c_005_comment = "TEXT"
		self.assertEqual(BarTable("bar").schema, [
			('id', 'INTEGER PRIMARY KEY'), 
			('comment', 'TEXT'), 
			('bdate', 'DATE')])

	def testBuildStatements(self):
		FooTable = self._getFooTable()
		class BarTable(FooTable):
			pc_000_silly = ("CREATE INDEX IF NOT EXISTS %(name)s_bdate"
				" ON %(name)s (bdate)")
		self.assertEqual(list(BarTable("bar").iterBuildStatements()), [
			'CREATE TABLE IF NOT EXISTS bar (id INTEGER PRIMARY KEY, bdate DATE)', 
			'CREATE INDEX IF NOT EXISTS bar_bdate ON bar (bdate)'])


class UpdateTest(testhelpers.VerboseTest):
	"""tests for working update mechanism.
	"""
	def testUpdateSelection(self):
		from sofoterm.model import updates
		class TooOld(updates.Updater):
			version = 2
			u_000_destroy = "NUKE ALL"
		class JustRight(updates.Updater):
			version = 3
			u_000_frob = "FROB TABLE events"
			u_100_nicate = "NICATE TABLE events"
		class JustRightToo(updates.Updater):
			version = 4
			u_000_yikes = "PREPARE FOR 3"
		class TooNew(updates.Updater):
			version = 5
			u_000_kill = "APPLY niebel TO africa"
		toRun = list(updates.getUpdateStatements(2, 4,
			[TooNew, JustRightToo, JustRight, TooOld]))
		self.assertEqual(toRun,
			['FROB TABLE events', 'NICATE TABLE events', 'PREPARE FOR 3'])


if __name__=="__main__":
	testhelpers.main(TableDefTest)
