"""
Tests exercising the CLI more or less directly.
"""

import io
import tempfile
from xml.etree import ElementTree as ET
from sofoterm import cli

import res_testserver
import testhelpers


_CONN = res_testserver.SERVER.conn


class CLITest(testhelpers.VerboseTest):
	"""tests for running the admin cli interface.
	"""
	def testHelp(self):
		self.assertOutput(cli.main, ["help"], expectedStdout=
			lambda output: b"-- outputs help to stdout" in output)

	def testSponsors(self):
		self.assertOutput(cli.main, ["add", "sponsor", "bugs", "bunny"],
			expectedStdout=lambda output: b"2 item(s) changed" in output)
		self.assertOutput(cli.main, ["show", "sponsor"],
			lambda output: b"bugs" in output and b"bunny" in output)
		self.assertOutput(cli.main, ["remove", "sponsor", "bugs", "bunny"],
			expectedStdout=lambda output: b"2 item(s) changed" in output)
		self.assertOutput(cli.main, ["show", "sponsor"],
			lambda output: not b"bugs" in output and not b"bunny" in output)

	def testTags(self):
		self.assertOutput(cli.main, ["add", "tag", "bugs", "bunny"],
			expectedStdout=lambda output: b"2 item(s) changed" in output)
		self.assertOutput(cli.main, ["show", "tag"],
			lambda output: b"bugs" in output and b"bunny" in output)
		self.assertOutput(cli.main, ["remove", "tag", "bugs", "bunny"],
			expectedStdout=lambda output: b"2 item(s) changed" in output)
		self.assertOutput(cli.main, ["show", "tag"],
			lambda output: not b"bugs" in output and not b"bunny" in output)

	def testSetAdd(self):
		self.assertOutput(cli.main, ["set", "testval", "a"],
			expectedStdout="")
		self.assertOutput(cli.main, ["show", "testval"],
			expectedStdout="a\n")
		self.assertOutput(cli.main, ["add", "testval", "b"],
			expectedStdout="1 item(s) changed\n")
		self.assertOutput(cli.main, ["show", "testval"],
			expectedStdout="a\nb\n")
		self.assertOutput(cli.main, ["rmAll", "testval"],
			expectedStdout="2 item(s) deleted\n")
		self.assertOutput(cli.main, ["show", "testval"],
			expectedStdout="\n")

	def testDump(self):
		def test(res):
			# this should raise for malformed xml...
			ET.fromstring(res)
			return True
		self.assertOutput(cli.main, ["dump"],
			expectedStdout=test)

_FAT_TEMPLATE = """uniList = model.EventList.fromSelectors(conn,
  startDate=base.dateHence(0), endDate=base.dateHence(10),
  unwantedTags=("wahlen",))
gremienList = model.EventList.fromSelectors(conn,
  startDate=base.dateHence(0), endDate=base.dateHence(23),
  wantedTags=("geheim",), unwantedTags=("wahlen",))
wahlenList = model.EventList.fromSelectors(conn,
  startDate=base.dateHence(0), endDate=base.dateHence(23),
  wantedTags=("wahlen",))
if not uniList and not gremienList:
  raise SendNoMail()


From: Studierendenrat <stura@stura.uni-heidelberg.de>
To: diskussion@stura.uni-heidelberg.de, fsen@stura.uni-heidelberg.de, graus@stura.uni-heidelberg.de, schlagzeiler@stura.uni-heidelberg.de, senat@stura.uni-heidelberg.de
Date: ${rfcdate}
Subject: Terminschlagzeiler vom $!datetime.datetime.now().strftime("%d.%m.%Y")!

Liebe Aktive und Interessierte,

nachfolgend erhaltet ihr eine Übersicht über hochschul- und
bildungspolitisch interessante Termine bis Ende kommender Woche sowie
die Gremien- und Wahltermine der nächsten Zeit.  Die verschickten
Termine findet ihr in fortlaufend aktualisierter Form auch auf der
StuRa-Homepage http://www.stura.uni-heidelberg.de/ unter "Termine" --
ansonsten stehen dort natürlich auch aktuelle Meldungen und Berichte.

Wer Termine eingeben will: hier findet ihr eine Anleitung:
https://www.stura.uni-heidelberg.de/wp-content/uploads/Anleitungen/SoFo-Leitfaden.pdf

1. Termine von und für Studis in den nächsten 10 Tagen

Vorträge, Arbeits- und Vorbereitungstreffen, öffentliche Sitzungen.

Bitte teilt uns grobe Fehler mit und toleriert kleinere. Eine Übersicht über
mehr Termine in der Region findet ihr hier: http://www.sofo-hd.de

$!model.textifyEventList(conn, uniList) or "(Keine bekannt -- wo sind  eure Termine?)"!

2. Gremientermine der nächsten drei Wochen

Um der Vollständigkeit näher zu kommen, bitten wir euch, uns
Gremientermine mitzuteilen. Das erleichtert euch und Mitgliedern anderer
Gremien die Planung und Koordinierung.

$!model.textifyEventList(conn, gremienList) or "(Keine  Gremiensitzungen absehbar)"!

3. Wahlen in der nächsten Zeit

Folgende Wahlen stehen an:

$!model.textifyEventList(conn, wahlenList) or "(Keine)"!

Die Auflistung einer Veranstaltung ist nicht gleichbedeutend mit einer
ideellen Unterstützung durch die Verfasste Studierendenschaft.
"""

class MailFormattingTest(testhelpers.VerboseTest):
	"""tests for mail templating and such.
	"""
	def testDefaultTemplate(self):
		dest = io.BytesIO()
		cli.cmd_mail(_CONN, {"template": None, "options": []}, dest)
		toPipe = dest.getvalue()
		decoded = toPipe.decode("ascii")  # transport should be 7-bit-clean

		self.assertHasStrings(decoded, [
			'Content-Type: text/plain; charset="utf-8"',
			'Subject: Termine 14.02.2010 bis 24.02.2010',
			'>>> Mittwoch, 17.02.2010',
			'jedeR wei=C3=9F, wo)',  # utf-8 quoted printable?
			'>>> Freitag, 19.02.2010 <<<', # all events covered?
			'Aktionswoche', # Mutiday covered?
			])

	def testExternalTemplate(self):
		with tempfile.NamedTemporaryFile() as f:
			f.write(_FAT_TEMPLATE.encode("utf-8"))
			f.flush()
			dest = io.BytesIO()
			cli.cmd_mail(_CONN, {"template": f.name, "options": []}, dest)

		decoded = dest.getvalue().decode("utf-8")
		self.assertHasStrings(decoded, [
			'Wir gedenken',  # basic events
			'Folgende Wahlen stehen an:\n\n(Keine)',  # format nulls
			'Geheimtreffen\n  http://localhost:8888/event/7', # tag selection
			])


class FulltextTest(testhelpers.VerboseTest):
	def testReindex(self):
		self.assertOutput(cli.main, ["reindex"], expectedStdout="")
		_CONN.execute(
			"INSERT INTO event_index(event_index, rank) VALUES('integrity-check', 1)")
		_CONN.getArchiveConnection().execute(
			"INSERT INTO event_index(event_index, rank) VALUES('integrity-check', 1)")


if __name__=="__main__":
	testhelpers.main(MailFormattingTest)
