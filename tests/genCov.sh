#!/bin/sh

COVERAGE=python3-coverage

# We're always appending, so clean away the previous run
rm -f .coverage *.cov

export COVERAGE_FILE=basic.cov
export COVERAGE_OPTIONS="-a"
$COVERAGE run --source .. runAllTests.py
echo "Running regression tests"
python3 regression.py

export -n COVERAGE_FILE
$COVERAGE combine *.cov
$COVERAGE report
python3-coverage html -i
echo "As html in htmlcov."
