"""
Helper functions and classes for our tests.
"""

import contextlib
import gc
import inspect
import os
import pickle
import re
import subprocess
import sys
import traceback
import unittest

try:
	import coverage
except ImportError:
	# we only need this if someone set COVERAGE_FILE; don't do that
	# if you don't have coverage.py installed.
	pass

import testresources
from testresources import TestResource, ResourcedTestCase

# Here's the deal on TestResource: When setting up complicated stuff for
# tests (like, a DB table), define a TestResource for it.  Override
# the make(deps) method returning something and the clean() method to destroy
# whatever you created in make(deps).
# Return the concrete object you want to make.
#
# Then, in VerboseTests, have a class attribute
# resource = [(name1, res1), (name2, res2)]
# giving attribute names and resource *instances*.
# 
# If you use this and you have a setUp of your own, you *must* call 
# the superclass's setUp method.

from sofoterm.base import common
common.setupDebug()


def bytify(s):
	if isinstance(s, str):
		return s.encode("utf-8")
	return s


class ForkingSubprocess(subprocess.Popen):
	"""A subprocess that doesn't exec but fork.
	"""
	def _execute_child(self, args, executable, preexec_fn, close_fds,
							 pass_fds, cwd, env,
							 startupinfo, creationflags, shell,
							 p2cread, p2cwrite,
							 c2pread, c2pwrite,
							 errread, errwrite,
							 restore_signals, 
               # post-3.7, some additional parameters were added, which
               # fortunately we don't need.
							 *ignored_args):
# stolen largely from 2.7 subprocess.  Unfortunately, I can't just override the
# exec action; so, I'm replacing the the whole exec shebang with a simple
# call to executable().
#
# Also, I'm doing some extra hack to collect coverage info if it looks
# like we're collecting coverage.
#
# The signature (and a few inside parts) is from 3.7 subprocess.  We're
# ignoring everything that 2.7 hasn't known under the bold assumption that our
# simple testing forks that doesn't matter (which ought to be true for
# start_new_session at least:-).

		sys.argv = args
		if executable is None:
				executable = args[0]

		def _close_in_parent(fd):
				os.close(fd)

		# For transferring possible exec failure from child to parent.
		# Data format: "exception name:hex errno:description"
		# Pickle is not used; it is complex and involves memory allocation.
		errpipe_read, errpipe_write = os.pipe()

		try:
						gc_was_enabled = gc.isenabled()
						# Disable gc to avoid bug where gc -> file_dealloc ->
						# write to stderr -> hang.  http://bugs.python.org/issue1336
						gc.disable()
						try:
								self.pid = os.fork()
						except:
								if gc_was_enabled:
										gc.enable()
								raise
						self._child_created = True
						if self.pid == 0:
								# Child
								try:
										# Close parent's pipe ends
										if p2cwrite!=-1:
												os.close(p2cwrite)
										if c2pread!=-1:
												os.close(c2pread)
										if errread!=-1:
												os.close(errread)
										os.close(errpipe_read)

										# When duping fds, if there arises a situation
										# where one of the fds is either 0, 1 or 2, it
										# is possible that it is overwritten (#12607).
										if c2pwrite == 0:
												c2pwrite = os.dup(c2pwrite)
										if errwrite == 0 or errwrite == 1:
												errwrite = os.dup(errwrite)

										# Dup fds for child
										def _dup2(a, b):
												# dup2() removes the CLOEXEC flag but
												# we must do it ourselves if dup2()
												# would be a no-op (issue #10806).
												if a == b:
														self._set_cloexec_flag(a, False)
												elif a!=-1:
														os.dup2(a, b)
										_dup2(p2cread, 0)
										_dup2(c2pwrite, 1)
										_dup2(errwrite, 2)

										# Close pipe fds.  Make sure we don't close the
										# same fd more than once, or standard fds.
										closed = set([-1])
										for fd in [p2cread, c2pwrite, errwrite]:
												if fd not in closed and fd > 2:
														os.close(fd)
														closed.add(fd)

										if cwd is not None:
												os.chdir(cwd)

										if "COVERAGE_FILE" in os.environ:
											os.environ["COVERAGE_FILE"] = "forked.cov"
											cov = coverage.Coverage(
												source=[".."],
												auto_data=True)
											cov.config.disable_warnings = ["module-not-measured"]
											cov.start()

										if preexec_fn:
												preexec_fn()

										exitcode = 0

								except:
										exc_type, exc_value, tb = sys.exc_info()
										# Save the traceback and attach it to the exception object
										exc_lines = traceback.format_exception(exc_type,
																													 exc_value,
																													 tb)
										exc_value.child_traceback = ''.join(exc_lines)
										os.write(errpipe_write, pickle.dumps(exc_value))
										os.close(errpipe_write)
										os._exit(255)

								os.close(errpipe_write)
								try:
									executable()
								except SystemExit as ex:
									exitcode = ex.code

								if "COVERAGE_FILE" in os.environ:
									cov.stop()
									cov.save()
								sys.stderr.close()
								sys.stdout.close()
								os._exit(exitcode)


						# Parent
						os.close(errpipe_write)
						if gc_was_enabled:
								gc.enable()

						# Now wait for the child to come up (at which point it will
						# close its error pipe)
						errpipe_data = bytearray()
						while True:
								part = os.read(errpipe_read, 50000)
								errpipe_data += part
								if not part or len(errpipe_data)>50000:
									break
								if errpipe_data:
									child_exception = pickle.loads(errpipe_data)
									raise child_exception

		finally:
				# close the FDs used by the child if they were opened
				if p2cread!=-1 and p2cwrite!=-1:
						_close_in_parent(p2cread)
				if c2pwrite!=-1 and c2pread!=-1:
						_close_in_parent(c2pwrite)
				if errwrite!=-1 and errread!=-1:
						_close_in_parent(errwrite)

				# be sure the error pipe FD is eventually no matter what
				os.close(errpipe_read)


class VerboseTest(ResourcedTestCase):
	"""A TestCase with a couple of convenient assert methods.
	"""
	def assertEqualForArgs(self, callable, result, *args):
		self.assertEqual(callable(*args), result, 
			"Failed for arguments %s.  Expected result is: %s, result found"
			" was: %s"%(str(args), repr(result), repr(callable(*args))))

	def assertRaisesVerbose(self, exception, callable, args, msg):
		try:
			callable(*args)
		except exception:
			return
		except:
			raise
		else:
			raise self.failureException(msg)

	def assertRaisesWithMsg(self, exception, errMsg, callable, args, msg=None):
		try:
			callable(*args)
		except exception as ex:
			if errMsg!=str(ex):
				raise self.failureException(
					"Expected %r, got %r as exception message"%(errMsg, str(ex)))
		except:
			raise
		else:
			raise self.failureException(msg or "%s not raised"%exception)

	def assertRuns(self, callable, args, msg=None):
		try:
			callable(*args)
		except Exception as ex:
			raise self.failureException("Should run, but raises %s (%s) exception"%(
				ex.__class__.__name__, str(ex)))

	def assertAlmostEqualVector(self, first, second, places=7, msg=None):
		try:
			for f, s in zip(first, second):
				self.assertAlmostEqual(f, s, places)
		except AssertionError:
			if msg:
				raise AssertionError(msg)
			else:
				raise AssertionError("%s != %s within %d places"%(
					first, second, places))

	def assertHasStrings(self, result, expected):
		for ex in expected:
			try:
				self.assertTrue(ex in result, "'%s' not in result.data"%ex)
			except AssertionError:
				with open("result.data", "w") as f:
					f.write(result)
				raise

	def assertOutput(self, toExec, argList, expectedStdout=None, 
			expectedStderr="", expectedRetcode=0, input=None,
			stdoutStrings=None):
		"""checks that execName called with argList has the given output and return
		value.

		expectedStdout and expectedStderr can be functions.  In that case,
		the output is passed to the function, and an assertionError is raised
		if the functions do not return true.

		The 0th argument in argList is automatically added, only pass "real"
		command line arguments.

		toExec may also be a zero-argument python function.  In that case, the
		process is forked and the function is called, with sys.argv according to
		argList.  This helps to save startup time for python main functions.
		"""
		for name in ["output.stderr", "output.stdout"]:
			try:
				os.unlink(name)
			except os.error:
				pass

		if isinstance(toExec, str):
			p = subprocess.Popen([toExec]+argList, executable=toExec, 
				stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
		else:
			p = ForkingSubprocess(["test harness"]+argList, executable=toExec, 
				stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
		out, err = p.communicate(input=bytify(input))
		retcode = p.wait()

		try:
			
			if isinstance(expectedStderr, (bytes, str)):
				self.assertEqual(err, bytify(expectedStderr))
			elif isinstance(expectedStderr, list):
				for sample in expectedStderr:
					self.assertTrue(bytify(sample) in err,
						"%s missing in stderr"%repr(sample))
			else:
				self.assertTrue(expectedStderr(err), "Stderr didn't match functional"
					" expectation: %s"%err.decode("ascii", "?"))

			self.assertEqual(expectedRetcode, retcode)
		except AssertionError:
			with open("output.stdout", "wb") as f:
				f.write(out)
			with open("output.stderr", "wb") as f:
				f.write(err)
			raise

		try:
			if isinstance(expectedStdout, (bytes, str)):
				self.assertEqual(out, bytify(expectedStdout))
			elif isinstance(expectedStdout, list):
				for sample in expectedStdout:
					self.assertTrue(bytify(sample) in out,
						"%s missing in stdout"%repr(sample))
			elif expectedStdout is not None:
				self.assertTrue(expectedStdout(out))
		except AssertionError:
			with open("output.stdout", "wb") as f:
				f.write(out)
			raise


class SamplesBasedAutoTest(type):
	"""A metaclass that builds tests out of a samples attribute of a class.

	To use this, give the class a samples attribute containing a sequence
	of anything, and a _runTest(sample) method receiving one item of
	that sequence.

	The metaclass will create one test<n> method for each sample.
	"""
	def __new__(cls, name, bases, dict):
		for sampInd, sample in enumerate(dict.get("samples", ())):
			def testFun(self, sample=sample):
				self._runTest(sample)
			dict["test%02d"%sampInd] = testFun
		return type.__new__(cls, name, bases, dict)


class EqualingRE(object):
	"""A value that compares equal based on RE matches.
	"""
	def __init__(self, pattern):
		self.pat = re.compile(pattern)
	
	def __eq__(self, other):
		return bool(self.pat.match(other))

	def __hash__(self):
		return hash(self.pat)

	def __ne__(self, other):
		return not self.__eq__(other)
	
	def __str__(self):
		return "<Pattern %s>"%self.pat.pattern

	__repr__ = __str__


@contextlib.contextmanager
def testFile(name, 
		content, 
		writeGz=False, 
		inDir="/tmp",
		timestamp=None):
	"""a context manager that creates a file name with content in inDir.

	The full path name is returned.

	content must be bytes.

	With writeGz=True, content is gzipped on the fly (don't do this if
	the data already is gzipped).

	You can pass in name=None to get a temporary file name if you don't care
	about the name.

	inDir will be created as a side effect if it doesn't exist but (right
	now, at least), not be removed.
	"""
	if not os.path.isdir(inDir):
		os.makedirs(inDir)

	if name is None:
		handle, destName = tempfile.mkstemp(dir=inDir)
		os.close(handle)
	else:
		destName = os.path.join(inDir, name)

	if writeGz:
		f = gzip.GzipFile(destName, mode="wb")
	else:
		f = open(destName, "wb")

	f.write(content)
	f.close()
	
	if timestamp:
		os.utime(destName, times=(timestamp, timestamp))

	try:
		yield destName
	finally:
		try:
			os.unlink(destName)
		except os.error:
			pass


def main(testClass, methodPrefix=None):
	if len(sys.argv)>2:
		testClass = inspect.stack()[1][0].f_globals[sys.argv[-2]]
	if len(sys.argv)>1 or methodPrefix:
		suite = unittest.makeSuite(testClass, methodPrefix or sys.argv[-1],
			suiteClass=testresources.OptimisingTestSuite)
		runner = unittest.TextTestRunner()
		runner.run(suite)
	else:
		unittest.main()
