"""
Runs all known tests.

This script *asssumes* it is run from tests subdirectory of the code
tree and silently won't work (properly) otherwise.

We're running normal unit tests from *test.py and trial-based tests
called test_*.py.
"""

import doctest
import glob
import os
import subprocess
import sys
import unittest
import warnings

import res_testserver

warnings.simplefilter("ignore", category=UserWarning)

def runTrialTests():
	"""runs trial-based tests, suppressing output, but raising an error if
	any of the tests failed.
	"""
	os.environ["PYTHONPATH"] = os.getcwd()
	args = ["-m", "twisted.trial", "--reporter", "text", "test_pages"] 

	if "COVERAGE_FILE" in os.environ:
		os.environ["COVERAGE_FILE"] = "trial.cov"
		executor = ["python3-coverage", "run", "--source", ".."]
	else:
		executor = ["python3"]

	subprocess.call(executor+args)


def _getPluginTests(rootPage):
	from sofoterm.pages import plugin

	loader = unittest.TestLoader()
	suite = unittest.TestSuite()
	for name, ns in plugin.getPlugins().items():
		if hasattr(ns, "getTests"):
			suiteNames = ns.getTests(rootPage)
			for t in [suiteNames[name] for name in suiteNames 
				if isinstance(suiteNames[name], type) 
					and issubclass(suiteNames[name], unittest.TestCase)]:
				suite.addTests(loader.loadTestsFromTestCase(t))
	return suite


def hasDoctest(fName):
	with open(fName, "rb") as f:
		return b"doctest.testmod" in f.read()


def getDoctests():
	doctests = []
	for dir, dirs, names in os.walk("../sofoterm"):
		parts = dir.split("/")[1:]
		for name in [n for n in names if n.endswith(".py")]:
			try:
				if hasDoctest(os.path.join(dir, name)):
					name = ".".join(parts+[name[:-3]])
					doctests.append(doctest.DocTestSuite(name))
			except Exception:
				sys.stderr.write("*** While collecting doctests from %s:\n\n"%
					os.path.join(dir, name))
				raise
	return unittest.TestSuite(doctests)


if __name__=="__main__":
	unittestSuite = unittest.defaultTestLoader.loadTestsFromNames(
		[n[:-3] for n in glob.glob("*test.py")])
	unittestSuite = unittest.TestSuite([unittestSuite, getDoctests()])

	runner = unittest.TextTestRunner()
	runner.run(unittest.TestSuite([unittestSuite,
		_getPluginTests(res_testserver.SERVER)]))
	
	print("\nTrial-based tests:")
	runTrialTests()
