# -*- coding: utf-8 -*-
"""
Tests not fitting anywhere else.
"""

import datetime
import os
import tempfile
import time

import testhelpers

import sofoterm
from sofoterm import base
from sofoterm.pages import pg_search
from sofoterm.pages import common as rsccommon
from sofoterm.pages.common import addMonths


class BackupTest(testhelpers.VerboseTest):
	def testBasic(self):
		with tempfile.TemporaryDirectory() as td:
			origname = td+"/basic.txt"
			testContent = "testing 1,2,3\n"
			with open(origname, "w") as f:
				f.write(testContent)
			base.backup(origname)

			with open(origname+".000") as f:
				self.assertEqual(f.read(), testContent)

			testContent = "testing 4,5,6\n"
			with open(origname, "w") as f:
				f.write(testContent)
			base.backup(origname)

			with open(origname+".001") as f:
				self.assertEqual(f.read(), testContent)

	def testNonExisting(self):
		with tempfile.TemporaryDirectory() as td:
			origname = td+"/basic.txt"
			base.backup(origname)
			# no exception raised is enough: no file, no backup
	
	def testWriteProblem(self):
		with tempfile.TemporaryDirectory() as td:
			origname = td+"/basic.txt"
			testContent = "testing 1,2,3\n"
			with open(origname, "w") as f:
				f.write(testContent)

			try:
				os.chmod(td, 0o000)
				self.assertRaisesWithMsg(PermissionError,
					testhelpers.EqualingRE(
						r"\[Errno 13\] Permission denied: '.*/basic.txt'"),
					base.backup,
					(origname,))
			finally:
				os.chmod(td, 0o700)


class RSTCompileTest(testhelpers.VerboseTest):
	"""tests compilation of RST to HTML.
	"""
	def testTrivial(self):
		res = sofoterm.compileRSTX("A few lines\nof RSTX.  What do you think?")
		self.assertEqual(res,
			"<p>A few lines\nof RSTX.  What do you think?</p>\n")
	
	def testWarning(self):
		self.assertRaisesWithMsg(sofoterm.RSTError,
			'Line 3: Definition list ends without a blank line;'
			' unexpected unindent.',
			sofoterm.compileRSTX,
			("Bombing\n * out.\nnow",))

	def testError(self):
		self.assertRaisesWithMsg(sofoterm.RSTError,
			"Line 7: Title level inconsistent: /  / DDD / '''",
			sofoterm.compileRSTX,
			("AAA\n===\nBBB\n---\nCCC\n===\nDDD\n'''",))
	
	def testNoInsertion(self):
		self.assertRaisesWithMsg(sofoterm.RSTError,
			'Line 1: "include" directive disabled.',
			sofoterm.compileRSTX,
			(".. include:: Iambad.doc",))


D = datetime.datetime
DD = datetime.date

class NthWeekdayTest(testhelpers.VerboseTest, metaclass=testhelpers.SamplesBasedAutoTest):
	def _runTest(self, sample):
		args, expected = sample
		self.assertEqual(rsccommon.getNthWeekday(*args), expected)
	
	samples = [
		((2017, 8, 0, 1), DD(2017, 8, 7)),
		((2017, 8, 1, 1), DD(2017, 8, 1)),
		((2017, 8, 1, 2), DD(2017, 8, 8)),
		((2017, 8, 4, 4), DD(2017, 8, 25)),
		((2017, 8, 4, -1), DD(2017, 8, 25)),
		((2017, 8, 4, -2), DD(2017, 8, 18)),
	]


class DateListTest(testhelpers.VerboseTest,
		metaclass=testhelpers.SamplesBasedAutoTest):
	"""tests for parsing of freeform date/time lists.
	"""

	def _runTest(self, sample):
		spec, expected = sample
		self.assertEqual(rsccommon.parseDateList(spec, self.initDT), expected)
	
	initDT = D(2010, 2, 14, 20, 00)

	samples = [(
			'17', [D(2010, 2, 17, 20, 0)]
		), (
			'23.\n24', [D(2010, 2, 23, 20, 0), D(2010, 2, 24, 20, 0)]
		), (
			'23\n17', [D(2010, 2, 23, 20, 0), D(2010, 3, 17, 20, 0)]
		), (
			'23, 19\n 23', [D(2010, 2, 23, 19, 0), D(2010, 2, 23, 19, 0)]
		), (
			'Montag\n Montag\n Dienstag\nDienstag', [
				D(2010, 2, 15, 20, 0),
				D(2010, 2, 22, 20, 0),
				D(2010, 2, 23, 20, 0),
				D(2010, 3, 2, 20, 0)]
		), (
			'Montag\n Montag, 13:00\n Dienstag, 17:30\nDienstag', [
				D(2010, 2, 15, 20, 0),
				D(2010, 2, 22, 13, 0),
				D(2010, 2, 23, 17, 30),
				D(2010, 3, 2, 17, 30)]
		), ("Montag*3\n Montag*4\nMontag*4\nMontag*5\nFreitag*1, 10:00", [
				D(2010, 2, 15, 20, 0),
				D(2010, 2, 22, 20, 0),
				D(2010, 3, 22, 20, 0),
				D(2010, 3, 29, 20, 0),
				D(2010, 4, 2, 10, 0)]
		), ("31.12.\n\n13.1.", [
				D(2010, 12, 31, 20, 0),
				D(2011, 1, 13, 20, 0)]),
		]


class DateListErrorTest(testhelpers.VerboseTest, metaclass=testhelpers.SamplesBasedAutoTest):
	def _runTest(self, sample):
		spec, ex_msg = sample
		self.assertRaisesWithMsg(
			ValueError,
			ex_msg,
			rsccommon.parseDateList,
			(spec, self.initDT))

	initDT = D(2010, 2, 14, 20, 00)

	samples = [(
		'Pfintsta*3',
		'Fehler in Datumsliste, Zeile 1: Kein bekannter Wochentag in *-Syntax'
		), (
		'Montag*0',
		'Fehler in Datumsliste, Zeile 1:'
		' Es gibt keine 0-ten Wochentage in Monaten.'
		), (
		'Dienstag*6',
		'Fehler in Datumsliste, Zeile 1:'
		' Es gibt keine 6-ten Wochentage in Monaten.'
		), (
		'Mittwoch*-60',
		'Fehler in Datumsliste, Zeile 1:'
		' Es gibt keine -60-ten Wochentage in Monaten.'
		), (
		'Montag+1\nDienstag*0',
		'Fehler in Datumsliste, Zeile 2: Es gibt keine 0-ten Wochentage in Monaten.'), (
		'Montag+1\nDas ist Quatsch',
		'Fehler in Datumsliste, Zeile 2: Kein Datum zu erkennen.'),
	]


class HTTPTimeFormatTest(testhelpers.VerboseTest):
	"""tests for HTTP formatting functions.
	"""
	def testFormat(self):
		self.assertEqual(
			base.formatHTTPDate(884901010.0),
			'Thu, 15 Jan 1998 21:50:10 GMT')
	
	def testParseGMT(self):
		self.assertEqual(base.parseHTTPDate('Sat, 29 Oct 1994 19:43:31 -0000'),
			783459811.0)

	def testParseNonGMT(self):
		self.assertEqual(base.parseHTTPDate('Sat, 29 Oct 1994 18:43:31 -0100'),
			783459811.0)

	def testRoundtrip1(self):
		now = base.now(utc=True).timestamp()
		fmt = base.formatHTTPDate(now)
		self.assertEqual(base.parseHTTPDate(fmt), now)

	def testRoundtrip2(self):
		self.assertEqual(base.formatHTTPDate(base.parseHTTPDate(
			'Mon, 01 Dec 2008 14:12:02 GMT')),
			'Mon, 01 Dec 2008 14:12:02 GMT')

	def testCurrent(self):
		aboutNow = base.parseHTTPDate(base.formatHTTPDate())
		self.assertTrue(time.time()-aboutNow<1)


class DateHenceTest(testhelpers.VerboseTest):
	def testLocal(self):
		self.assertEqual(base.dateHence(4), datetime.date(2010, 2, 18))

	def testUTC(self):
		self.assertEqual(base.dateHence(4, True), datetime.date(2010, 2, 18))


class MonthsAlgebraTest(testhelpers.VerboseTest, metaclass=testhelpers.SamplesBasedAutoTest):
	def _runTest(self, sample):
		ym, diff, expected = sample
		self.assertEqual(addMonths(ym[0], ym[1], diff), expected)
	
	samples = [
		((2010, 5), 0, (2010, 5)),
		((2010, 1), 1, (2010, 2)),
		((2010, 1), -1, (2009, 12)),
		((2009, 12), 1, (2010, 1)),
		((2009, 12), 12, (2010, 12)),
		((2009, 12), 13, (2011, 1)),
		((2009, 12), -12, (2008, 12)),
	]


if __name__=="__main__":
	testhelpers.main(DateHenceTest)
