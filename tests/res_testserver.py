# -*- coding: utf-8 -*-
"""
A test server.

This module creates a test server (writing into a memory data base)
on import.

This is used here since testresources and trial don't mix naturally and
I'm too lazy to make them fit.
"""

import datetime
import tempfile

from twisted.web import server
from twisted.internet import reactor

import testhelpers # sets up the date and time of the tests (Feb 2010)

import sofoterm
from sofoterm import base
from sofoterm import model
from sofoterm.base import _, common
from sofoterm.formal import twistedpatch
from sofoterm.pages import PageFactory


def _makeSomeTags(conn):
	curs = conn.cursor()
	curs.executemany("INSERT INTO config (key, value) VALUES (?,?)", [
		('tag', 'Linkes'),
		('tag', 'Frauen'),
		('tag', 'Revo'),
		('tag', 'Lokales'),
		('tag', 'Uni'),
		('tag', 'FSK'),
		('tag', 'AKF'),
		('tag', 'AIHD'),
		('tag', 'Wildwuchs'),
		('tag', 'Anarchie')])


def _makeSomeSponsors(conn):
	curs = conn.cursor()
	curs.executemany("INSERT INTO config (key, value) VALUES (?,?)", [
		('sponsor', 'potz'),])
	curs.executemany("INSERT INTO config (key, value) VALUES (?,?)", [
		('sponsor', 'pfurzner'),])


def _makeSomeEvents(conn):
	model.Event.create("Test", conn, curId=1111)
	ev1 = model.TaggedEvent.create("Test", conn, curId=1)
	ev1.change(teaser=_("Super-VA (jedeR weiß, wo)."),
		bdate=base.now()+datetime.timedelta(days=3),
		tags=["a", "c"],
		btime=datetime.time(11, 55)).compileFulltext().toDB(conn)
	model.TaggedEvent.create("Test", conn, curId=2
		).change(teaser=_("Denkwürdiges"),
			fulltext="Na ja, da war mal was.",
			bdate=base.now()+datetime.timedelta(days=5),
			tags=["a", "b"]).compileFulltext().toDB(conn)
	model.TaggedEvent.create("Test", conn, curId=3
		).change(teaser=_("Wir gedenken"),
			fulltext="Kein Vergessen und so.",
			tags=["c"],
			lastChanged=base.now(utc=True).timestamp()-86400,
			btime=datetime.time(20, 00), place="Autonomes Zentrum",
			bdate=base.now()+datetime.timedelta(days=5)).compileFulltext().toDB(conn)
	model.TaggedEvent.createWith("Test", conn, id=4,
		teaser=_("Vergangenes"), fulltext="Ist jetzt schon was her.",
		bdate=base.now()+datetime.timedelta(days=-5),
		tags=["vorbei", "rum"])
	model.TaggedEvent.createWith("Test", conn, id=5,
		teaser=_("Aktionswoche"), fulltext="Haut die Bullen...",
		bdate=base.now()+datetime.timedelta(days=2),
		fdate=(base.now()+datetime.timedelta(days=-2)).date(),
		tags=["c"])
	model.TaggedEvent.createWith("Test", conn, id=6,
		teaser=_("Debattenwoche"), fulltext="Lasst uns reden",
		bdate=base.now()+datetime.timedelta(days=120),
		fdate=(base.now()+datetime.timedelta(days=100)).date())
	model.TaggedEvent.createWith("Test", conn, id=7,
		teaser=_("Geheimtreffen"), fulltext="Gremien reden halt",
		bdate=base.now()+datetime.timedelta(days=10),
		private=True, tags=["geheim"])
	model.TaggedEvent.createWith("Test", conn, id=8,
		teaser=_("Märzhase"), fulltext="Ganzlang "*300,
		bdate=base.now()+datetime.timedelta(days=30),
		tags=[_("blöde")])
	model.TaggedEvent.createWith("Test", conn, id=9,
		teaser=_("Ewig her"),
		bdate=datetime.datetime(2006, 10, 21),
		tags=[_("c")])

	model.archive(conn)


_TEST_EVENTS = """
14.2.2000;1;In zehn Jahren wird getestet.
%%
8.2.2005;1;Wie schon 14.2.2000 hat jemand gegähnt.
"""

def _makeSomeMemories(conn):
	from sofoterm.plugins import memOTD

	with tempfile.NamedTemporaryFile() as f:
		f.write(_TEST_EVENTS.encode("utf-8"))
		f.flush()

		memOTD.main(conn, [f.name])


def make():
	conn = sofoterm.getConnection(":memory:")
	conn.stconf.setKeyValue('mailFrom', 'testing@ignor.ed')
	_makeSomeTags(conn)
	_makeSomeEvents(conn)
	_makeSomeSponsors(conn)
	_makeSomeMemories(conn)
	factory =  PageFactory(conn)
	factory.requestFactory = twistedpatch.MPRequest
	return factory
SERVER = make()


def main():
	factory = server.Site(SERVER)
	factory.requestFactory = twistedpatch.MPRequest
	reactor.listenTCP(8888, factory)
	reactor.run()


if __name__=="__main__":
	from sofoterm.pages.common import EventPage
	EventPage.doLastModified = False
	main()
