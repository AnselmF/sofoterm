#!/usr/bin/python3
"""
Regression tests for sofoterm.

This is basically a pyunit.TestCase subclass with some convencience
methods for pulling and checking things from a running server.

When in doubt, make your tests run with test_pages; this is mainly testing
that our FakeRequest infrastructure doesn't hide bugs.

This needs python3-requests.
"""

import contextlib
import os
import re
import subprocess
import time
import unittest

from lxml import etree as lxtree
import requests

from sofoterm import base

import testhelpers

class RegTest(unittest.TestCase):
	"""A regression test.

	Each RegTest corresponds to single "transaction", usually a single
	request.  path says what's retrieved (appended to serverURL), args gives
	arguments, headers gives additional headers.  For special effects override
	retrieveData, which has to return a requests.Response instance.

	That instance is available as self.response to individual tests, its content 
	attribute as self.data.  You'll usually use assertHasStrings and friends
	rather than mucking around with self.response and self.data.
	"""
	path = None
	method = "GET"
	headers = {}
	args = {}
	timeout = None
	serverURL = "http://localhost:8888"
	sponsor = False

	def __init__(self, methodName):
		super().__init__(methodName)
		self.retrieveData()

	def retrieveData(self):
		startTime = time.time()
		if self.path is None:
			raise Exception("Incomplete test (no url): {}".format(
				self.__class__.__name__))

		if self.sponsor:
			cookies = {"sponsor": "cG90eg=="}
		else:
			cookies = None

		try:
			self.response = requests.request(self.method, 
				self.serverURL+self.path, 
				headers=self.headers, params=self.args, cookies=cookies)
			self.data = self.response.content
		finally:
			self.requestTime = time.time()-startTime

	@contextlib.contextmanager
	def dumpOnFail(self):
		try:
			yield
		except:
			if hasattr(self, "data"):
				with open("bad-response.dat", "wb") as f:
					f.write(self.data)
			raise

	def assertHasStrings(self, *strings):
		"""checks that all its arguments are found within content.

		If string arguments are passed, they are utf-8 encoded before
		comparison.  If that's not what you want, pass bytes yourself.
		"""
		with self.dumpOnFail():
			for phrase in strings:
				assert base.bytify(phrase) in self.data, "%s missing"%repr(phrase)

	def assertLacksStrings(self, *strings):
		"""checks that all its arguments are *not* found within content.
		"""
		with self.dumpOnFail():
			for phrase in strings:
				assert base.bytify(phrase) not in self.data, \
					"Unexpected: '%s'"%repr(phrase)

	def assertHTTPStatus(self, expectedStatus):
		"""checks whether the request came back with expectedStatus.
		"""
		assert expectedStatus==self.response.status_code, (
			"Bad status received, {} instead"
			" of {}".format(self.response.status_code,
				expectedStatus))

	def assertXpath(self, path, assertions):
		"""checks an xpath assertion.

		path is an xpath (as understood by lxml)
		path must match exactly one element.

		assertions is a dictionary mapping attribute names to
		their expected value.	Use the key None to check the
		element content, and match for None if you expect an
		empty element.  To match against a namespaced attribute, you
		have to give the full URI; prefixes are not applied here.
		This would look like::

		  "{http://www.w3.org/2001/XMLSchema-instance}type": "vg:OAIHTTP"

		If you need an RE match rather than equality, there's
		EqualingRE in your code's namespace.
		"""
		if not hasattr(self, "cached parsed tree"):
			setattr(self, "cached parsed tree", lxtree.fromstring(self.data))
		tree = getattr(self, "cached parsed tree")
		
		with self.dumpOnFail():
			res = tree.xpath(path, namespaces=self.XPATH_NAMESPACE_MAP)
			if len(res)==0:
				raise AssertionError("Element not found: %s"%path)
			elif len(res)!=1:
				raise AssertionError("More than one item matched for %s"%path)

			el = res[0]
			for key, val in assertions.items():
				if key is None:
					try:
						foundVal = el.text
					except AttributeError:
						# assume the expression was for an attribute and just use teh
						# value
						foundVal = el
				else:
					foundVal = el.attrib[key]
				assert val==foundVal, "Trouble with %s: %s (%s, %s)"%(
					key or "content", path, repr(val), repr(foundVal))
	
	def assertHeader(self, key, value):
		"""checks that header key has value in the response headers.

		keys are compared case-insensitively, values are compared literally.
		"""
		try:
			foundValue = self.response.headers.get(key)
			self.assertEqual(value, foundValue)
		except KeyError:
			raise AssertionError("Header %s not found in %s"%(
				key, self.headers))

	def getUnique(self, seq):
		"""returns seq[0], asserting at the same time that len(seq) is 1.

		The idea is that you can say row = self.getUnique(self.getVOTableRows())
		and have a nice test on the side -- and no ugly IndexError on an
		empty respone.
		"""
		self.assertEqual(len(seq), 1)
		return seq[0]


def startTestServer():
	"""runs res_testserver through subprocess, returning the Popen
	object resulting.

	I/O is swallowed.
	"""
	if "COVERAGE_FILE" in os.environ:
		executor = ["python3-coverage", "run", "-a", "--source", ".."]
	else:
		executor = ["python3"]

	return subprocess.Popen(executor+["res_testserver.py"])


class BasicsMixin:
	"""a mixin that tests for status.
	"""
	expectedStatus = 200

	def testOk(self):
		self.assertHTTPStatus(self.expectedStatus)


class TestRootDelivered(BasicsMixin, RegTest):
	path = "/"

	def testSidebarPresent(self):
		self.assertHasStrings('/cal">Kalender')

	def testDatePresent(self):
		self.assertHasStrings('class="dayhead">Mittwoch, 17.02.</span>')
	
	def testFootlinePresent(self):
		self.assertHasStrings('	<div class="col"><a href="/dyn/about">Kontakt</a>')


class TestSuccessfulLogin(BasicsMixin, RegTest):
	method = "POST"
	path = "/login"
	args = {"sponsor": "potz", "__nevow_form__": "login"}

	def testCookie(self):
		self.assertEqual(self.response.cookies["sponsor"], "cG90eg==")

	def testPayload(self):
		self.assertHasStrings(">Abmelden<")


class TestStartEdit(BasicsMixin, RegTest):
	path = "/edit"
	sponsor = True

	def testPayload(self):
		self.assertHasStrings("Neuer Termin",
			"Privat</label>")

	def testRedirected(self):
		self.assertTrue(re.match(r"1\d\d+", self.response.url.split("/")[-1]))


class TestEditExisting(BasicsMixin, RegTest):
	path = "/edit/1"
	sponsor = True

	def testLongformPresent(self):
		self.assertHasStrings(
			'class="teaser">Super-VA (jedeR weiß, wo).</span></h1>')

	def testShortformPresent(self):
		self.assertHasStrings(
			'11.55 Uhr</span></span>:\n\t<span class="teaser">Super-VA')

	def testCanRemove(self):
		self.assertHasStrings(
			"Diesen Termin löschen")

def main():
	server = startTestServer()
	time.sleep(4)  # lazy: give the server time to come up.
	try:
		testhelpers.main(TestRootDelivered)
	finally:
		server.terminate()


if __name__=="__main__":
	main()
