# -*- coding: utf-8 -*-
"""
Tests for basic event classes and friends.
"""

import datetime
import io
import os
import sqlite3 as sqlite

import sofoterm
from sofoterm import base
from sofoterm import model
from sofoterm.model import ical
from sofoterm.model import textformat
# I want to test search() here, but it's to puny for a model of its own
from sofoterm.pages import pg_search

import res_testserver
import testhelpers


_CONN = res_testserver.SERVER.conn


class ConfigTest(testhelpers.VerboseTest):
	def testStringSet(self):
		tags = _CONN.stconf.tags
		self.assertIsInstance(tags, set)
		self.assertTrue("Uni" in tags)

	def testUndefined(self):
		self.assertRaisesWithMsg(base.Error,
			"No such predefined config: hurgel",
			lambda: _CONN.stconf.hurgel,
			())

	def testIntDeserialised(self):
		_CONN.stconf.sslPort = 40444
		# there's quite a bit of serialisation going on until:
		self.assertEqual(_CONN.stconf.sslPort, 40444)

	def testUniqueValue(self):
		_CONN.commit()
		_CONN.stconf.setUniqueValue("serverPID", 20)
		try:
			self.assertEqual(_CONN.stconf.serverPID, 20)
			self.assertRaisesWithMsg(ValueError,
				"serverPID is already set",
				_CONN.stconf.setUniqueValue,
				("serverPID", 30))
			self.assertEqual(_CONN.stconf.serverPID, 20)
		finally:
			_CONN.stconf.rmKeyValue("serverPID", 20)
			self.assertRaisesWithMsg(AttributeError,
				"serverPID",
				lambda: _CONN.stconf.serverPID,
				())

	def testGetValueMissing(self):
		self.assertRaisesWithMsg(KeyError,
			repr("knallfrosch"),
			_CONN.stconf.getValue,
			("knallfrosch",))

	def testGetValueBroken(self):
		self.assertRaisesWithMsg(ValueError,
			"More than one value for config item tag",
			_CONN.stconf.getValue,
			("tag",))


class SubscriptionTest(testhelpers.VerboseTest):
	def testSubscribeAndPost(self):
		received = []
		def handler(conn, **kwargs):
			received.append(kwargs)

		_CONN.subscribe("testsignal", handler)
		_CONN.post("testsignal", foo=3, bar=9)

		self.assertEqual(received[0], {"foo": 3, "bar": 9})

		_CONN.unsubscribe("testsignal", handler)
		_CONN.post("testsingal", foo=9, bar=27)

		# should not have changed since we're unsubscribed
		self.assertEqual(len(received), 1)

	def testUnsubNotsubscribed(self):
		_CONN.unsubscribe("testsignal", lambda conn: conn)

	def testFailingHandler(self):
		def handler(conn):
			return 1//0

		_CONN.subscribe("crashing", handler)
		with self.assertWarns(Warning, msg="quokka") as found:
			_CONN.post("crashing")
		self.assertEqual(found.warning.args[0][:40],
			"Exception while delivering crashing to <")


class PlainEventTest(testhelpers.VerboseTest):
	"""tests for building simple sofoterm events.
	"""
	def _getSampleEvent(self):
		return sofoterm.Event(id="abc", bdate=datetime.date(2003, 2, 3),
			btime=datetime.time(12, 00),
			teaser="Sample plays ex", sponsor="God")

	def testRepr(self):
		self.assertEqual(repr(self._getSampleEvent()),
			"<Event bdate=datetime.date(2003, 2, 3) btime=datetime.time(12, 0) id='abc' sponsor='God' teaser='Sample plays ex'>")

	def testEmptyIsInvalid(self):
		ev = sofoterm.Event()
		self.assertRaises(sofoterm.InvalidEvent, ev.assertValid)

	def testEquality(self):
		ev1 = self._getSampleEvent()
		ev2 = self._getSampleEvent()
		self.assertFalse(ev1 is ev2)
		self.assertEqual(ev1, ev2)

	def testInEquality(self):
		ev1 = self._getSampleEvent()
		ev2 = self._getSampleEvent()
		ev2.id = "xxx"
		self.assertFalse(ev1==ev2)

	def testConstructorWorks(self):
		ev = self._getSampleEvent()
		self.assertRuns(ev.assertValid, ())
		self.assertEqual(ev.id, "abc")
		self.assertEqual(ev.bdate, datetime.date(2003, 2, 3))
		self.assertEqual(ev.btime, datetime.time(12, 00))

	def testNotFromDBFails(self):
		ev = self._getSampleEvent()
		self.assertRaises(sofoterm.InvalidEvent, ev.toDB, _CONN)
	
	def testUniqueIds(self):
		ev1 = sofoterm.Event.create("tester", _CONN)
		ev2 = sofoterm.Event.create("tester", _CONN)
		ev3 = sofoterm.Event.create("tester", _CONN)
		self.assertFalse(ev1.id==ev2.id)
		self.assertFalse(ev1.id==ev3.id)
		self.assertFalse(ev2.id==ev3.id)

	def testNoSaving(self):
		ev = sofoterm.Event.fromId(1111, _CONN)
		self.assertRaises(model.IntegrityError, ev.save, _CONN)

	def testSerialization(self):
		ev = sofoterm.Event.create("tester", _CONN)
		ev = self._getSampleEvent().change(id=ev.id)
		ev.toDB(_CONN)
		deserialized = sofoterm.Event.fromId(ev.id, _CONN)
		self.assertEqual(deserialized, ev)

	def testNoChange(self):
		ev = self._getSampleEvent()
		self.assertTrue(ev.change() is ev)

	def testCloneFrom(self):
		ev = self._getSampleEvent()
		clone = model.Event.cloneFrom(ev, btime=datetime.time(13,00))
		self.assertEqual(clone.teaser, "Sample plays ex")
		self.assertEqual(clone.btime, datetime.time(13,00))

	def testAttach(self):
		ev = sofoterm.Event.createWith("tester", _CONN, bdate=base.now(),
			teaser="a")
		try:
			self.assertEqual(ev.attachments, 0)
			ev.setAttachment(_CONN, b"%PDF")
			self.assertEqual(ev.attachments, 1)

			ev = sofoterm.Event.fromId(ev.id, _CONN)
			self.assertEqual(ev.attachments, 1)
			attPath = ev.getAttachmentName()
			with open(attPath, "rb") as f:
				self.assertEqual(f.read(), b"%PDF")

			ev.removeAttachment(_CONN)
			ev = sofoterm.Event.fromId(ev.id, _CONN)
			self.assertEqual(ev.attachments, 0)
			self.assertFalse(os.path.exists(attPath))
		finally:
			ev.delete(_CONN)

	def testBoolSerialization(self):
		ev = sofoterm.Event.createWith("tester", _CONN, bdate=base.now(),
			teaser="a", private=True)
		ev.toDB(_CONN)
		ev = sofoterm.Event.fromId(ev.id, _CONN)
		self.assertEqual(ev.private, True)
		ev.delete(_CONN)

	def testIterAttributes(self):
		ev = self._getSampleEvent()
		self.assertEqual(
			set(k for k, v in ev.iterAttributes()),
			{'bdate', 'attachments', 'lastChanger', 'lastChanged',
				'fulltextCompiled', 'place', 'sponsor', 'teaser', 'fdate',
				'btime', 'fulltext', 'private', 'id'})
		self.assertEqual(
			dict(ev.iterAttributes(skipEmpty=True)), {
				'bdate': datetime.date(2003, 2, 3),
 			 	'btime': datetime.time(12, 0),
 			 	'id': 'abc', 'sponsor': 'God',
 			 	'teaser': 'Sample plays ex'})
		
	def testComparisons(self):
		ev = self._getSampleEvent()
		self.assertEqual(ev, ev)
		self.assertTrue(ev!=0)
		self.assertFalse(ev==0)

	def testNoIdChange(self):
		ev = self._getSampleEvent()
		self.assertRaisesWithMsg(ValueError,
			"Cannot modify id in-place, use change.",
			lambda: ev.modify(id=23),
			())

	def testAttrChecked(self):
		ev = self._getSampleEvent()
		self.assertRaisesWithMsg(ValueError,
			"Invalid event attribute: 'roi'",
			lambda: ev.modify(roi="23%"),
			())

	def testWeed(self):
		ev = sofoterm.Event.createWith("tester", _CONN, bdate=base.now())
		evId = ev.id
		self.assertEqual(
			list(_CONN.execute("SELECT count(*) from events where id=?", (evId,))),
			[(1,)])
		_CONN.commit()
		model.weed(_CONN)
		self.assertEqual(
			list(_CONN.execute("SELECT count(*) from events where id=?", (evId,))),
			[(0,)])
		_CONN.commit()


class TaggedEventTest(testhelpers.VerboseTest):
	"""tests for building and deserializing events with tags.
	"""
	def testRoundtrip(self):
		ev = sofoterm.TaggedEvent.createWith("tester", _CONN,
			bdate=base.now(), teaser="x",
			tags=set(["foo", "bar"]))
		newEv = sofoterm.TaggedEvent.fromId(ev.id, _CONN)
		self.assertEqual(ev, newEv)

	def testSetChange(self):
		"""tests for tags being serilaized to disk.
		"""
		ev = sofoterm.TaggedEvent.createWith("tester", _CONN,
			bdate=base.now(), teaser="x",
			tags=set(["foo", "bar"]))
		ev.tags.remove("bar")
		ev.tags.add("quux")
		ev.toDB(_CONN)
		newEv = sofoterm.TaggedEvent.fromId(ev.id, _CONN)
		self.assertEqual(ev, newEv)

	def testDeletion(self):
		"""tests for tags being removed when an event is deleted.
		"""
		ev = sofoterm.TaggedEvent.createWith("tester", _CONN,
			bdate=base.now(), teaser="x",
			tags=set(["foo", "bar"]))
		ev.toDB(_CONN)
		id = ev.id
		cursor = _CONN.execute("SELECT tag FROM tags WHERE id=?", (id,))
		self.assertEqual(set(r[0] for r in cursor.fetchall()),
			set(['foo', 'bar']))
		ev = sofoterm.Event.fromId(id, _CONN)  # Event, not TaggedEvent!
		ev.delete(_CONN)
		cursor = _CONN.execute("SELECT tag FROM tags WHERE id=?", (id,))
		self.assertEqual(cursor.fetchall(), [])


class _ComplementSet(set):
	"""A set that says something is in it when it's not in another set.

	This is useless outside of this context since we have a *very* funky
	notion of equality.
	"""
# we need this for robustness of some of the EventListSelectorTests
	def __init__(self, base):
		set.__init__(self)
		self.base = set(base)

	def __repr__(self):
		return "ComplementSet([%s])"%",".join(str(s) for s in self.base)

	def __contains__(self, item):
		return not item in self.base

	def __eq__(self, otherSet):
		for el in otherSet:
			if el not in self:
				return False
		return True


class EventListSelectorTest(testhelpers.VerboseTest,
		metaclass=testhelpers.SamplesBasedAutoTest):
	"""tests for specifying event lists.
	"""

	def _runTest(self, sample):
		conditions, ids = sample
		if isinstance(ids, list):
			ids = set(ids)
		if "startDate" not in conditions:
			conditions["startDate"] = base.now()
		evs = set(ev.id
			for ev in sofoterm.EventList.fromSelectors(_CONN, **conditions))
		self.assertEqual(evs, ids)
	
	samples = [
		({'wantedTags': ['a']}, [1,2]),
		({'wantedTags': ['a', 'd']}, [1,2]),
		({'wantedTags': ['a', 'c']}, [1,2,3]),
		({'wantedTags': 'ac', 'unwantedTags': 'b'}, [1,3]),
		({'unwantedTags': 'b'}, [1, 3, 8]),
# 05
		({'wantedTags': 'ac', 'endDate': datetime.datetime(2010, 2, 20)},
			[1,2,3]),
		({'wantedTags': 'ac', 'endDate': datetime.datetime(2010, 2, 13)},
			[]),
		({'wantedTags': 'ac', 'startDate': datetime.datetime(2010, 2, 17),
			'endDate': datetime.datetime(2010, 2, 20)},
			[2,3]),
		({'wantedTags': ['geheim']}, [7]),
		({'wantedTags': ['geheim', 'a']}, [1,2,7]),
# 10
		({'sponsor': True}, [1,2,3,7,8]),
		]


class XMLTest(testhelpers.VerboseTest):
	"""tests from serialization and deserialization to/of XML.
	"""
	def _toXML(self, ob):
		f = io.BytesIO()
		sofoterm.dump(ob, f)
		return f.getvalue().decode("utf-8")

	def testSimpleSer(self):
		ev = sofoterm.Event(bdate=datetime.date(2010, 2, 20),
			btime=datetime.time(13, 40), teaser="Übernahme",
			fulltext="foobar", fulltextCompiled="<a>link</a>")
		self.assertHasStrings(repr(self._toXML(ev)), [
			"<bdate>2010-02-20</bdate>",
			"<fulltextCompiled>&lt;a&gt;l",
			"<teaser>Über"])
	
	def testTaggedSer(self):
		ev = sofoterm.TaggedEvent(bdate=datetime.date(2010, 2, 20),
			teaser="x", tags=set(["a", "b", "c"]))
		self.assertHasStrings(
			repr(self._toXML(ev)), ["<tag>a</tag>", "<tag>c</tag>"])

	def _makeTestEvents(self, conn):
		sofoterm.TaggedEvent.createWith("foo", conn, id=111000,
			bdate=datetime.date(2010, 2, 20),
			teaser="x", tags=set(["a", "dumptest"]))
		sofoterm.TaggedEvent.createWith("foo", conn, id=111001,
			bdate=datetime.date(2010, 2, 21),
			teaser="y", tags=set(["a", "b", "dumptest"]))

	def testEventListPlain(self):
		try:
			self._makeTestEvents(_CONN)
			res = repr(self._toXML(sofoterm.EventList.fromSelectors(_CONN,
				wantedTags=["dumptest"])))
			self.assertHasStrings(res,
				["<event><id>111000", "</event></eventList>"])
		finally:
			_CONN.rollback()

	def testEventListTagged(self):
		try:
			self._makeTestEvents(_CONN)
			res = repr(self._toXML(sofoterm.EventList.fromSelectors(_CONN,
				wantedTags=["dumptest"], evClass=sofoterm.TaggedEvent)))
			self.assertHasStrings(res, [
				'<tag>b</tag>',
				'<tag>dumptest</tag>'])
		finally:
			_CONN.rollback()

	def testRoundtripSingle(self):
		ev = sofoterm.TaggedEvent(bdate=datetime.date(2010, 2, 20),
			btime=datetime.time(13, 40), teaser="Übernahme",
			fulltext="foobar", fulltextCompiled="<p>foobar</a>",
			place="some place", tags=set(["a", "b", "c"]))
		ev.compileFulltext()
		ser = self._toXML(ev)
		newEv = sofoterm.parseEvent(io.StringIO(ser),
			evClass=sofoterm.TaggedEvent)
		self.assertEqual(ev, newEv)

	def testRoundtripEventList(self):
		evlist = sofoterm.EventList([
			sofoterm.TaggedEvent(bdate=datetime.date(2010, 2, 20),
				btime=datetime.time(13, 40), teaser="Übernahme",
				fulltext="foobar", fulltextCompiled="<a>link</a>",
				place="some place", tags=set(["a", "b", "c"])),
			sofoterm.TaggedEvent(bdate=datetime.date(2010, 2, 21),
				btime=datetime.time(13, 20), teaser="Übernahme2",
				fulltext="foobar")])
		for ev in evlist:
			ev.compileFulltext()
		ser = self._toXML(evlist)
		newEvList = sofoterm.parseEventList(io.StringIO(ser),
			evClass=sofoterm.TaggedEvent)
		for orig, copy in zip(evlist, newEvList):
			self.assertEqual(orig, copy)


class ArchiveTest(testhelpers.VerboseTest):
	"""tests for working archiving.
	"""
	def setUp(self):
		if hasattr(_CONN, "archiveTestEvent"):
			self.createdEv = _CONN.archiveTestEvent
		else:
			self.createdEv = sofoterm.TaggedEvent.createWith("testing",
				_CONN, id=45000, bdate=datetime.date(2010, 2, 13),
				teaser="foo", tags=set(["toarchive", "test"]))
			sofoterm.archive(_CONN)
			_CONN.archiveTestEvent = self.createdEv

	def testNotInMain(self):
		self.assertRaisesWithMsg(sofoterm.EventNotFound,
			"Event 45000 not found",
			sofoterm.Event.fromId,
			(self.createdEv.id, _CONN))

	def testInArchive(self):
		arconn = _CONN.getArchiveConnection()
		arched = sofoterm.TaggedEvent.fromId(self.createdEv.id, arconn)
		self.assertEqual(arched.id, self.createdEv.id)
		self.assertEqual(arched.teaser, self.createdEv.teaser)
		self.assertEqual(arched.tags, self.createdEv.tags)

	def testArchiveList(self):
		arconn = _CONN.getArchiveConnection()
		el = list(sofoterm.EventList.fromSelectors(arconn,
			startDate=self.createdEv.bdate,
			endDate=self.createdEv.bdate,
			evClass=sofoterm.TaggedEvent))
		self.assertTrue(el[0].id==self.createdEv.id)
		self.assertTrue(el[0].tags==self.createdEv.tags)

	def testArchiving(self):
		sofoterm.TaggedEvent.createWith("testing", _CONN,
			id=45001, bdate=datetime.date(1999, 5, 10),
				teaser="antik", tags=set(["toarchive", "test"]))
		_CONN.commit()
		evs = list(_CONN.execute("SELECT * from events where id=45001"))
		self.assertEqual(len(evs), 1)
		from sofoterm import cli
		cli.cmd_archive(_CONN, {})
		evs = list(_CONN.execute("SELECT * from events where id=45001"))
		self.assertEqual(len(evs), 0)
		evs = list(_CONN.getArchiveConnection().execute(
			"SELECT * from events where id=45001"))
		self.assertEqual(len(evs), 1)

		# we may have archived away event 1111 depending on the execution sequence
		model.Event.create("Test", _CONN, curId=1111)


class ICalTest(testhelpers.VerboseTest):
	def _getSampleEvent(self, **overrides):
		items = {"id": "abc",
			"bdate": datetime.date(2003, 2, 3),
			"btime": datetime.time(12, 00),
			"teaser": "Sample plays ex",
			"sponsor": "God",}
		items.update(**overrides)
		return sofoterm.Event(**items)
	
	def testBasic(self):
		res = ical.formatAsICal(self._getSampleEvent(), _CONN)
		self.assertTrue("UID:abc@sofo-hd.de\r\n" in res)
		# Warning: this will fail when run in non-CET time zones
		self.assertTrue("DTSTART:20030203T110000Z" in res)
		self.assertTrue('\r\nSUMMARY:"Sample plays ex' in res)

	def testDST(self):
		res = ical.formatAsICal(self._getSampleEvent(
			bdate=datetime.date(2003, 5, 3)), _CONN)
		self.assertTrue("DTSTART:20030503T100000Z" in res)

	def testFolding(self):
		res = ical.formatAsICal(self._getSampleEvent(
			teaser="Doofer Kram, der umgebrochen werden muss"
				" und überhaupt nervt",
			fulltext="Ein längerer Text mit komischen Zeichen (äüß"
				" und sowas) und überhaupt allerlei\tdoofen Sachen, so dass es"
				" notwendig werden wird, ihn\numzubrechen.\n"), _CONN)
		self.assertTrue("UID:abc@sofo-hd.de" in res)
		self.assertTrue('SUMMARY:"Doofer Kram, der' in res)
		self.assertTrue("\r\n ) und überhaupt allerlei\t" in res)
		self.assertTrue("äüß und sowas\r" in res)
		self.assertTrue("notw\r\n endig" in res)


class RemoteFulltextTest(testhelpers.VerboseTest):
	"""tests for events having just a URL as their fulltexts.
	"""
	def testText(self):
		ev = sofoterm.Event(id="abc", bdate=datetime.date(2003, 2, 3),
			place="_test",
			btime=datetime.time(12, 00),
			teaser="Go somewhere", sponsor="God",
			fulltext="http://some.where/else")
		self.assertHasStrings(textformat.formatEvent(_CONN, ev), [
 			"\n  http://some.where/else", # URL rendered directly
 			"12.00, Cherry Lane", # HTML defused in predefined place
		])


class FulltextTest(testhelpers.VerboseTest):
	def testBasic(self):
		res = pg_search.search(_CONN, "Vergessen")
		self.assertEqual(res, [(3, 'Kein ❯Vergessen❰ und so.', 'Wir gedenken', '2010-02-19', '')])
	
	def testArchive(self):
		res = pg_search.search(_CONN, "ewig")
		self.assertEqual(res, [(9, '❯Ewig❰ her', 'Ewig her', '2006-10-21', 'archive')])

	maxDiff = None
	def testEllipsis(self):
		res = pg_search.search(_CONN, "Ganzlang")
		self.assertEqual(res, [(8,
			'❯Ganzlang❰ ❯Ganzlang❰ ❯Ganzlang❰ ❯Ganzlang❰ ❯Ganzlang❰ ❯Ganzlang❰ '
			'❯Ganzlang❰ ❯Ganzlang❰ ❯Ganzlang❰ ❯Ganzlang❰ ❯Ganzlang❰ ❯Ganzlang❰ '
			'❯Ganzlang❰ ❯Ganzlang❰ ❯Ganzlang❰…',
			'Märzhase',
			'2010-03-16',
			'')])


if __name__=="__main__":
	testhelpers.main(XMLTest)
