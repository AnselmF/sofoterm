"""
Twisted trial-based tests.

Run these using trial test_pages.py or somesuch.
"""

# IMPORTANT: If you have deferreds in your tests, *return them* from
# the tests.  Looks weird, but that's the only way the reactor gets
# to see them.  If you don't have deferreds in your tests, they don't
# really belong here in the first place.

import atexit
import base64
import datetime
import io
import os
import re
import tempfile
import urllib.parse

from twisted.python import threadable
threadable.init()

import sofoterm
from sofoterm import _
from sofoterm import base
from sofoterm import model
from sofoterm.base import common as basecommon
from sofoterm.formal import nevowc
from sofoterm.formal import testing
from sofoterm.pages import errors
from sofoterm.pages import root

import testhelpers
from res_testserver import SERVER


def _nukeNamespaces(xmlString):
	nsCleaner = re.compile(b'^(</?)(?:[a-z0-9]+:)')
	return re.sub(b"(?s)<[^>]*>",
		lambda mat: nsCleaner.sub(br"\1", mat.group()),
		re.sub(b'xmlns="[^"]*"', b"", xmlString))


def getXMLTree(xmlString, debug=False):
	"""returns an ``libxml2`` etree for ``xmlString``, where, for convenience,
	all namespaces on elements are nuked.

	The libxml2 etree lets you do xpath searching using the ``xpath`` method.

	Nuking namespaces is of course not a good idea in general, so you
	might want to think again before you use this in production code.
	"""
	from lxml import etree as lxtree
	munged = _nukeNamespaces(xmlString)
	try:
		tree = lxtree.fromstring(munged)
	except lxtree.XMLSyntaxError:
		with open("remote.data", "wb") as f:
			f.write(munged)
		raise

	if debug:
		lxtree.dump(tree)
	return tree


def makeSponsoredRequest(sponsor):
	encodedSponsor = (None if sponsor is None
		else base64.b64encode(sponsor.encode("utf-8")))

	class SponsoredRequest(testing.FakeRequest):
		def __init__(self, *args, **kwargs):
			super().__init__(*args, **kwargs)
			self.setHost(b"localhost", 8080)

		def getCookie(self, key):
			assert key==b"sponsor"
			return encodedSponsor

		def addCookie(self, *args, **kwargs):
			pass

	return SponsoredRequest


class PagesTest(testing.RenderTest):
	"""a base class for tests of our twisted web/nevow resources/page.
	"""
	sponsor = "potz"
	renderer = SERVER

	errorHandler = staticmethod(lambda flr, req: errors.serveError(req, flr))

	def runQuery(self, *args, **kwargs):
		sponsor = kwargs.pop("sponsor", self.sponsor)
		if "requestClass" not in kwargs:
			kwargs["requestClass"] = makeSponsoredRequest(sponsor)
		return super().runQuery(*args, **kwargs)

	def assertXpath(self, result, path, assertions):
		"""checks an xpath assertion.

		path is an xpath (as understood by lxml); XML namespaces are nuked.
		path must match exactly one element.

		assertions is a dictionary mapping attribute names to
		their expected value.   Use the key None to check the
		element content, and match for None if you expect an
		empty element.

		On success, this returns result, so these can be daisy-chained.

		The normal use is something like

		return assertGETHasStrings().addCallback(
			self.assertXpath, "//foo[@id='ga'", {...})
		"""
		tree = getXMLTree(result[0])
		res = tree.xpath(path)
		if len(res)==0:
			raise AssertionError("Element not found: %s"%path)
		elif len(res)!=1:
			raise AssertionError("More than one item matched for %s"%path)

		el = res[0]
		for key, val in assertions.items():
			if key is None:
				foundVal = el.text
			else:
				foundVal = el.attrib[key]
			assert val==foundVal, "Trouble with %s: %s (%s, %s)"%(
				key or "content", path, repr(val), repr(foundVal))
		
		return result


class DateEntryTest(PagesTest, metaclass=testhelpers.SamplesBasedAutoTest):
	"""playing with various date inputs.
	"""
	def _runTest(self, sample):
		input, output = sample
		def realTest(postResult):
			return self.assertGETHasStrings("/edit/1111", {}, [output])
		return self.runQuery(SERVER, "POST", "/edit/1111",
			{"teaser":"a", "bdate": input,
			"__nevow_form__": "termedit"}
		).addCallback(realTest)

	samples = [
		("1", "01.03.2010"),
		("16", "16.02.2010"),
		("31.1.", "31.01.2011"),
		("31.3.", "31.03.2010"),
		("20/03", "20.03.2010"),
		("10.3.10", "10.03.2010"),
		("20. Juni", "20.06.2010"),
		("Mittwoch, 20. Juni", "20.06.2010"),
		("20.6.2010", "20.06.2010"),
		("2010-06-28", "28.06.2010"),
	]


class TimeEntryTest(PagesTest, metaclass=testhelpers.SamplesBasedAutoTest):
	"""playing with various time inputs.
	"""
	def _runTest(self, sample):
		input, output = sample
		def realTest(postResult):
			return self.assertGETHasStrings("/edit/1111", {}, [output])
		return self.runQuery(SERVER, "POST", "/edit/1111",
			{"teaser":"a", "bdate": "1",
			"btime": input, "__nevow_form__": "termedit"},
		).addCallback(realTest)

	samples = [
		("1", "01:00"),
		("16 Uhr", "16:00"),
		("14:12", "14:12"),
		("14.13.", "14:13"),
		("16 Uhr 30", "16:30"),
	]


class EditTest(PagesTest):
	def _getForm(self, res):
		dest, args = res[1].getLocationValue().split("?")
		return self.assertGETHasStrings(
			dest, urllib.parse.parse_qs(args, encoding="utf-8"), [
				'value="16.02.2010"', # expanded from "Dienstag" in bdate
				"Immer wieder dienstags: Ö"])

	def testGETFillsForm(self):
		return self.assertGETHasStrings("/edit", {
				"teaser": "Immer wieder dienstags: Ö",
				"bdate": "Dienstag"},
			['href="/edit/', 'manually']
			).addCallback(self._getForm)

	def testAttachingJunk(self):
		return self.assertPOSTHasStrings("/edit/8",
			{"__nevow_form__": "attach"},
			["Der Zusatz sieht überhaupt nicht nach einem PDF aus."],
			rm=lambda req: req.addUpload("upload", b"abc"))

	def _assertNoAttachment(self, res):
		return self.assertGETLacksStrings("/edit/8", {},
			["PDF zum Termin",  # rendering of event with attachment
		])

	def _removeAttachment(self, res):
		# We really should redirect in the attach form so the labels
		# (Ersetzen/Löschen) are in sync right away, but I can't be bothered
		# now.
		return self.assertPOSTHasStrings("/edit/8",
			{"__nevow_form__": "attach", "delAttach": "1"},
			['Ersetzen', 'Löschen']
			).addCallback(self._assertNoAttachment)

	def testAttachingPDF(self):
		return self.assertPOSTHasStrings("/edit/8",
			{"__nevow_form__": "attach", "upload": b"%PDF"}, [
				'<a href="/event/8/attached.pdf">',
				"PDF zum Termin",  # rendering of event with attachment
				],
				rm=lambda req: req.addUpload("upload", b"%PDF")
			).addCallback(self._removeAttachment)

	def testUploadLimit(self):
		SERVER.conn.stconf.uploadLimit = 10000
		return self.assertPOSTHasStrings("/edit/8", {
				"__nevow_form__": "attach",}, [
				"Zusätze dürfen nicht länger als 10000 Bytes sein",],
			rm=lambda req: req.addUpload("upload", b"%PDF"*3000))

	def testEditGhostEvent(self):
		return self.assertGETHasStrings("/edit/ronzo", {},
			["<h1>Nicht gefunden</h1>"])

	def testCloneGhostEvent(self):
		return self.assertGETHasStrings("/edit/ronzo/clone", {},
			["<h1>Nicht gefunden</h1>"])


class TagsEntryTest(PagesTest):
	"""entering and deleting tags.
	"""
	def assertTagResult(self, moreKeys, test):
		args = {"bdate": "1", "teaser": "a",
			"__nevow_form__": "termedit"}
		args.update(moreKeys)
		return self.runQuery(SERVER, "POST", "/edit/1111", args,
			).addCallback(test)

	def testSetNoTag(self):
		def test(postResult):
			return self.assertGETLacksStrings("/edit/1111", {}, ["checked"])
		return self.assertTagResult({}, test)

	def testSetOneTag(self):
		def assertChecked(result):
			tree = getXMLTree(result[0])
			self.assertEqual(
				tree.xpath("//input[@value='Frauen']")[0].get("checked"),
				"checked")

		def test(postResult):
			return self.assertGETHasStrings("/edit/1111", {},
				['<span class="curid">1111</span>']
				).addCallback(assertChecked)

		return self.assertTagResult({"tags": "Frauen"}, test)

	def testSetTwoTags(self):
		def assertChecked(result):
			tree = getXMLTree(result[0])
			self.assertEqual(tree.xpath("//input[@value='Frauen']")[0].get("checked"),
				"checked")
			self.assertEqual(
				tree.xpath("//input[@value='AKF']")[0].get("checked"),
				"checked")

		def test(postResult):
			return self.assertGETHasStrings("/edit/1111", {},
				['<span class="curid">1111</span>']
				).addCallback(assertChecked)
				
		return self.assertTagResult({"tags": ["Frauen", "AKF"]}, test)

	def testCustomTagsShow(self):
		def test(ignored):
			return self.assertGETHasStrings("/edit/1111", {}, []
				).addCallback(self.assertXpath,
					"//input[@value='customTag1']",
					{"name": "tags", "checked": "checked"}
				).addCallback(self.assertXpath,
					"//input[@value='customTag2']",
					{"name": "tags", "checked": "checked"})
		return self.assertTagResult({"tags": ["customTag1",
				"customTag2"]}, test)

	def testExtraTag(self):
		def cleanup(ignored):
			SERVER.conn.stconf.rmKey(self.sponsor+"::tag")

		SERVER.conn.stconf.addKeyValue(self.sponsor+"::tag", "krawall")
		return self.assertGETHasStrings("/edit/1111", {},
			['value="krawall"']
		).addBoth(cleanup)

	def testNoExtraTag(self):
		return self.assertGETLacksStrings("/edit/1111", {},
			['value="krawall"'])


class ErrorsTest(PagesTest):
	"""See if error pages are delivered as expected.
	"""
	def testPlain404(self):
		return self.assertGETHasStrings("/nonexistingresource", {},
			["Nicht gefunden", "No such resource"])

	def testNoneIsNotFound(self):
		return self.assertGETHasStrings("/edit/bar/foo", {},
			["Nicht gefunden", "No such resource"])

	def testNoEvent(self):
		return self.assertStatus("/edit/1112", 404)
	
	def testRedirect(self):
		return self.assertStatus("/errors/re", 302)
	
	def testForbiddenCode(self):
		return self.assertStatus("/errors/fb", 403)
	
	def testForbiddenMsg(self):
		return self.assertGETHasStrings("/errors/fb", {},
			["kann nur machen", "statt klicken"])
	
	def testBrokenCode(self):
		return self.assertGETHasStrings("/errors/murks", {},
			["passieren sollen", "unspecified"])

	def testBadDate(self):
		return self.assertPOSTHasStrings("/edit/1111", {
			"teaser":"a", "bdate": "glugger", "__nevow_form__": "termedit"},
			["Kein Datum zu erkennen"])

	def testBadTime(self):
		return self.assertPOSTHasStrings("/edit/1111", {
			"teaser":"a", "bdate": "1", "__nevow_form__": "termedit", "btime": "32"},
			["Zeit aus den Fugen", "0..23"])

	def testRSTErrorEncoding(self):
		return self.assertPOSTHasStrings("/edit/1111", {
			"teaser":"a", "bdate": "1", "__nevow_form__": "termedit",
			"fulltext": '`knülle`_'},
			['Line 1: Unknown target name: "knülle".'])

	def testChildless(self):
		return self.assertGETHasStrings("/config/rotten", {},
			["Nicht gefunden", "/config hat keine Unterseiten"])


class NoSponsorTest(PagesTest):
	"""tests for behaviour for non-sponsors.
	"""
	sponsor = None

	def testRootAdapts(self):
		return self.assertGETLacksStrings("/list", {}, ["[Neuer Termin]"])

	def testStyleRemains(self):
		return self.assertGETHasStrings("/list", {}, ["default.css"])

	def testEditDenied(self):
		return self.assertStatus("/edit", 403)

	def testEditArticleDenied(self):
		return self.assertPOSTHasStrings("/edit/1", {"teaser": "x"},
			["Darfst du nicht", "Hingehen"])

	def testNoDelete(self):
		return self.assertResultHasStrings("DELETE", "/event/1111",
			{}, ["Nix gibts"])

	def testNoTagedit(self):
		def assertion(res):
			return self.assertStringsIn(res,
				["<p>Diese Funktion hier kann nur machen,"])

		return self.runQuery(SERVER, "GET", "/tagedit", {},
			).addCallback(assertion)

	def testCal(self):
		return self.assertGETLacksStrings("/cal", {},
			["Neuen Termin für diesen Tag", "/edit"])


class EventListTest(PagesTest):
	"""tests dealing with event lists.
	"""
	def testMainPage(self):
		return self.assertGETHasStrings("/list", {}, [
			"Neuer Termin", "default.css",
			"Mittwoch, 17.02.</span></h2>",
			'11.55 Uhr</span>',
			'<span class="teaser">Super-VA',
			'teaser">Denkw',
			'<span class="time">20.00 Uhr</span>',
			'class="icon place"></i>Autonomes Zentrum',
			'<span class="teaser">Wir'])

	def testEndDate(self):
		def _checkNoLateEvent(res):
			self.assertFalse(b"Wir gedenken" in res[0])
			return res

		return self.assertGETHasStrings("/list", {"endDate": "2010-02-17"}, [
			"17.02"]).addCallback(_checkNoLateEvent)

	def testBadStartDate(self):
		return self.assertGETHasStrings("/list", {"startDate": "foobar"}, [
			"	Bad ISO date/time: 'foobar'",
			"Ups..."])

	def testOneTagCatches(self):
		return self.assertGETHasStrings("/list", {'tag': 'a'}, [
			"Super-VA", ">Denkw"])

	def testOneTagLacks(self):
		return self.assertGETLacksStrings("/list", {'tag': 'a'}, [
			"event/3",
			"Aktionswoche",  # see that long-terms are excluded as well
			"Im Gange...",   # and that the header gets swallowed
		])

	def testOneTagLongterm(self):
		return self.assertGETHasStrings("/list", {'tag': 'c'}, [
			"Aktionswoche",
			"Im Gange...",
			"Super-VA"])

	def testDefaultList(self):
		return self.assertGETHasStrings("/list", {}, [
			'<span class="time">11.55 Uhr</span>',
			'<h3 class="termhead"><span class="teaser">Denkw'])


class ArchiveTest(PagesTest):
	def testArchiveList(self):
		return self.assertGETHasStrings("/archive", {}, [
			'span class="teaser">Vergangenes'])

	def testArchiveNoEdit(self):
		return self.assertGETLacksStrings("/archive", {}, [
			'Edit'])
	
	def testArchiveYear(self):
		return self.assertGETHasStrings("/archive/2010", {}, [
			'span class="teaser">Vergangenes'])

	def testArchiveYearSelects(self):
		return self.assertGETLacksStrings("/archive/2009", {}, [
			'span class="teaser">Vergangenes'])

	def testArchiveMonth(self):
		return self.assertGETHasStrings("/archive/2010/02", {}, [
			'span class="teaser">Vergangenes'])

	def testArchiveMonthSelects(self):
		return self.assertGETLacksStrings("/archive/2010/01", {}, [
			'span class="teaser">Vergangenes'])

	def testArchiveBadChild(self):
		return self.assertGETHasStrings("/archive/badyear", {},
			["Servermaschinerie: This doesn't lead to any archive."])

	def testArchiveNextYear(self):
		return self.assertGETHasStrings("/archive/2009/12", {}, [
			"Dezember 2009",])
		# We also ought to test if the end date really is 2010-01-01 here,
		# but I'm too lazy.
	
	def testArchiveOverrideDates(self):
		return self.assertGETHasStrings("/archive",
			{"startDate": "1980-01-01", "endDate": "2013-02-01"}, [
			"Termin-Archiv zwischen 1. Januar 1980 und 1. Februar 2013"])

	def testArchiveOverrideDays(self):
		return self.assertGETHasStrings("/archive",
			{"endDate": "2008-06-04", "nDays": "365"}, [
			"Termin-Archiv zwischen 5. Juni 2007 und 4. Juni 2008"])

	def testYearBeforeStops(self):
		return self.assertGETLacksStrings("/archive/2001", {},
			["archive/2000", ">1999"])

	def testForwardLinks(self):
		return self.assertGETHasStrings("/archive/2006", {},
			["2007", "2008", "2009"])

	def testYearAfterStops(self):
		return self.assertGETLacksStrings("/archive/2010", {},
			["2011", "2012", "2013"])

	def testArchivedCal(self):
		return self.assertGETHasStrings("/cal/2006/10", {"tag": ["c", "foo"]}, [
			"Ewig her",
			'<a href="/cal/2006/11?tag=c&amp;tag=foo">November 2006'])


class FullevTest(PagesTest):
	"""tests for working full event/full text display.
	"""
	def testSimple(self):
		return self.assertGETHasStrings("/event/1", {}, [
			'default.css',
			'<title>Termin für 2010-02-17',
			'<h1><span class="teaser">Super-VA '])
	
	def testFulltextEdit(self):
		def testOriginalUnchanged(ignored):
			return self.assertGETLacksStrings("/event/1", {}, [
				'<li>einmal'])

		def removeChange(ignored):
			return self.assertPOSTHasStrings("/edit/1111", {"fulltext":
				"Hurgel", "__nevow_form__": "termedit", "teaser": "a", "bdate": "1"},
				["Hurgel"]).addCallback(testOriginalUnchanged)

		def testChanged(ignored):
			return self.assertGETHasStrings("/event/1111", {}, [
				'<li>einmal']
			).addCallback(removeChange)

		return self.assertPOSTHasStrings("/edit/1111", {
				"teaser": "a", "bdate": "1",
				"fulltext": "* einmal\n* zweimal", "__nevow_form__": "termedit"}, []
			).addCallback(testChanged)
	
	def testBadRSTRejected(self):
		return self.assertPOSTHasStrings("/edit/1111", {
				"teaser": "a", "bdate": "1",
				"fulltext": "* einmal\nzweimal", "__nevow_form__": "termedit"}, [
				'<strong>Langtext : ',
				'Bullet list ends without'])

	def testArchivedEventFound(self):
		return self.assertGETHasStrings("/event/4", {},
			["Ist jetzt schon was her."])

	def testNaked(self):
		return self.assertGETHasStrings("/event/n/4", {},
				['full-description"><p>Ist jetzt']
			).addCallback(
				self.assertXpath, "//p[@class='ednote']/a", {"href": "/"})


class StaticTest(PagesTest):
	"""tests for delivering static resources.
	"""
	def testNonExisting(self):
		return self.assertStatus("/static/etc/passwd", 404)
	
	def testNoParent(self):
		return self.assertStatus("/static/../model.py", 403)
	
	def testCSSDelivery(self):
		def test(res):
			self.assertEqual(res[1].getResponseHeader("Content-Type"), "text/css")
			self.assertEqual(res[1].code, 200)
			self.assertTrue(b"body" in res[0], "CSS content seems broken")

		return self.runQuery(SERVER, "GET", "/static/default.css", {}
			).addCallback(test)
	
	def testCSSRootAlias(self):
		def test(res):
			self.assertEqual(res[1].getResponseHeader("Content-Type"), "text/css")
			self.assertEqual(res[1].code, 200)

		return self.assertGETHasStrings("/default.css", {},
			["body"]
			).addCallback(test)

	def testNonExistingTemplatePage(self):
		return self.assertStatus("/dyn/missionstatement", 404)

	def testDynRoot(self):
		return self.assertStatus("/dyn", 404)
	
	def testValidDyn(self):
		return self.assertGETHasStrings("/dyn/mitmachen", {}, ["fortschritt"])


def _makeStaticDir():
	# a helper resource for ExternalStaticTest, cleaned up using atexit
	staticDir = os.path.join(os.environ["HOME"], "static")
	os.makedirs(staticDir, exist_ok=True)
	dir = tempfile.TemporaryDirectory(dir=staticDir)
	with open(os.path.join(dir.name, "index.html"), "w") as f:
		f.write('<html class="test"/>\n')
	with open(os.path.join(dir.name, "other.txt"), "w") as f:
		f.write("A, B, C\n")

	def cleanup():
		dir.cleanup()
		try:
			os.rmdir(staticDir)
		except IOError:
			pass
	atexit.register(cleanup)

	return dir.name.split("/")[-1]

_STATIC_DIR = _makeStaticDir()

class ExternalStaticTest(PagesTest):
	def testCustomStaticRetrieval(self):
		return self.assertGETHasStrings("/static/"+_STATIC_DIR, {}, [
			'<html class="test"/>'])

	def testCustomStaticRetrievalWithSlash(self):
		return self.assertGETHasStrings("/static/"+_STATIC_DIR+"/", {}, [
			'<html class="test"/>'])

	def testCustomStaticRetrievalOther(self):
		return self.assertGETHasStrings("/static/"+_STATIC_DIR+"/other.txt", {}, [
			'A, B, C'])

	def testNoTraversal(self):
		return self.assertGETHasStrings("/static/"+_STATIC_DIR+"../../.ssh", {}, [
			"You're not serious, are you?</p>"])


class RenderTest(PagesTest):
	"""tests for some aspects of rendering events in HTML.
	"""
	def testPlaceExpansionLiteral(self):
		def testExpanded(ignored):
			return self.assertGETHasStrings("/event/1111", {},
				["Cherry Lane,",  '<a href="http://www.'])
		return self.assertPOSTHasStrings("/edit/1111", {
				"__nevow_form__": "termedit", "teaser": "a", "bdate": "1",
				"place": "_test"}, []).addCallback(testExpanded)

	def testPlaceExpansionEmbedded(self):
		def testExpanded(ignored):
			return self.assertGETHasStrings("/event/1111", {},
				["Xanadu</a>, erster Stock"])
		return self.assertPOSTHasStrings("/edit/1111", {
				"__nevow_form__": "termedit", "teaser": "a", "bdate": "1",
				"place": "@_test, erster Stock"}, []).addCallback(testExpanded)

	def testStylesheetReplacement(self):
		return self.assertGETHasStrings("/list", {
			"style": '"><script language="text/javascript'},
			['href="&quot;&gt;&lt;script language=&quot;tex'])
	
	def testMoreLink(self):
		def check(ignored):
			return self.assertGETHasStrings("/list", {
				"startDate": "1990-01-01",
				"endDate": "1990-01-02"},
				['Mehr]</a>'])
		return self.assertPOSTHasStrings("/edit/1111", {
				"__nevow_form__": "termedit", "teaser": "a", "bdate": "1.1.1990",
				"fulltext": "test"}, []).addCallback(check)

	def testNoMoreLink(self):
		def check(ignored):
			return self.assertGETLacksStrings("/list", {
				"startDate": "1990-01-01",
				"endDate": "1990-01-02"},
				['1111">[Mehr]'])
		return self.assertPOSTHasStrings("/edit/1111", {
				"__nevow_form__": "termedit", "teaser": "a", "bdate": "1.1.1990",
				"fulltext": ""}, []).addCallback(check)


class EventLifeTest(PagesTest):
	"""tests for creation and deletion of events.
	"""
	def _runADeletionTest(self, deleteFunction):
		def testDeleted(res, id):
			self.assertEqual(res[1].code, 302)
			self.assertTrue(res[1].getLocationValue().endswith("/list"))
			curs = SERVER.conn.execute("SELECT bdate FROM events WHERE id=?", (id,))
			self.assertEqual(len(curs.fetchall()), 0)

		def testCreated(res):
			self.assertEqual(res[1].code, 302)
			editURL = res[1].getLocationValue()
			id = int(editURL.split("/")[-1])
			curs = SERVER.conn.execute("SELECT bdate FROM events WHERE id=?", (id,))
			self.assertEqual(len(curs.fetchall()), 1)
			return deleteFunction(id
			).addCallback(testDeleted, id)

		return self.runQuery(SERVER, "POST", "/edit", {},
		).addCallback(testCreated)

	def testCreationAndDeletionDelete(self):
		"""delete using DELETE method.
		"""
		def deleteEvent(id):
			return self.runQuery(SERVER, "DELETE", "/event/%d"%id,
				{"isSure": "Yes"})
		return self._runADeletionTest(deleteEvent)

	def testCreationAndDeletionPost(self):
		"""delete using POST method.
		"""
		def deleteEvent(id):
			return self.runQuery(SERVER, "POST", "/event/%d"%id,
				{"isSure": "Yes", "method": "DELETE"})
		return self._runADeletionTest(deleteEvent)


class FakedHostNameTest(PagesTest):
	"""tests for configured hostname being used in redirects.
	"""
	def tearDown(self):
		SERVER.conn.stconf.rmKey("hostName")

	def setUp(self):
		SERVER.conn.stconf.addKeyValue("hostName", "flatula")

	def testRedirectOnHostname(self):
		def checkLocation(res):
			req = res[1]
			self.assertEqual(
				req.getLocationValue(),
				"http://flatula:8888/event/2")

		return self.assertGETHasStrings("/event/2", {}, [
			", your browser has ignored a redirect.  Thus,"
		]).addCallback(checkLocation)
	

class TestDayList(PagesTest):
	"""tests for path-pages event lists.
	"""
	def testPlain(self):
		return self.assertGETHasStrings("/list/day/17/02/2010", {}, [
			"Super-VA", "17.02.2010"])

	def testPlainSingle(self):
		return self.assertGETHasStrings("/list/day/17", {}, [
			"Super-VA", "17.02.2010"])

	def testUnicode(self):
		return self.assertGETHasStrings("/list/day/16. März", {}, [
			'teaser">Märzhase', "Dienstag, 16.03."])
	
	def testBizarre(self):
		return self.assertGETHasStrings("/list/day/35", {}, [
			"Nicht gefunden", "Kein Datum"])
	
	def testToday(self):
		return self.assertGETHasStrings("/list/day", {}, [
			"Termine f", "14.02.2010"])


class MultidayTest(PagesTest):
	"""tests for rendering and editing of multi-day events.
	"""
	def testShortRender(self):
		return self.assertGETHasStrings("/", {}, [
			'class="date">12.02-16.02.2010</s'])
	
	def testLongRender(self):
		return self.assertGETHasStrings("/event/6", {}, [
			'Dienstag', 'bis', 'Montag', 'Lasst uns reden'])
	
	def testEdit(self):
		def checkInList(ignored):
			return self.assertGETHasStrings("/", {}, [
				'class="date">01.02-28.02.2010</s'])

		return self.runQuery(SERVER, "POST", "/edit/1111", {
			"__nevow_form__": "termedit", "teaser": "Februar",
			"bdate": "28.2.", "fdate": "1.2."},
		).addCallback(checkInList)


class CloneUserTest(PagesTest):
	sponsor = None

	def testNoUserCloning(self):
		return self.assertStatus("/edit/1/clone", 403)


class CloneTest(PagesTest):
	def _checkTwice(self, result):
		self.assertTrue(re.search(b"(?s)Super-VA.*Super-VA", result[0]))
		# screenscrape delete link (ok, ok...)
		mat = re.search(rb'(?s)Super-VA.*?href="(/edit/\d\d+)"', result[0])
		deleteLink = mat.group(1).replace(b"edit", b"event")
		return self.runQuery(SERVER, "DELETE", deleteLink, {"isSure": "yes"})

	def _checkInList(self, ignored):
		# "Super-VA" twice in data...
		return self.runQuery(SERVER, "GET", "/list", {},
			).addCallback(self._checkTwice)
	
	def _checkCloned(self, res):
		content, req = res
		self.assertEqual(req.code, 302)
		nxURL = req.getLocationValue()
		return self.assertGETHasStrings(nxURL, {}, [
			'curid">126615', # ok unless tests consume several thousand ids
			'17.02.2010',
			'Super-VA']
		).addCallback(self._checkInList)

	def testClone(self):
		return self.runQuery(SERVER, "GET", "/edit/1/clone", {},
			).addCallback(self._checkCloned)

	def _assertNonArchived(self, ignored):
		curs = SERVER.conn.execute("SELECT id, bdate FROM events WHERE teaser=?",
			("Vergangenes",))
		res = list(curs)
		self.assertEqual(len(res), 1)
		ev = sofoterm.Event.fromId(res[0][0], SERVER.conn)
		ev.delete(SERVER.conn)

	def _checkArchiveCloned(self, res):
		content, req = res
		self.assertEqual(req.code, 302)
		nxURL = req.getLocationValue()
		return self.assertGETHasStrings(nxURL, {}, [
			'curid">126615', # ok unless tests consume several thousand ids
			'09.02.2010',
			'Vergangenes']
		).addCallback(self._assertNonArchived)
	
	def testCloneArchive(self):
		return self.runQuery(SERVER, "GET", "/edit/4/clone", {},
			).addCallback(self._checkArchiveCloned)
	
	def testBadMoreDates(self):
		return self.assertPOSTHasStrings("/edit/1", {
			"__nevow_form__": "moreDates", "moreterms": "3\nmuaks"},
				["Zeile 2: Kein Datum zu erkennen.", "action="])

	def _removeNew(self, result):
		newIds = [int(s.group(1))
			for s in re.finditer(rb'href="/edit/(\d\d+)"', result[0])]
		# can't be bothered to figure out sqlite's set adaption now
		cursor = SERVER.conn.execute("DELETE FROM events WHERE id IN (%s)"%
			", ".join(str(i) for i in newIds))
		self.assertEqual(cursor.rowcount, 3)
			
	def testOkMoreDates(self):
		return self.assertPOSTHasStrings("/edit/1", {
			"__nevow_form__": "moreDates", "moreterms": "Dienstag\n 22\n4.3., 12 Uhr "},
				["17.02.", "23.02.", "04.03.", "22.03."]
			).addCallback(self._removeNew)


class TextTest(PagesTest):
	"""tests for rendering text lists.
	"""
	def _checkText(self, res):
		content, req = res
		self.assertEqual(req.getResponseHeader("Content-Type"),
			"text/plain;charset=utf-8")
		self.assertStringsIn(res, [
			'* Denkwürdiges',
			'>>> Freitag, 19.02.2010 <<<',
			'* 20.00, Autonomes Zentrum: Wir gedenken\n'
			'  http://localhost:8888/event/3',
			])

	def testBasic(self):
		return self.runQuery(SERVER, "GET", "/list/text", {}
		).addCallback(self._checkText)


class OptionsTest(PagesTest):
	"""tests for setting options.
	"""
	def testAbsoluteURLs(self):
		from twisted.web import resource
		from sofoterm.pages import pg_dyn

		template = nevowc.XMLString('<div xmlns:n="http://nevow.com/ns/nevow/0.1"'
			' xmlns="http://www.w3.org/1999/xhtml">'
			'<n:invisible n:render="setOptions useFullURLs=true"/>'
			'<ul n:data="eventlist nDays=30" n:render="sequence">'
			'<li n:pattern="item">'
			'<ul n:data="1" n:render="sequence">'
			'<li n:pattern="item" n:render="listshort"/></ul></li></ul></div>')

		class MyServer(resource.Resource):
			def getChild(self, name, request):
				request.isSponsor = False
				return pg_dyn.DynPage(template, SERVER.conn, request)

		def _checkAbsURL(res):
			self.assertStringsIn(res, [
				' href="http://localhost:8888/event/3"'])

		return self.runQuery(MyServer(), "GET", "/knabber", {}
		).addCallback(_checkAbsURL)

	def testPlain(self):
		return self.assertGETLacksStrings("/list", {"plain": "True"},
			['<a href="/config">Eigene Kalender</a>'])

	def testFullURL(self):
		return self.assertGETHasStrings("/list", {"useFullURLs": "True"}, [
			'href="/config">Eigene', # That sucks.  Do we want to change it?
			'class="toolbutton" href="http://localhost:8888/event/3">'])

	def testPaper(self):
		return self.assertGETHasStrings("/list", {"paper": "True"}, [
			' class="fullink"', 'http://localhost:8888/event/2</a>'])
	
	def testBreakout(self):
		def assertNoBodies(res):
			self.assertFalse(b'<div class="event-description">' in res[0],
				"Bodies with flags=breakout")
			return res
		return self.assertGETHasStrings("/list", {"breakout": "True"}, [
			' class="toolbutton" href="/event/2" target="_top">[Mehr]</a>']
			).addCallback(assertNoBodies)

	def testBreakoutAndPaper(self):
		return self.assertGETHasStrings("/list",
			{"paper": "True", "breakout": "True"}, [
			' class="fullink" href="http://localhost:8888/event/2" target="_top">http://localhost:8888/event/2</a>'])
	
	def testStrippedRender(self):
		return self.assertGETLacksStrings("/list", {
			"plain": 'logisch'},
			['id="body"'])


class SubscribeTest(PagesTest):
	"""tests for the subscription page.
	"""
	def testForm(self):
		return self.assertGETHasStrings("/dyn/subscribe", {}, [
			'action="/subscribe'])

	def testWrongCha(self):
		return self.assertPOSTHasStrings("/subscribe", {
			"cha": "wrong", "email": "foo@bar"}, [
			"Du musst sofo ins textcha schreiben"])

	def testRightCha(self):
		SERVER.conn.stconf.clearCaches()
		SERVER.conn.stconf.setUniqueValue("mailmanAPI", "TEST INSTRUMENTATION")

		return self.assertPOSTHasStrings("/subscribe", {
			"cha": "sofo", "email": "foo@bar"}, [
			"<strong>just testing (and breaking)</strong></p>"])


class ToplevelPagesTest(PagesTest, metaclass=testhelpers.SamplesBasedAutoTest):
	"""tests for rendering of some important pages.
	"""

	def _runTest(self, args):
		path, expected = args
		return self.assertGETHasStrings(path, {}, expected)
	
	samples = [
		('/list', ['class="dayhead">Freitag, 19.02', "Datenschutz"]),
		('/config', ["keine Titelleiste", _("umgezogen").encode("utf-8")]),
		('/dyn/about', ["Heidelberger", "Datenschutz"]),
		('/list/text', ['>>> Freitag, 19.02.2010 <<<', 'http://localhost:8888/event/3']),
		('/dyn/unimut', ['id="event-container"']),
#05
		('/dyn/mitmachen', ['Mitmachen!']),
		('/cal', ["Februar 2010", "Montag", "Kalender für Februar"]),
		('/cal/2010/2', ["März 2010", "Montag", "Kalender für Feburar"]),
		('/cal/2010/3', ["März 2010", "Montag", "Kalender für März"]),
		('/cal/2010', ["There is no such calendar"]),
#10
		('/cal/naserumpf/5', ["There is no such calendar"]),
		]


class ModMetaDataTest(PagesTest):
	"""tests for setting modification dates and sponsors.
	"""
	sponsor = "tester"
	localNow = datetime.datetime(2010,3,17,13,2,2)

	def setUp(self):
		self.originalNow = basecommon.now()
		basecommon.setupDebug(self.localNow)

	def tearDown(self):
		from sofoterm.base import common
		basecommon.setupDebug(self.originalNow)

	def _assertLastModified(self, res):
		content, request = res
		self.assertEqual(request.getResponseHeader("Last-Modified"),
			'Wed, 17 Mar 2010 12:02:02 GMT')
		return res

	def _assertMetaChanged(self, res):
		self.sponsor = "tester"
		from sofoterm.base import common
		ev = sofoterm.Event.fromId(1111, SERVER.conn)
		self.assertEqual(ev.lastChanged, basecommon.now(utc=True).timestamp())
		self.assertEqual(ev.lastChanger, "pfurzner")
		self.assertEqual(ev.sponsor, "Test")
		return self.runQuery(SERVER, "GET", "/event/1111", {}
		).addCallback(self._assertLastModified)

	def testChangedMeta(self):
		self.sponsor = "pfurzner"
		return self.assertPOSTHasStrings("/edit/1111",
			{"teaser": "a", "bdate": "13",
			"__nevow_form__": "termedit"}, [
			"unlogwidget", 'teaser">a</span>'
		]).addCallback(self._assertMetaChanged)

	def _assertReturns304(self, res):
		content, req = res
		self.assertEqual(req.code, 304)
		return res

	def _assertReturnsFullDoc(self, res):
		content, req = res
		self.assertEqual(req.code, 200)
		self.assertStringsIn(res, ['"teaser">Wir gedenken'])
		return res

	def testIfNotModified(self):
		return self.runQuery(SERVER, "GET", "/event/3", {}, moreHeaders={
			'if-modified-since': 'Sun, 14 Feb 2010 13:05:10 GMT'}
		).addCallback(self._assertReturns304)

	def testIfModified(self):
		return self.runQuery(SERVER, "GET", "/event/3", {}, moreHeaders={
			'if-modified-since': 'Sun, 14 Feb 2010 13:04:01 GMT'}
		).addCallback(self._assertReturnsFullDoc)


class CachingTest(PagesTest):
	"""tests for delivering pages from the cache and clearing the cache.
	"""
	sponsor = None

	def testFromCacheDeliver(self):
		return self.runQuery(SERVER, "GET", "/", {},
		).addCallback(self._runCachedTest)

	def _runCachedTest(self, res):
		return self.runQuery(SERVER, "GET", "/", {},
		).addCallback(self._testFromCache)

	def _testFromCache(self, res):
		content, request = res
		self.assertTrue(request.getResponseHeader("X-Cache-Creation"))
		return self.runQuery(SERVER, "GET", "/", {},
			moreHeaders={'If-Modified-Since':
				base.formatHTTPDate(request.lastModified)}
		).addCallback(self._testNotModified)

	def _testNotModified(self, res):
		content, request = res
		self.assertEqual(request.code, 304)
		self.assertEqual(content, b"")
		return self.runQuery(SERVER, "POST", "/edit/1111",
			{"teaser":"a", "bdate": "4",
			"__nevow_form__": "termedit"}, sponsor="potz"
		).addCallback(self._runNotCachedTest)
	
	def _runNotCachedTest(self, res):
		return self.runQuery(SERVER, "GET", "/", {},
		).addCallback(self._testNotFromCache)

	def _testNotFromCache(self, res):
		content, request = res
		self.assertFalse(request.getResponseHeader("x-cache-creation"))
		return res


class TageditTest(PagesTest):
	# largely another endless macro test; sorry about that, but this is all
	# about state.

	def testCreationAndDeletion(self):
		return self.assertGETHasStrings("/tagedit", {},
			['<form action="tagedit/add']
			).addCallback(self._runCreation)
	
	def _runCreation(self, ignored):
		return self.runQuery(SERVER, "POST", "/tagedit/add", {"tag": "hypatia"},
			).addCallback(self._assertRedirection
			).addCallback(self._runAnotherCreation)

	def _assertRedirection(self, res):
		self.assertEqual(res[1].code, 302)
		self.assertEqual(res[1].getLocationValue(),
			"http://localhost:8888/tagedit")

	def _runAnotherCreation(self, ignored):
		return self.runQuery(SERVER, "POST", "/tagedit/add",
			{"tag": "pöbel".encode("utf-8")},
			).addCallback(self._assertRedirection
			).addCallback(self._runTagsPresentTest)

	def _runTagsPresentTest(self, ignored):
		return self.assertGETHasStrings("/tagedit", {},
			["<li>hypatia", "pöbel".encode("utf-8"),
				'href="/tagedit/remove/hypatia"',
				'href="/tagedit/remove/p%C3%B6bel"']
			).addCallback(self._runPostDeletion
			).addCallback(self._runTagsAbsentTest)
	
	def _runPostDeletion(self, ignored):
		return self.runQuery(SERVER, "POST", "/tagedit/remove/hypatia", {}
			).addCallback(self._assertRedirection
			).addCallback(self._runGetDeletion)

	def _runGetDeletion(self, ignored):
		return self.runQuery(SERVER, "GET", "/tagedit/remove/p%C3%B6bel", {}
			).addCallback(self._assertRedirection)

	def _runTagsAbsentTest(self, ignored):
		return self.assertGETLacksStrings("/tagedit", {},
			["<li>hypatia", "pöbel".encode("utf-8"),
				'href="/tagedit/remove/hypatia"',
				'href="/tagedit/remove/p%C3%B6bel"'])


_LONG_RST = """
Some text
---------

This has `links`_ & other fancy formatting.  It even features a \\.

* bullet lists, for sure
* **boldface**, too

.. pull-quote::

	Wäre es da nicht besser, die Regierung
	setzte das Volk ab und wählte ein anderes?

.. _links: http://foo.bar?a=b&c=d
"""


class ICalTest(PagesTest):
	sponsor = None
	def testBasic(self):
		def _assertOthers(res):
			# no private events by default
			self.assertFalse(b"Geheimtreffen" in res[0])
			self.assertEqual(res[1].getResponseHeader("content-type"),
				"text/calendar;charset=utf-8")
			return res

		return self.assertGETHasStrings("list/ical", {}, [
			"BEGIN:VCALENDAR", 'SUMMARY:"Denkwürdiges',
			"DTSTAMP:20100214T130500Z",
			"DTSTART:20100219T190000Z", # relevant: UTC transformation:
				# "wir gedenken" starts at 20:00 CET.
			]).addCallback(
			_assertOthers)
	
	def testTagsAndComplexRST(self):
		newev = model.TaggedEvent.createWith("Test", SERVER.conn,
			bdate=base.now()+datetime.timedelta(days=2),
			teaser="Ein Event mit langem Text",
			fulltext=_LONG_RST,
			private=True,
			tags=["für-ical"])
		newev.compileFulltext().toDB(SERVER.conn)

		def cleanup(res):
			newev.delete(SERVER.conn)
			return res

		def assertTagsHonoured(res):
			# that's the "wir gedenken" timestamp I'm testing for in testBasic
			# (which should be deselected by the tag)
			self.assertFalse(b"DTSTART:20100219T190000Z" in res[0])
			return res

		return self.assertGETHasStrings("list/ical", {"tag": "für-ical"}, [
			r'DESCRIPTION:"\nSome text\n---------\n\nThis has `links`_ & other fa',
			r"a \\.",
			'URL:"http://localhost:8888/event/',
		]).addCallback(assertTagsHonoured
		).addBoth(cleanup)


class ExtPagesTest(PagesTest):
	def testInvalidExtension(self):
		return self.assertGETHasStrings("ext/blabla", {},
			["No such extension registered.", "klicken!</p>"])
	

# Tests for various extensions.
# It *would* be nice if extensions could define trial tests as they can
# define normal unit tests.  But until there are out-of-tree extensions
# I think it's not worth the effort.

class ShorturlTest(PagesTest):
	def testNonExisting(self):
		return self.assertGETHasStrings("s/blabla", {},
			["'blabla' is not a shortened path id"])

	def _assertRedirects(self, res):
		self.assertEqual(res[1].getLocationValue(), "http://localhost:8888/4")
		return res

	def _assertResolves(self, res):
		evURL = res[0].split(b"Termin:")[-1].strip()
		nextPath = urllib.parse.urlparse(evURL).path
		return self.assertGETHasStrings(
			nextPath, {},
			['<a href="http://localhost:8888/4">']
			).addCallback(self._assertRedirects)

	def testCreationAndResolution(self):
		return self.assertGETHasStrings("s", {"q": "4"},
			["Kurze URL für diesen Termin: http://localhost:8888/s/1"]
			).addCallback(self._assertResolves)

	def testRes(self):
		return self.assertGETHasStrings("s", {},
			["Servermaschinerie: No short URL"])


class RSSTest(PagesTest):
	def _assertReturns304(self, res):
		self.assertEqual(res[1].code, 304)
		return res

	def _assertCached(self, res):
		return self.runQuery(SERVER, "GET", "/rss", {}, moreHeaders={
			'if-modified-since': base.formatHTTPDate(res[1].lastModified)}
		).addCallback(self._assertReturns304)

	def _assertMediaType(self, res):
		self.assertEqual(res[1].getResponseHeader("Content-Type"),
			"application/atom+xml")
		return res

	def testBasic(self):
		return self.assertGETHasStrings("/rss", {}, [
			"<ns0:title>rss:title needs to be configured</ns0:title>",
			'<ns0:content type="html">&lt;p&gt;Na ja, da war mal was.&lt;/p&gt;',
			]).addCallback(self._assertMediaType
			).addCallback(self._assertCached)
	
	def testDateCheat(self):
		return self.assertGETHasStrings("/rss", {"cheat_date": "1"}, [
			# That's event 1's bdate, just as the update time (which
			# is what the date cheat is about.
			"<ns0:updated>2010-02-17T11:55:00Z</ns0:updated>"
		])

	def _assertNoOtherEvents(self, res):
		self.assertTrue(b"Wir gedenken" not in res[0])
		return res

	def testSelection(self):
		return self.assertGETHasStrings("/rss", {"tag": "geheim"}, [
			"<ns0:title>24.2.2010 &#8211; Geheimtreffen</ns0:title>"]
			).addCallback(self._assertNoOtherEvents)


class MemOTDPage(PagesTest):
	def testInSidebar(self):
		return self.assertGETHasStrings("/", {}, [
			'<div id="memOTD"', '<strong>vor 10 Jahren</strong>',
			'<p>In zehn Jahren wird getestet.</p>'])
	
	def testInRSS(self):
		return self.assertGETHasStrings("/rss", {}, [
			'<ns0:content type="html">&lt;p&gt;vor 10 Jahren &#8211;'])
	
	def testMemPageToday(self):
		# no fixed date for "today"
		return self.assertGETHasStrings("/ext/memForDate", {}, [
			'<h1>Am 14.2.2010…</h1>\n\t<div class_="dates">\n\t\t\n\t</div>'])

	def testMemPageWithDate(self):
		return self.assertGETHasStrings("/ext/memForDate",
			{"year": "2005", "month": "2", "day": "8"}, [
			'<div class="memforday"><p>Wie schon <a class="reference external" href="/ext/memForDate?day=14&amp;month=2&amp;year=2000">14.2.2000</a> hat jemand gegähnt.</p>'])

	def testMemOTDPageToday(self):
		return self.assertGETHasStrings("/ext/memOTD", {}, [
			'<input type="text" name="day" size="2" value="14">',
			'…<strong>vor 10 Jahren</strong> – 14.2.2000: <p>In zehn'
			' Jahren wird getestet.</p>'])

	def testMemOTDPageDiff0(self):
		return self.assertGETHasStrings("/ext/memOTD",
			{"year": "2005", "month": "2", "day": "8"}, [
			"…<strong>passierte</strong> – 8.2.2005:"])

	def testMemOTDPageDiffLastYear(self):
		return self.assertGETHasStrings("/ext/memOTD",
			{"year": "2004", "month": "2", "day": "8"}, [
			"…<strong>in einem Jahr</strong> – 8.2.2005:"])

	def testMemOTDPageDiffNextYear(self):
		return self.assertGETHasStrings("/ext/memOTD",
			{"year": "2006", "month": "2", "day": "8"}, [
			"…<strong>vor einem Jahr</strong> – 8.2.2005:"])

	def testMemOTDPageDiff10YearsAgo(self):
		return self.assertGETHasStrings("/ext/memOTD",
			{"year": "1995", "month": "2", "day": "8"}, [
			"…<strong>in 10 Jahren</strong> – 8.2.2005:"])
	
	def testNoMemory(self):
		restoreTo = base.now()
		basecommon.setupDebug(datetime.datetime(2010, 2, 15))
		return self.assertGETLacksStrings("/ext/memOTD", {}, [
			'id="memOTD"']
			).addBoth(lambda res: basecommon.setupDebug(restoreTo) or res)

	def testAnniversaryMemory(self):
		restoreTo = base.now()
		basecommon.setupDebug(datetime.datetime(1995, 2, 8))
		return self.assertGETHasStrings("/ext/memOTD", {}, [
			'14.2.2000</a> hat jemand gegähnt.</p>'],
			).addBoth(lambda res: basecommon.setupDebug(restoreTo) or res)


class SearchTest(PagesTest):
	def testDescriptionMatch(self):
		return self.assertPOSTHasStrings("/search",
			{"q": "reden", "__nevow_form__": "fulltext"}, [
				'<a href="/event/6">',
				'Debattenwoche',
				'<span class="snippet">Lasst uns ❯reden❰</span>',
				'id="fulltext-q-field"',
				'(2010-06-14)'])
	
	def testTitleMatch(self):
		return self.assertGETHasStrings("/search",
			{"q": "denkwürdig*", "__nevow_form__": "fulltext"}, [
				'<a href="/event/2">',
				"Denkwürdiges", "(2010-02-19)"])
	
	def testNoMatch(self):
		return self.assertPOSTHasStrings("/search",
			{"q": "knürzgrubel*", "__nevow_form__": "fulltext"}, [
				'<p class="search-message">Keine Termine'])
