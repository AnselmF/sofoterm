"""
Tests for (some of) our special renderers.
"""

import datetime

from twisted.web import template
from twisted.web.template import tags as T

import sofoterm
from sofoterm import formal
from sofoterm.formal import nevowc
from sofoterm.model import event as mevent
from sofoterm.pages import common as pgcommon
from sofoterm.pages import event as pgevent

import testhelpers


class EventRenderersTest(testhelpers.VerboseTest):

	def _getSampleEvent(self):
		return sofoterm.Event(id="abc", bdate=datetime.date(2003, 2, 3),
			btime=datetime.time(12, 00),
			teaser="Sample plays ex", sponsor="God")

	def _render(self, tpl, ev=None):
		if ev is None:
			ev = self._getSampleEvent()

		class Pg(pgevent._EventFragment):
			loader = template.TagLoader(tpl)

		frag = Pg(ev, self)
		return nevowc.flattenSyncToString(frag)
	
	def testItemsRendered(self):
		self.assertEqual(
			self._render(T.div(render="ifany btime attachments")["a", "b", "c"]),
			"<div>abc</div>")

	def testItemsNotRendered(self):
		self.assertEqual(
			self._render(T.p["Nix: ",
				T.span(render="ifany fulltext fdate")["Falsch"]]),
			"<p>Nix: </p>")

	def testGeoPlaceWithOSM(self):
		ev = self._getSampleEvent().change(place="gegendruck")
		self.assertEqual(
			self._render(T.span(class_="place", render="place"), ev),
			'<span class="place">'
			'<a href="http://www.openstreetmap.org/?zoom=17&amp;mlon=8.7104'
			'&amp;mlat=49.4131" title="Location in OSM" class="osmlink">'
			'<img class="osmlink" alt="[M]" src="/static/osmlink.png" /></a>'
			'Cafe Gegendruck, Fischergasse 2</span>')

	def testGeoPlaceWithoutOSM(self):
		ev = self._getSampleEvent().change(place="gegendruck")
		self.assertEqual(
			self._render(T.span(class_="place", render="placeWithoutIcon"), ev),
			'<span class="place"><i class="icon place"></i>Cafe Gegendruck, Fischergasse 2 <a'
			' href="http://www.openstreetmap.org/?zoom=17&amp;mlon=8.7104'
			'&amp;mlat=49.4131">[Karte]</a></span>')

	maxDiff = 1000
	def testNonGeoPlace(self):
		ev = self._getSampleEvent().change(place="_test")
		self.assertEqual(
			self._render(T.ul[T.li(render="placeWithoutIcon"),
				T.li(render="place")], ev), '<ul>'
				'<li><i class="icon place"></i>Cherry Lane, <a href="http://www.xanadu.org">Xanadu</a></li>'
				'<li><i class="icon place"></i>Cherry Lane, <a href="http://www.'
				'xanadu.org">Xanadu</a></li></ul>')

	def testNoPlace(self):
		self.assertEqual(
			self._render(T.ul[T.li(render="placeWithoutIcon"),
				T.li(render="place")]),
			'<ul></ul>')

	def testAtPreserved(self):
		ev = self._getSampleEvent().change(place="hut@titiwu")
		self.assertEqual(
			self._render(T.p(render="placeWithoutIcon"), ev),
			'<p><i class="icon place"></i>hut@titiwu</p>')

	def testRenderSimpleEvdate(self):
		ev = self._getSampleEvent()
		self.assertEqual(
			self._render(T.div(render="date"), ev),
			'<div><i class="icon date"></i><span class="weekday">Montag</span><span class="separator">, </span><span class="date">03.02.2003 </span></div>')

	def testRenderRangedEvdate(self):
		ev = self._getSampleEvent().change(fdate=datetime.datetime(2003, 1, 7))
		self.assertEqual(
			self._render(T.div(render="date"), ev),
			'<div><i class="icon date"></i><span class="weekday">Dienstag</span><span class="separator">, </span><span class="date">07.01.2003 </span><span class="joiner"> bis </span><span class="weekday">Montag</span><span class="separator">, </span><span class="date">03.02.2003 </span></div>')

	def testNoAttachmentLink(self):
		self.assertEqual(
			self._render(T.a(render="attachmentLink")["Anhänge"]),
			"")

	def testAttachmentLink(self):
		ev = self._getSampleEvent().change(attachments=1)
		self.assertEqual(
			self._render(T.a(render="attachmentLink")["Anhänge"], ev),
			'<a href="/event/abc/attached.pdf">Anhänge</a>')

	def testLogos(self):
		# this will fail if executed outside of tests
		with testhelpers.testFile("sometag.png",
				b"Fako.",
				inDir="../sofoterm/static/logos"):
			ev = self._getSampleEvent()
			ev.tags = {"sometag"}
			self.assertEqual(
				self._render(T.ul(render="logos"), ev),
				'<ul><li class="logoitem"><img src="/static/logos/sometag.png" /></li></ul>')

	# for testMoreLinkX
	@template.renderer
	def proclink(self, request, tag):
		return tag

	# dito
	def makeURL(self, path):
		return path

	def testMoreLinkPureURL(self):
		ev = self._getSampleEvent().change(fulltext="http://foo.bar/baz")
		self.assertEqual(
			self._render(T.a(render="moreLink")["[Mehr]"], ev),
			'<a href="http://foo.bar/baz">[Mehr]</a>')

	def testMoreLinkAttachment(self):
		ev = self._getSampleEvent().change(attachments=1)
		self.assertEqual(
			self._render(T.a(render="moreLink")["[Mehr]"], ev),
			'<a href="/event/abc/attached.pdf">[Mehr]</a>')

	def testMoreLinkFulltext(self):
		ev = self._getSampleEvent().change(fulltext="Es war einmal")
		self.assertEqual(
			self._render(T.a(render="moreLink")["[Mehr]"], ev),
			'<a href="/event/abc">[Mehr]</a>')

	def testMoreLinkNoMore(self):
		self.assertEqual(
			self._render(T.a(render="moreLink")["[Mehr]"]),
			'')

	def testComplete(self):
		self.assertEqual(
			self._render([
				T.transparent(render="ifcomplete")["Vollständig"],
				T.transparent(render="ifincomplete")["Unvollständig"]]),
			'Vollständig')

	def testIncomplete(self):
		ev = self._getSampleEvent().change(teaser=None)
		self.assertEqual(
			self._render([
				T.transparent(render="ifcomplete")["Vollständig"],
				T.transparent(render="ifincomplete")["Unvollständig"]], ev),
			'Unvollständig')



if __name__=="__main__":
	testhelpers.main(EventRenderersTest)
