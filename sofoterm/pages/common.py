"""
Basic classes for sofoterm pages.
"""

import base64
import datetime
import functools
import os
import re
import urllib.parse

from twisted.web import http
from twisted.web import static
from twisted.web import template
from twisted.web import util
from twisted.web.template import tags as T

from sofoterm import base
from sofoterm import formal
from sofoterm import model
from sofoterm.base import _
from sofoterm.formal import nevowc


def nullRenderer(pg, request, tag):
	return ""


@functools.lru_cache(maxsize=None)
def getLogoURL(tagName):
	"""returns a path (that serves as a relative URL) to a logo for tagName
	or None if no such logo is known.
	"""
	try:
		resPath = "/static/logos/%s.png"%tagName
		fullPath = base.getResource(resPath[1:])
		if os.path.exists(fullPath):
			return resPath
	except IOError:
		pass
	return None


def establishSponsorness(conn, request):
	"""adds isSponsor and sponsor attributes to the request.

	This attribute is True iff the incoming request as a sponsor cookie
	with a value in conn.stconf.sponsors.
	"""
	request.isSponsor = False
	encSponsor = request.getCookie(b"sponsor")
	if encSponsor:
		sponsor = base64.b64decode(urllib.parse.unquote(
			encSponsor.decode("ascii"))).decode("utf-8")
		request.isSponsor = sponsor in conn.stconf.sponsors
		if request.isSponsor:
			request.sponsor = sponsor
			request.addCookie("sponsor", encSponsor,
				max_age=str(90*3600*24), path="/")


class SimpleRenderMixin(object):
	"""A mixin providing some commonly useful render methods.

	It also contains a "safe-fallback" extension mechanism for render methods.
	The idea is that plugins (and similar) can define render methods and
	add them to classes.  Template pages then say render="ext <name>"
	to call them.  However, if the mehtods are not there, an empty
	string is returned rather than an error.

	The render functions have the signature rnd(pg, request, tag) -> stan,
	i.e., they receive the EventPage instance as the first argument.  They
	don't end up as instance methods, though, in case that matters.
	"""
	style = None
	extRenderFuncs = None

	@classmethod
	def addExtRenderer(cls, name, extRender):
		if cls.extRenderFuncs is None:
			cls.extRenderFuncs = {}
		cls.extRenderFuncs[name] = extRender

	@template.renderer
	def ext(self, name):
		if self.extRenderFuncs is None:
			self.__class__.extRenderFuncs = {}
		func = self.extRenderFuncs.get(name, nullRenderer)
		def render(request, tag):
			return func(self, request, tag)
		return render

	@template.renderer
	def ifdata(self, request, tag):
		if tag.slotData:
			return tag
		else:
			return ""

	@template.renderer
	def doctitle(self, request, tag):
		return tag[_("Veranstaltungskalender in Heidelberg")]

	@template.renderer
	def unicode(self, request, tag):
		return tag[str(tag.slotData)]

	@template.renderer
	def urlencoded(self, request, tag):
		return urllib.parse.quote(str(tag.slotData))

	@template.renderer
	def stdhead(self, request, tag):
		"""adds some standard stuff to our HTML headers.

		Use this as a renderer to head or in an invisible within head.
		"""
		style = self.style
		if not style:
			style = "/default.css"
		return tag[
			T.meta(**{"content": "application/xhtml;charset=utf-8",
			  "http-equiv": "content-type"}),
			T.link(rel="stylesheet", href="/formal.css", type="text/css"),
			T.meta(name="viewport", content="width=device-width, initial-scale=1"),
			T.link(rel="stylesheet", href=style, type="text/css"),
			T.link(render="ext rssLink"),
			T.script(type="text/javascript", src="/static/sofo.js"),
			T.script(type="text/javascript", src="/static/formal.js"),
			]


def _getPlaceExpansion(placeId, includeOSMIcon):
	"""helps renderPlace.

	This assumes placeId actually is a key in model.PLACES.
	"""
	expansion = model.PLACES[placeId]
	if len(expansion)==3:
		name, int, lat = expansion
		osmLink = "http://www.openstreetmap.org/?zoom=17&mlon=%s&mlat=%s"%(
			int, lat)

		if includeOSMIcon:
			return [
				T.a(href=osmLink, title="Location in OSM", class_="osmlink")[
					T.img(
						class_="osmlink",
						alt="[M]",
						src="/static/osmlink.png")],
				T.xml(name)]
		
		else:
			return [T.xml(name), " ", T.a(href=osmLink)["[Karte]"]]

	else:
		# some sort of plain HTML in element 0, ignore anything else
		return [T.xml(expansion[0])]


def renderPlace(placeStr, includeOSMIcon=False):
	"""returns stan for placeStr.

	This means that references to well-known places (in model.PLACES)
	are expanded.  Such a reference can be in the form
	@placename within the text (any @something not matching a place is
	left alone), which is then replaced by the places expansion; a
	standalone reference into model.PLACES is also expanded.
	"""
	if not placeStr:
		return ""
	result, lastPos = [], 0
	
	if placeStr in model.PLACES:
		return renderPlace("@"+placeStr, includeOSMIcon)
	
	for mat in re.finditer("@(\w+)", placeStr):
		result.append(placeStr[lastPos:mat.start()])
		if mat.group(1) in model.PLACES:
			result.extend(_getPlaceExpansion(mat.group(1), includeOSMIcon))
		else:
			result.append(mat.group(0))
		lastPos = mat.end()

	result.append(placeStr[lastPos:])
	# add an icon unless an osm icon is already there
	for el in result:
		if isinstance(el, template.Tag) and el.attributes.get("class")=="osmlink":
			break
	else:
		result[:0] = [T.i(class_="icon place")]

	return result


class EventPage(nevowc.TemplatedPage, SimpleRenderMixin):
	"""A base for pages dealing with events.

	This basically contains some common renderers.

	All EventPages have an attribute conn containing a database
	connection.

	EventPages contain some options that you can fill from request arguments
	or from a template (though that doesn't always make sense) via
	the setOptions renderer.  The options include:

	* style -- a URL for a stylesheet
	* plain -- if given at all, render without a sidebar
	* useFullURLs -- if given at all, make internal URLs absolute
	* breakout -- render date links so they break out of frames;
	  also: very compact format.
	* paper -- render things so they work better when printed

	There's also the sponsor attribute that is filled from a request.
	It is None if there is no request or the request does not originate
	from a sponsor, otherwise it's the sponsor string (that must be
	equal to something in the database).

	For uniform error handling, do not override render but
	define _produceDocument with an identical signature.
	"""
	breakout = False
	useFullURLs = False
	paper = False
	plain = False
	sponsor = None

	changeStamp = None

	def __init__(self, conn, request):
		super(EventPage, self).__init__()
		self.conn, self.request = conn, request
		if request and request.isSponsor:
			self.sponsor = request.sponsor
		if request:
			self._setOptions(request.args)
		else:
			self._setOptions({})

	def _setLastChange(self, changeStamp):
		"""generates a Last-Modified header if possible.

		changeStamp is a unix timestamp in UTC.

		The actual EventPage instances call this with some time info they
		have.
		"""
		if self.request.method!=b"GET" or self.request.isSponsor:
			return
		self.changeStamp = changeStamp
		if changeStamp:
			if self.request.setLastModified(changeStamp)==http.CACHED:
				raise base.NotModified("Stop writing")
				
	def _setOptions(self, rawArgs):
		"""sets the EventPage flags from request rawArgs.

		See class docstring for the flags available.  They are filled
		from the first item in the list for each same-named key in args
		(or just if there's any value at all).

		Contrary to what we do almost everywhere else, rawArgs here is
		the bytes-to-bytes thing we get from twisted.
		"""
		if b"style" in rawArgs and rawArgs[b"style"]:
			self.style = base.debytify(rawArgs[b"style"][0])
		for flagName in [b"plain", b"useFullURLs", b"breakout", b"paper"]:
			if flagName in rawArgs:
				setattr(self, flagName.decode("ascii"), bool(rawArgs[flagName]))
		if self.paper:
			self.useFullURLs = True
			self.plain = True
			self.sponsor = None

	def makeURL(self, path):
		if self.useFullURLs:
			return base.completeURL(path, self.conn)
		else:
			return path

	@template.renderer
	def encoded(self, request, tag):
		# That's a no-op now, as we're all unicode to the last moment in py3
		return tag[tag.slotData]

	@template.renderer
	def setOptions(self, args):
		"""gets render options from data.

		The available options are in the class docstring.  They are
		encoded in URL query strings.
		"""
		self._setOptions(urllib.parse.parse_qs(args.encode("ascii")))
		def render(request, tag):
			return ""
		return render

	@template.renderer
	def dayhead(self, request, tag):
		"""renders a header for a day.

		data must be an item from EventList.iterGrouped.
		"""
		return tag(class_="dayhead")[
			base.weekdays[tag.slotData[0].weekday()],
			", ",
			tag.slotData[0].strftime("%d.%m.")]

	@template.renderer
	def ifsponsor(self, request, tag):
		"""renders everything below this tag only if client is a sponsor
		(i.e., may create articles).
		"""
		if self.sponsor is None:
			return ""
		else:
			return tag

	@template.renderer
	def ifuser(self, request, tag):
		"""the inverse of ifsponsor.
		"""
		if self.sponsor is None:
			return tag
		else:
			return ""

	@template.renderer
	def proclink(self, request, tag):
		"""changes a link according to our options.

		(paper, breakout)
		"""
		if self.paper:
			tag.clear()
			tag[tag.attributes["href"]]
		if self.breakout:
			tag(target="_top")
		return tag

	@template.renderer
	def withsidebar(self, request, tag):
		"""adds the title, sidebar and footer to the three sides of tag.

		This is probably mostly useful on body.  The former content of
		tag ends up in a div of id body.
		"""
		if self.plain:
			return tag
		self._pagecontent = tag.children
		return tag.clear()[self._pageframe[0].children]

	@template.renderer
	def header(self, request, tag):
		return self._header

	@template.renderer
	def sidebar(self, request, tag):
		return self._sidebar

	@template.renderer
	def footer(self, request, tag):
		return self._footer

	@template.renderer
	def pagecontent(self, request, tag):
		return tag[self._pagecontent]

	@template.renderer
	def ifflag(self, flags):
		"""renders everything below this tag only if one of the options
		in flags (space separated) is active.

		Flags defined are breakout, useFullURLs, paper, plain, sponsor.
		"""
		flags = set(flags.split())
		def _(request, tag):
			for f in flags:
				if bool(getattr(self, f)):
					return tag
			return ""
		return _

	@template.renderer
	def ifnoflag(self, flags):
		"""the reverse of ifflag, i.e., render if none of the flags are active.
		"""
		flags = set(flags.split())
		def _(request, tag):
			for f in flags:
				if bool(getattr(self, f)):
					return ""
			return tag
		return _

	@template.renderer
	def subsForm(self, request, tag):
		"""a form leading to a mailman subscription.
		"""
		try:
			return tag[
				T.form(action="/subscribe", method="post",
						class_="small")[
					T.input(type="text", name="email", size="30"),
					"textcha:",
					T.input(type="text", name="cha", size="4"),
					T.input(type="submit", value=_("Abonnieren"))]]
		except KeyError:
			return tag[T.p[_("(Keine Mailingliste definiert, drum"
				" geht auch kein Abo.)")]]

	@template.renderer
	def icalLink(self, request, tag):
		"""returns a link to the current list as iCal.
		"""
		if self.paper:
			return ""
		else:
			return tag(href="/list/ical?"+base.urlencodeArgs(self.args))

	@template.renderer
	def rssLink(self, request, tag):
		"""returns a link to the current list in atom format.
		"""
		if self.paper:
			return ""
		else:
			return tag(href="/rss?"+base.urlencodeArgs(self.args))

	def data_eventlist(self, args):
		"""returns a "custom" event list.

		The argument is a http query string as for the pg_list.
		"""
		def _(request, tag):
			return self.data_eventlist_flat(args)(request, tag).iterGrouped()
		return _

	def data_eventlist_flat(self, args):
		"""returns a "custom" event list.

		The argument is a http query string as for the pg_list.
		"""
		def _(request, tag):
			return model.EventList.fromQueryDict(
					self.conn, urllib.parse.parse_qs(args))
		return _

	def _getMultiOptions(self, request):
		args = base.debytifyArgs(request.args)
		return {
			"sponsor": self.sponsor,
			"wantedTags": args.get("tag", []),
			"unwantedTags": args.get("notag", []),}

	def data_multifuture(self, nDays):
		"""returns an event list for multiday events starting up to nDays in
		the future.
		"""
		def render(request, tag):
			return model.EventList.fromSelectors(self.conn,
				startDate=base.now(),
				maxFirstDay=base.now().date()+datetime.timedelta(days=int(nDays)),
				endDate=base.now().date()+datetime.timedelta(days=int(nDays)),
				**self._getMultiOptions(request))
		return render

	def data_sortedTags(self, request, tag):
		return sorted(self.conn.stconf.tags)

	def render(self, request):
		return self._produceDocument(request)

	def _produceDocument(self, request):
		return super().render(request)

	_header = nevowc.XMLFile(base.getResource("templates/header.html")).load()
	_sidebar = nevowc.XMLFile(base.getResource("templates/sidebar.html")).load()
	_footer = nevowc.XMLFile(base.getResource("templates/footer.html")).load()
	_pageframe = nevowc.XMLFile(base.getResource("templates/pageframe.html")
		).load()


class LoginMixin:
	"""A mixin for pages that (may) want to show the login dialog.

	This only works where the page actually is a ResourceWithForm.
	"""
	processOnGET = True

	def form_login(self, request):
		form = formal.Form()
		form.addField("sponsor", formal.String(required=True),
			label=_("Sponsor-Schlüssel"), description=_("Der magische String,"
			" den ihr von uns bekommen habt.  Tippt irgendeinen Quatsch hier"
			' rein, um "auszuloggen".'))
		form.addAction(self.login_evaluate, label="Ok")
		return form
	
	def login_evaluate(self, request, form, data):
		request.addCookie("sponsor",
			urllib.parse.quote(
				base64.b64encode(data["sponsor"].encode("utf-8")).strip()),
			max_age=str(90*3600*24),
			path="/")
		return util.Redirect(request.uri.split(b"?")[0])


def makeStatic(segments):
	"""returns a static resource.

	The ones required by sofoterm come in via pkg_resources.  You can
	override them or add more in ~/static

	All this logic isn't quite sane, and I don't like the idea that
	it's the single "static" below there that's between a this and
	publishing the entire server user's home to the web.  Hm.
	"""
# For now, they always come form pkg_resources.  Later, probably allow
# to override out of package
	if ".." in segments or "" in segments[:-1]:
		raise base.Forbidden("You're not serious, are you?")

	homeDir = os.environ.get("HOME", "/x-invalid/")
	relPath = os.path.join("static", *segments)

	overriddenPath = os.path.join(homeDir, relPath)
	if os.path.exists(overriddenPath):
		# allow directories in here, but don't bother with dir listings
		if not os.path.isfile(overriddenPath):
			if os.path.exists(os.path.join(overriddenPath, "index.html")):
				return static.File(os.path.join(overriddenPath, "index.html"))
		return static.File(overriddenPath)

	path = base.getResource(relPath)
	if os.path.exists(path):
		return static.File(path)
	else:
		raise base.PageNotFound("Static file not found")


class StaticDir(nevowc.TemplatedPage):
	"""A thing that just publishes a directory tree verbatim.

	This provides virtually no checks except attempting to make sure
	people don't walk up beyond the root of the published tree.

	This thing never renders itself, it only works through locateChild.
	"""
# TODO: At this point we're only using this for vanity, and we
# should use something else for that.
	def __init__(self, treeRoot):
		self.staticPath = treeRoot
		nevowc.TemplatedPage.__init__(self)

	def render(self, req):
		# this just redirects to index.html
		raise base.Redirect(req.uri.decode("utf-8")+"/index.html")

	def getChild(self, name, request):
		# TODO: I should be using request.postpath here
		# so I don't depend on being the immediate child of
		# root.  But for some reason it has too many segments
		# cut off.  Hm.
		relPath = b"/".join(request.uri[1:].split(b"/")[1:]
			).decode("utf-8")
		request.postpath = []

		if relPath in ["", "index.html"]:
			destPath = os.path.join(self.staticPath, "index.html")
			if os.path.exists(destPath):
				return static.File(destPath)
			else:
				raise base.PageNotFound("index.html missing")

		destName = os.path.join(self.staticPath, relPath)

		# make sure destName actually is below staticPath so stupid
		# tricks with .. or similar don't work on us
		if not os.path.abspath(destName).startswith(
				os.path.abspath(self.staticPath)):
			raise base.Forbidden("%s is not located below staticData"%
				destName)

		if os.path.isdir(destName):
			raise base.Redirect(request.uri.decode("utf-8").rstrip("/"
				)+"/index.html")
		elif os.path.isfile(destName):
			return static.File(destName)

		else:
			raise base.PageNotFound("No %s available here."%relPath)
		

def makeSerialLocator(*locators):
	"""returns a locateChild funtion calling locators in turn.

	The first locator that returns non-None wins (and its result is returned).
	If no locator matches, None will be returned.

	We normalize the segments such that there always is at least one item
	present; it is the empty string for originally empty paths.

	Each function in locators has to have the signature
	fun(conn, request, segments) -> resource or None.
	"""
	def locateChild(conn, request, segments):
		if len(segments)==0:
			segments = ("",)
		for locator in locators:
			res = locator(conn, request, segments)
			if res is not None:
				return res
		return None
	return locateChild


def getEvent(conn, id, eventClass=model.TaggedEvent):
	"""returns the event with id from either the archive or the current
	event table.

	id must be something int-able, and unknown events raise EventNotFound
	exceptions.
	"""
	id = int(id)
	try:
		return eventClass.fromId(id, conn)
	except base.EventNotFound:
		return eventClass.fromId(id, conn.getArchiveConnection())
	

def addMonths(year, month, diffMonths):
	"""returns year and month of diffMonths later or earlier.
	"""
	month += diffMonths
	diffYear = (month-1)//12
	year += diffYear
	return year, (month-1)%12+1


def getNthWeekday(year, month, wdn, n):
	"""returns a date instance for the n-th occurrence of weekday wdn in month
	of year.

	wdn=0 is monday.  This raises a ValueError if no such day exists.
	n=1 really means "1st occurrence".  n=0 is not admitted here.
	Negative n count from behind (4, -1 is last Friday of a month).
	"""
	firstWdDay = wdn-datetime.date(year, month, 1).weekday()+1
	if n>0:
		# get the date of the 1st such weekday in the month, then forward
		# n weeks.
		if firstWdDay<=0:
			firstWdDay += 7
		return datetime.date(year, month, firstWdDay+(n-1)*7)

	elif n<0:
		# We first establish experimentally the day of the last such weekday
		# (it's going to be either the 4th or the 5th) then work forward
		# from there
		try:
			lastWdDay = datetime.date(year, month, firstWdDay+4*7)
		except ValueError:
			lastWdDay = datetime.date(year, month, firstWdDay+3*7)
		return datetime.date(year, month, lastWdDay.day+(n+1)*7)

	else:
		raise ValueError(_("Es gibt keine 0-ten Tage."))


def parseFreeformTime(value):
	if value is None or not value.strip():
		return None
	parts = list(map(int, re.sub("[^\d]+", " ", value).strip().split()))
	if len(parts)==1:
		h, m = int(parts[0]), 0
	elif len(parts)==2:
		h, m = list(map(int, parts))
	else:
		raise ValueError("Hier ist keine Zeitangabe zu erkennen")
	try:
		return datetime.time(h, m)
	except ValueError as ex:
		raise ValueError("Zeit aus den Fugen: %s"% str(ex))


# we don't want to rely on locales and correct locale data here
# and rather do these things ourselves (at some point)
monthNames = ["Januar", "Februar", "März",
	"April", "Mai", "Juni", "Juli", "August", "September",
	"Oktober", "November", "Dezember"]

def getMonthName(month):
	return monthNames[int(month)-1]


_monthsInds = {"Nov": 11, "Sept":9, "Okt":10}
_monthsInds.update(dict((n, i+1) for i, n in enumerate(monthNames)))
_monthPat = re.compile("|".join(_monthsInds))


def _replaceMonthStrings(value):
	"""helps parseFreeformDate.
	"""
	return _monthPat.sub(lambda mat: str(_monthsInds[mat.group()]),
		value)


def _parseISODate(value, today):
	"""parses an ISO date.

	A helper for parseFreeformDate.
	"""
	try:
		return datetime.datetime.strptime(value, "%Y-%m-%d").date()
	except ValueError:
		return None  # try next parser


def _parseNumericDate(value, today):
	"""parses seminumeric date specification.

	A helper for parseFreeformDate.
	"""
	value = _replaceMonthStrings(value)
	parts = list(map(int, re.sub("[^\d]+", " ", value).strip().split()))
	if len(parts)==3:
		d, m, y = parts
	elif len(parts)==2:
		d, m = parts
		y = today.year
		if m<today.month:
			y += 1
	elif len(parts)==1:
		d = parts[0]
		m = today.month
		if d<today.day:
			m += 1
		y = today.year
		if m<today.month:
			y += 1
	else:
		return
	if y<1990:
		y += 2000
	return datetime.date(y, m, d)


_WEEKDAYS = {
	'montag': 0, 'dienstag': 1, 'mittwoch': 2, 'donnerstag': 3,
	'freitag': 4, 'samstag': 5, 'sonntag': 6,
	'mo': 0, 'di': 1, 'mi': 2, 'do': 3,
	'fr': 4, 'sa': 5, 'so': 6,
	'monday': 0, 'tuesday': 1, 'wednesday': 2, 'thursday': 3,
	'friday': 4, 'saturday': 5, 'sunday': 6,}


def _parseWeekdayDate(value, today):
	"""parses weekday-type date specs.

	A helper for parseFreeformDate.  If the weekday passed is the current
	weekday, return the next occurrence of that date.
	"""
	value = value.lower()
	if value in _WEEKDAYS:
		diff = _WEEKDAYS[value]-today.weekday()
		if diff<1:
			return today+datetime.timedelta(days=diff+7)
		else:
			return today+datetime.timedelta(days=diff)


def _parseMonthlyWeekday(value, today):
	"""parses specs like Monday*2 (next 2nd monday of the month in the future)
	or Friday*-1 (next last Friday of a month in the future).

	This is used by parseFreeformDate.
	"""
	mat = re.match("(\w+)\*(-?\d+)$", value)
	if mat:
		dayName = mat.group(1).lower()
		if dayName not in _WEEKDAYS:
			raise ValueError(_("Kein bekannter Wochentag in *-Syntax"))
		dayIndex = _WEEKDAYS[dayName]

		count = int(mat.group(2))
		if abs(count)>5 or abs(count)==0:
			raise ValueError(_("Es gibt keine %s-ten Wochentage in Monaten.")%count)
	
		# We allow things like 5th Wednesday that don't occur in every month.
		# Hence, we may have to iterate (but that won't happen for count=1-4)
		curYear, curMonth = today.year, today.month
		for attempt in range(30):
			try:
				newDate = getNthWeekday(curYear, curMonth, dayIndex, count)
				if newDate<=today.date():
					raise ValueError("In the past")
				break
			except ValueError:
				# no count-wd in this month, try next
				curYear, curMonth = addMonths(curYear, curMonth, 1)
		else:
			raise ValueError(_("Kein %s in den nächsten 30 Monaten?")%value)
		
		return datetime.datetime.combine(newDate, today.time())


def parseFreeformDate(value, today=None):
	"""returns a data instance for a "freeform" date input.

	See user docs for legal forms.  The function raises a value
	error if it cannot make out a date.

	To make this match better the formal machinery, it will return None
	on certain recognized null-type values.
	"""
	if today is None:
		today = base.now()
	if value is None or not value.strip():
		return None
	value = value.strip()
	for parser in [
			_parseISODate,
			_parseMonthlyWeekday,
			_parseWeekdayDate,
			_parseNumericDate]:
		res = parser(value, today)
		if res is not None:
			return res
	raise ValueError("Kein Datum zu erkennen.")


def _parseDTSpec(dtLiteral):
	"""returns a pair of dateLiteral, timeLiteral from dtLiteral.

	This is a helper for parseDateList and understands formats given
	there.

	timeLiteral may be None if no time spec can be made out
	"""
	timeLiteral = None
	try:
		dateLiteral, timeLiteral = dtLiteral.split(",")
		timeLiteral = timeLiteral.strip()
	except ValueError:
		dateLiteral = dtLiteral
	return dateLiteral.strip(), timeLiteral


def parseDateList(listLiteral, initDT):
	"""return a list of datetime.datetime instances from a string in value.

	Value contains one datetime spec per line.  initDT is a datetime instance
	for the "first" occurrence.  Each line specifies a successive recurrence
	of the event.

	See user docs for what is considered ok in the lines and how it is
	interpreted.
	"""
	res = []
	curLine, curDT = 0, initDT
	for dtLiteral in (s.strip() for s in listLiteral.split("\n")):
		curLine += 1
		if not dtLiteral:
			continue
		try:
			dateLiteral, timeLiteral = _parseDTSpec(dtLiteral.strip())
			nextDate = parseFreeformDate(dateLiteral, curDT)
			nextTime = curDT.time()
			if timeLiteral:
				nextTime = parseFreeformTime(timeLiteral)
			curDT = datetime.datetime.combine(nextDate, nextTime)
			res.append(curDT)
		except ValueError as ex:
			raise ValueError("Fehler in Datumsliste, Zeile %s: %s"%(
				curLine, str(ex)))
	return res


def formatHumanReadable(date):
	"""returns a string giving a date string for date with a bit of human
	appeal.
	"""
	return "{}. {} {}".format(
		date.day,
		getMonthName(date.month),
		date.year)
