"""
Central page factory plus root resource.
"""

import imp
import os
import time

from twisted.internet import defer
from twisted.python import failure
from twisted.web import resource
from twisted.web import static

from sofoterm import base
from sofoterm.pages import common
from sofoterm.pages import caching
from sofoterm.pages import errors
from sofoterm.pages import pg_list
from sofoterm.pages import plugin


cacheablePaths = [
	'',
	'/',
	'/dyn/unimut',
	'/dyn/about',
	'/dyn/gremien',
	'/dyn/howto',
	'/dyn/privay',
	'/archive',
]


class WellKnown(resource.Resource):
	"""A handler for the .well-known hierarchy.

	We only do something for ACME at this point.
	"""
	acmeChallengeDir = os.path.join(base.getAppPath(), "acme-challenge")

	def getChild(self, request, segments):
		if segments[0]=="acme-challenge" and len(segments)==2:
			if os.path.exists(self.acmeChallengeDir):
				return static.File(
					os.path.join(self.acmeChallengeDir, segments[1]))

		raise base.PageNotFound("Only ACME supported in .well-known")


class PageFactory(resource.Resource):
	"""the factory for pages.  It is *never* rendered.

	The reason it's not rendered is that event pages need access to the current
	request (e.g., to figure out if they should render for a sponsor).  Thus,
	there's not much useful I could render as root.

	It may render an exception, though.

	The main dispatching action is done through the eventPages class
	attribute -- it contains the EventPages as descripted in the package
	__init__.
	"""
	forDate = None
	eventPages = {}

	def __init__(self, conn):
		self.conn = conn
		resource.Resource.__init__(self)
		self.cache = caching.PageCache(conn, cacheablePaths)
		plugin.loadPlugins(self)

	@classmethod
	def addPage(cls, pgName):
		"""adds a child page to the page factory.

		The method imports pg_<pgName> from sofoterm.pages and enters
		its locateChild function in the eventPages class attribute.
		"""
		moduleName = "pg_"+pgName
		modNs = imp.load_source(moduleName, base.getResource(
			"pages/%s.py"%moduleName))
		try:
			cls.eventPages[pgName] = modNs.locateChild
		except AttributeError:
			raise Exception("Internal Error: %s module has no locateChild"%
				moduleName)

	def render(self, request):
		# our getChild does a lot of setup, which we need to
		# re-do.  The None is a sentinel for the root page (which we
		# may want to make configurable).
		return self._realLocateChild(request, [""]).render(request)
	
	def renderHTTP_exception(self, request, failure):
		return defer.maybeDeferred(errors.errback, failure
			).addCallback(lambda res: res.render(request)
			).addCallback(lambda res:
				request.finishRequest(False) or res)

	def _canonicalizeHost(self, request):
		reqHost = base.debytify(request.getHost())
		request.setHost(
			self.conn.stconf.hostName.encode("ascii"),
			self.conn.stconf.port)
		if reqHost is None or request.method!=b"GET":
			return

		# The host name canonicalisation is probably broken since the python3
		# move, but I couldn't be bothered to figure it our so far.
		if (isinstance(reqHost, str)
				and reqHost.split(":")[0]!=self.conn.stconf.hostName):
			raise base.Redirect(base.completeURL(b"/"+request.uri.lstrip(b"/"),
				self.conn))

	def _updateIfMidnight(self):
		"""On midnight every day, fake an update.

		This is to make the job easier for pages that change daily; these
		can rely on their caches being cleared when they should.
		"""
		today = time.localtime()[1:3]
		if self.forDate is None:
			self.forDate = today
		elif self.forDate!=today:
			self.conn.post("edit", event=None)
			self.forDate = today

	def _realLocateChild(self, request, segments):
		self._canonicalizeHost(request)
		common.establishSponsorness(self.conn, request)
		self._updateIfMidnight()

		cached = self.cache.process(request)
		if cached:
			return cached

		if (segments[0]
				and hasattr(self, "child_"+segments[0])):
			request.postpath = [s.encode("utf-8") for s in segments[1:]]
			return getattr(self, "child_"+segments[0])

		# internal sentinel for the root page
		if len(segments)==1 and segments[0]=="":
			return pg_list.EventList(self.conn, request)

		selector = segments[0]
		if selector in self.eventPages:
			res = self.eventPages[selector](
				self.conn, request, segments[1:])
			if res is not None:
				return res

		raise base.PageNotFound("No such resource.")

	def getChild(self, name, request):
		segments = [s.decode("utf-8") for s in [name]+request.postpath]
		request.postpath = []
		try:
			return self._realLocateChild(request, segments)
		except Exception:
			return errors.mapFailure(failure.Failure())


setattr(PageFactory, 'child_default.css', common.makeStatic(("default.css",)))
setattr(PageFactory, 'child_formal.css', common.makeStatic(("formal.css",)))
setattr(PageFactory, 'child_.well-known', WellKnown())

# quick vanity hack; make a proper vanity mechanism and then remove this.
setattr(PageFactory, 'child_krise', common.StaticDir("/home/sofo/krisehtml"))

try:
	setattr(PageFactory, 'child_favicon.ico',
		common.makeStatic(("favicon.ico",)))
except base.PageNotFound:  # No favicon, don't worry.
	pass


# The pages defined here -- see the __init__.py docstring
PageFactory.addPage("archive")
PageFactory.addPage("cal")
PageFactory.addPage("config")
PageFactory.addPage("dyn")
PageFactory.addPage("edit")
PageFactory.addPage("errors")
PageFactory.addPage("event")
PageFactory.addPage("list")
PageFactory.addPage("static")
PageFactory.addPage("ext")
PageFactory.addPage("login")
PageFactory.addPage("tagedit")
PageFactory.addPage("subscribe")
PageFactory.addPage("search")
