"""
Display and management of single events.
"""

import re

from twisted.web import static
from twisted.web import util as twu

from sofoterm import base
from sofoterm import model
from sofoterm.base import _
from sofoterm.formal import nevowc
from sofoterm.pages import common
from sofoterm.pages import event

class ConfirmDeletion(event.EventPageWithEvent):
	"""A little "dialog" requesting confirmation and deleting the
	page if confirmation has been given.
	"""
	def _produceDocument(self, request):
		if self.sponsor is None:
			raise base.Forbidden(_("Termine löschen, eh?  Nix gibts."))
		if base.getfirst(self.request.args, b"isSure", None)==b"Yes":
			self.event.delete(self.conn)
			self.conn.commit()
			return twu.Redirect(b"/list").render(request)
		else:
			return event.EventPageWithEvent._produceDocument(self, request)

	def render_DELETE(self, request):
		request.method = b"POST"
		return self._produceDocument(request)

	loader = nevowc.XMLFile(base.getResource(
		"templates/confirmdelete.html"))


class EventDisplay(event.EventPageWithEvent):
	"""display and manipulation of a single event.

	GET (or POST, for that matter) return formatted ("long") display,
	DELETE deletes the event (with a dialog, usually).
	"""
	loader = nevowc.XMLFile(base.getResource("templates/event.html"))


class NakedEventDisplay(event.EventPageWithEvent):
	"""display of a single event without sofo fluff.
	"""
	loader = nevowc.XMLFile(base.getResource("templates/nakedevent.html"))


def redirectToEdit(conn, request, segments):
	"""redirects root accesses to /edit.
	"""
	if len(segments)==1 and segments[0]=="":
		return request.iURLPath.click("edit")


_idPat = re.compile(r"^\d+$")


def doDeletion(conn, request, segments):
	"""deletion requests.
	"""
	if len(segments)==1:
		if ((request.method==b"DELETE" 
					or base.getfirst(request.args, b"method", None)==b"DELETE")
				and _idPat.match(segments[0])):
			return ConfirmDeletion(conn, request, 
				model.Event.fromId(int(segments[0]), conn))


def displayEvent(conn, request, segments):
	if len(segments)==1 and _idPat.match(segments[0]):
		return EventDisplay(conn, request,
			common.getEvent(conn, segments[0]))


def displayNakedEvent(conn, request, segments):
	if len(segments)==2 and segments[0]=="n" and _idPat.match(segments[1]):
		return NakedEventDisplay(conn, request,
			common.getEvent(conn, segments[1]))


def attachment(conn, request, segments):
	if len(segments)==2 and segments[1]=="attached.pdf":
		return static.File(common.getEvent(conn, segments[0])
			.getAttachmentName())


locateChild = common.makeSerialLocator(
	redirectToEdit,
	doDeletion,
	displayEvent,
	displayNakedEvent,
	attachment)
