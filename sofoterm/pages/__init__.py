"""
The resources actually delivered by the server, and their helpers.

Actual pages reside in modules called pg_<whatever>.py, where <whatever>
is the first part of the URL path (the only exception is the root
that resides in root.py).

Each pg_ module implements a function
locateChild(conn, request, segments) -> resource (or None).

To acquaint the dispatcher (PageFactory in root.py) with a page,
use PageFactory's class method addPage(part); pass the <whatever>
part, e.g., "edit" for pg_edit.py.
"""

# Not checked by pyflakes: Only setting up namespace

from sofoterm.pages.common import EventPage, renderPlace
from sofoterm.pages.root import PageFactory
from sofoterm.pages.plugin import registerExtPage, ExtensionPage
