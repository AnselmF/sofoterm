
"""
Everything to do with editing events.

(note deletion is done via DELETE (or POST with attrs) to event).
"""

import datetime
import urllib.parse

from sofoterm import base
from sofoterm import formal
from sofoterm import model
from sofoterm.base import _
from sofoterm.formal import nevowc
from sofoterm.model import PLACES_SELECT
from sofoterm.pages import common
from sofoterm.pages import customui
from sofoterm.pages import errors
from sofoterm.pages import event
from sofoterm.pages import pg_list


class Editor(formal.ResourceWithForm, event.EventPageWithEvent):
	"""An editor for events.

	It's constructed through the makeEditor factory with an event instance.
	"""
	def crash(self, failure, request):
		return errors.serveError(request, failure)

	def proclink(self, request, tag):
		return tag

	def makeURL(self, link):
		return link

	def data_curid(self, request, tag):
		return self.event.id

	def data_event(self, request, tag):
		return self.event

	def saveEvent(self, request, form, data):
		try:
			self.event.processFormData(data)
		except base.RSTError as ex:
			raise formal.FieldError(str(ex), "fulltext")
		self.event.toDB(self.conn)
		self.conn.commit()
		self.conn.post("edit", event=self.event)

	def _addDefaultsFromRequest(self, request, form):
		# this is for partially filled out forms (GET): take the GET
		# parameters and  stuff them into fields as appropriate.
		if self.request.method==b"GET":
			for n, v in self.request.args.items():
				try:
					field = form.items.getItemByName(n)
					field.process(request, form, self.request.args, form.errors)
				except KeyError: # no such form element
					pass

	def _getTagsAvailable(self):
		"""returns a set of tags the user can choose from.

		This is composed of the configured default set, any extra tags
		on the event, and possibly custom tags configured for the current user.
		"""
		tagsAvailable = self.conn.stconf.tags
		if hasattr(self.event, "tags"):
			tagsAvailable |= self.event.tags
		tagsAvailable |= set(
			self.conn.stconf.getConfigs(self.request.sponsor+"::tag"))
		return tagsAvailable

	def form_termedit(self, request):
		form = formal.Form()
		form.addField("bdate", formal.Date(required=True),
			customui.FreeformDateInput,
			label="Datum", description=_("Tag des Termins; Doppelklick für Termine,"\
			" die es an dem Tag schon gibt."))
		form.addField("btime", formal.Time(),
			customui.FreeformInput,
			label="Anfangszeit", description=_("Anfangszeit.  Kann leer bleiben"
				" für ganztägige Termine (sollten Ausnahme sein)"))
		form.addField("teaser", formal.String(required=True),
			formal.widgetFactory(customui.ScalingTextArea, rows=3),
			label="Kurztext/Titel", description=_("Kurzbeschreibung"
			" des Termins; das ist, was in den Listen kommt.  Kurz lassen"
			" (Richtwert: 140 Zeichen).  Kein HTML o.ä."))
		form.addField("place", formal.String(),
			formal.widgetFactory(customui.ScalingTextArea, rows=3),
			label="Ort", description=_("Veranstaltungsort.  HTML geht"
				" nicht.  Vordefinierte Orte: %s.")%PLACES_SELECT)
		form.addField("fulltext", formal.String(),
			formal.widgetFactory(customui.ScalingTextArea, rows=10),
			label="Langtext", description=_("Alles, was ihr für die Beschreibung"
				" der Veranstaltung für nötig haltet.  HTML geht nicht, wohl aber"
				" ReStructured text.  Ihr könnt auch einfach eine URL reinschreiben,"
				" dann ist die Veranstaltungs-URL genau das."))
		form.addField("fdate", formal.Date(), customui.FreeformInput,
			label="Erster Tag",
			description=_("Erstes Datum für mehrtägige Termine (bleibt normalerweise"
			" leer, vgl. 'Hilfe zur Eingabe')"))
		form.addField("tags", formal.String(),
			formal.widgetFactory(formal.CheckboxMultiChoice, 
				options=[(a,a) for a in sorted(self._getTagsAvailable())], 
				lots=True),
			description=_("Wählt Tags aus, die eure Veranstaltung beschreiben;"
				" Richtschnur: Wenn jemand Termine mit dem Tag auswählt, würden"
				" sie euren Termin sehen wollen?"),
			label="Tags")
		form.addField("private", formal.Boolean(), 
			label=_("Privat"),
			description=_('Ein "privater" Termin wird nur in Listen'
				' für einen Tag des Termins angezeigt (das willst du in der Regel'
				' nicht).'))
		form.addAction(self.saveEvent, label="Ok")
		form.data = self.event.getFormData()
		self._addDefaultsFromRequest(request, form)
		return form

	def addAttachment(self, request, form, data):
		if data["upload"] is None or not data["upload"][0]:
			return self
		ignored, contentF = data["upload"]
		content = contentF.read(self.conn.stconf.uploadLimit+1)
		if len(content)>self.conn.stconf.uploadLimit:
			raise formal.FieldError(_("Zusätze dürfen nicht länger als %s"
				" Bytes sein")%self.conn.stconf.uploadLimit, "upload")
		if not content.startswith(b"%PDF"):
			raise formal.FieldError(_("Der Zusatz sieht überhaupt nicht nach"
				" einem PDF aus.  Ich will aber nur PDFs haben hier"), "upload")
		self.event.setAttachment(self.conn, content)

	def removeAttachment(self, request, form, data):
		self.event.removeAttachment(self.conn)

	def form_attach(self, request):
		form = formal.Form()
		form.addField("upload", formal.File(required=not self.event.attachments),
			label=_("Zusatz"), description=_(
			"Eine Einladung, ein Fluggi, ein Thesenpapier o.ä."
			"  Muss ein PDF sein und darf nicht länger als 1 MB sein."
			"  Es gibt nur eine solche Datei pro Termin."))
		if self.event.attachments==1:
			form.addAction(self.addAttachment, label=_("Ersetzen"),
				name="addAttach")
			form.addAction(self.removeAttachment, label=_("Löschen"),
				name="delAttach")
		else:
			form.addAction(self.addAttachment, label=_("Hochladen"),
				name="addAttach")
		return form

	def _makeCopies(self, request, form, data):
		if data["moreterms"] is None:
			return self

		# parsing of the moreterms string must take place here since
		# we need to know the current event's bdate.
		btime = self.event.btime
		if btime is None:
			btime = datetime.time(0, 0)
		try:
			dts = common.parseDateList(data["moreterms"], 
				datetime.datetime.combine(self.event.bdate, btime))
		except ValueError as ex:
			raise formal.FieldError(str(ex), "moreterms")

		allEvs = [self.event]
		for dt in dts:
			newEv = model.TaggedEvent.fromEvent(self.request.sponsor,
				self.conn, self.event)
			newEv.bdate = dt
			newEv.toDB(self.conn)
			allEvs.append(newEv)
		nextRes = pg_list.EventList(self.conn, self.request)
		nextRes.evList = model.EventList(allEvs)
		return nextRes

	def form_moreDates(self, request):
		form = formal.Form()
		form.addField("moreterms", formal.String(required=True), 
			widgetFactory=formal.TextArea,
			label=_("Weitere Termine"), description=_("Weitere Termine, an denen das"
			" hier stattfindet.  Das ist etwas tricky, also erstmal die Doku"
			" lesen"))
		form.addAction(self._makeCopies, label=_("Erzeugen"))
		return form

	loader = nevowc.XMLFile(base.getResource("templates/edit.html"))


def _assertSponsor(conn, request, segments):
	if not request.isSponsor:
		raise base.Forbidden(_("Wenn du Termine editieren willst, wende"
			" dich an die OperatorInnen"))


def _makeNewEvent(conn, request, segments):
# alloc a new event on access to root.
	if len(segments)==1 and segments[0]=="":
		event = model.TaggedEvent.create(request.sponsor, conn)
		conn.commit()

		defaults = ""
		args = base.debytifyArgs(request.args)
		if args:
			defaults = "?"+urllib.parse.urlencode(
				args, encoding="utf-8", doseq=True)
		
		raise base.Redirect("/{}/{}{}".format(
			base.debytify(request.uri).lstrip("/"),
			str(event.id),
			defaults))


def _editExistingEvent(conn, request, segments):
	if len(segments)!=1:
		return
	try:
		id = int(segments[0])
	except ValueError:
		raise base.EventNotFound(segments[0])
	return Editor(conn, request, model.TaggedEvent.fromId(
		id, conn, sponsor=request.sponsor))


def _cloneEvent(conn, request, segments):
	if len(segments)!=2 or segments[-1]!="clone":
		return
	try:
		id = int(segments[0])
	except ValueError:
		raise base.EventNotFound(segments[0])
	original = common.getEvent(conn, id)
	clone = model.TaggedEvent.fromEvent(request.sponsor,
		conn, original)
	conn.post("edit", event=clone)
	raise base.Redirect("/edit/%s"%clone.id)


locateChild = common.makeSerialLocator(
	_assertSponsor, 
	_makeNewEvent, 
	_editExistingEvent,
	_cloneEvent)
