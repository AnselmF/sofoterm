"""
A simple caching system for nevow pages.

The basic idea is to monkeypatch the request object in order to
snarf content and headers.
"""

import time

from twisted.web import http
from twisted.web import resource

from sofoterm import base


def instrumentRequestForCaching(request, finishAction):
	"""changes request such that finishAction is called with the request and
	the content written for a successful page render.
	"""
	builder = CacheItemBuilder(finishAction)
	origWrite, origFinish = request.write, request.finish

	def write(content):
		builder.addContent(content)
		return origWrite(content)

	def finish():
		builder.finish(request)
		return origFinish()

	request.write = write
	request.finish = finish


class CacheItemBuilder(object):
	"""an aggregator for web pages as they are written.

	On successful page generation an function is called with
	the request and the content written as arguments.
	"""
	def __init__(self, finishAction):
		self.finishAction = finishAction
		self.contentBuffer = []
	
	def addContent(self, data):
		self.contentBuffer.append(data)
	
	def finish(self, request):
		if request.code==200:
			self.finishAction(request, b"".join(self.contentBuffer))


class CachedPage(resource.Resource):
	def __init__(self, content, headers, changeStamp):
		self.content, self.headers = content, list(headers.getAllRawHeaders())
		self.headers.append(("x-cache-creation", [str(time.time())]))
		self.changeStamp = changeStamp

	def render(self, request):
		if request.setLastModified(self.changeStamp)==http.CACHED:
			return b""

		else:
			for key, value in self.headers:
				request.responseHeaders.setRawHeaders(key, value)
			# override the cached date header; since this is a different request
			# we consider the response a different entity.
			request.setHeader('date', base.formatHTTPDate())
			return self.content


def enterIntoCacheAs(key, destDict):
	"""returns a finishAction that enters a page into destDict under key.
	"""
	def finishAction(request, content):
		try:
			lastModified = base.parseHTTPDate(
				request.responseHeaders.getRawHeaders("last-modified")[0])
		except:	# Cachable pages without last-modified are assumed
						# to be made on the fly
			lastModified = time.time()
		destDict[key] = CachedPage(content, request.requestHeaders, lastModified)
	return finishAction


class PageCache(object):
	"""A cache.

	This works by registering certain paths as cacheable.  You then
	pass nevow requests into the process method.  If something is
	cacheable, you either get back a nevow resource you can dump, or
	the cache will instrument the request so the result of your operation
	will end up in the cache.
	"""
	def __init__(self, conn, cacheablePaths=[]):
		self.conn = conn
		self.cacheable = set()
		self.cache = {}

		for p in cacheablePaths:
			self.addCacheable(p)

		conn.subscribe("reload", lambda conn: self.clear())
		conn.subscribe("edit", lambda conn, event: self.clear())
	
	def clear(self):
		self.cache = {}

	def addCacheable(self, path):
		self.cacheable.add(base.bytify(path))
	
	def process(self, request):
		if (request.method==b"GET"
				and request.uri in self.cacheable
				and not request.isSponsor):
			request.setLastModified(self.conn.lastChange)
			if request.uri in self.cache:
				return self.cache[request.uri]
			else:
				instrumentRequestForCaching(request,
					enterIntoCacheAs(request.uri, self.cache))
