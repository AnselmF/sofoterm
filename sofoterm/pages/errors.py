"""
Renderers for errors displayed to the user.
"""

from twisted.python import log
from twisted.web import server
from twisted.web import template
from twisted.web.template import tags as T

from sofoterm import base
from sofoterm.formal import nevowc
from sofoterm.pages import common


class ErrorBase(nevowc.TemplatedPage, common.SimpleRenderMixin):
	"""The base class for all error pages.

	Error pages are constructed with an exception; it must have a
	msg attribute that can be rendered by stan.
	"""
	stylesheet = "/default.css"

	def __init__(self, exception):
		self.exception = exception
		self.msg = exception.msg
	
	@template.renderer
	def message(self, request, tag):
		if not self.msg:
			return ""
		return tag[self.msg]

	def render(self, request):
		request.setResponseCode(self.code)
		return nevowc.TemplatedPage.render(self, request)

	def render_DELETE(self, request):
		# we want to be able to produce proper errors for event DELETE, too
		# (not that anyone would actually do that)
		return self.render_GET(request)


class NotFound(ErrorBase):
	code = 404
	handles = (base.PageNotFound,)
	loader = nevowc.XMLFile(base.getResource("templates/404.html"))


class Redirect(ErrorBase):
	code = 302
	handles = (base.Redirect,)
	def render(self, request):
		request.setHeader("location", str(self.msg))
		return ErrorBase.render(self, request)

	@template.renderer
	def locationhref(self, request, tag):
		return tag(href=str(self.msg))[self.msg]

	loader = template.TagLoader(T.html[T.body[
		T.p["If you're seeing this, your browser has ignored"
			" a redirect.  Thus, you'll have to go to ",
			T.a(render="locationhref"), " manually."]]])


class NotModified(ErrorBase):
	code = 304
	handles = (base.NotModified,)

	class loader(object):
		@staticmethod
		def load(*args):
			return ""


class Forbidden(ErrorBase):
	code = 403
	handles = (base.Forbidden,)
	loader = nevowc.XMLFile(base.getResource("templates/forbidden.html"))


class ServerError(ErrorBase):
	code = 500
	handles = (base.WebException,)
	loader = nevowc.XMLFile(
		base.getResource("templates/serverError.html"))


def mapOtherErrors(ex):
	"""maps internal exceptions to web exceptions as appropriate.
	"""
	if isinstance(ex, base.EventNotFound):
		return base.PageNotFound("No such event: %s"%ex.id)
	if isinstance(ex, base.Error):
		return base.WebException(str(ex))
	return ex


def mapFailure(failure):
	"""returns a nevow resource appropriate for a twisted failure.

	If no such resource can be found, failure's exception is re-raised.
	"""
	ex = mapOtherErrors(failure.value)
	for pg in _errHandlingPages:
		if isinstance(ex, pg.handles):
			return pg(ex)

	log.err(failure)
	# comment out to let exceptions through to the top-level handler
	return ServerError(base.WebException(str(ex)))

	# if we're still handling the exception, keep its traceback.
	failure.raiseException()


def serveError(request, failure):
	"""Writes an error page suitable for failure to request.
	"""
	res = mapFailure(failure)
	res.render(request)
	return server.NOT_DONE_YET


# configure twisted web's Request to use us to produce errors
server.Request.processingFailed = serveError


_errHandlingPages = [NotModified, Redirect, NotFound, Forbidden, ServerError]
