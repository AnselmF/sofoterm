"""
Pages registred by plugins and such.

To have sofoterm-type pages in plugins, inherit from 
pages.ExtensionPage.  There is self.segments containing url segments
below the page, otherwise, the rules for EventPages apply (in particular,
use _produceDocument rather than render if necessary.
"""

from sofoterm import base
from sofoterm.pages import plugin


def locateChild(conn, request, segments):
	if not segments or segments[0]=='' or segments[0] not in plugin.EXT_PAGES:
		raise base.PageNotFound("No such extension registered.")
	return plugin.EXT_PAGES[segments[0]](conn, request, segments[1:])
