"""
Archive access.
"""

import datetime
import re
import time

from twisted.web import template

from sofoterm import base
from sofoterm import model
from sofoterm.base import _
from sofoterm.formal import nevowc
from sofoterm.pages import eventlist, common


# This is much more similar to pg_list.py than I'd like to be -- I
# wonder if the pages should be merged.

class ArchiveList(eventlist.EventListBase):
	"""Lists of past events.
	"""
	def __init__(self, conn, request, evList=None):
		self.year = base.now().year
		self.month = base.now().month
		self.startYear = conn.stconf.startYear
		self.yearSpan = 4
		if not "title" in request.args:
			request.args["title"] = ["Das Termin-Archiv"]
		eventlist.EventListBase.__init__(self, conn, request)
		self.evList = evList

	@classmethod
	def makeForYear(cls, year, conn, request):
		startDate = datetime.datetime(year, 1, 1, 0, 0)
		endDate = datetime.datetime(year, 12, 31, 23, 59)
		request.args["title"] = [_("Termin-Archiv: %s")%(year)]
		obj = cls(conn, request, evList=model.EventList.fromSelectors(
			conn, startDate, endDate))
		obj.year, obj.month = year, 1
		return obj

	@classmethod
	def makeForMonth(cls, year, month, conn, request):
		startDate = datetime.datetime(year, month, 1, 0, 0)
		if month==12:
			endDate = datetime.datetime(year+1, 1, 1, 0, 0)
		else:
			endDate = datetime.datetime(year, month+1, 1, 0, 0)
		request.args["title"] = [_("Termin-Archiv: %s %s")%(
			common.getMonthName(month), year)]
		obj = cls(conn, request, model.EventList.fromSelectors(
			conn, startDate, endDate))
		obj.year, obj.month = year, month
		return obj

	def _computeList(self):
		args = base.debytifyArgs(self.args)
		endDate = base.getfirst(args, "endDate", None)
		if endDate:
			endDate = base.parseISODate(endDate).date()
		else:
			endDate = base.now().date()

		startDate = base.getfirst(args, "startDate", None)
		if startDate:
			startDate = base.parseISODate(startDate).date()
		else:
			nDays = base.parseInt(base.getfirst(args, "nDays", -1))
			if nDays<0:
				startDate = endDate-self.conn.stconf.defaultTimeDelta
			else:
				startDate = endDate-datetime.timedelta(days=nDays)

		self._setLastChange(time.mktime(endDate.timetuple()))

		self.title = "Termin-Archiv zwischen {} und {}".format(
			common.formatHumanReadable(startDate),
			common.formatHumanReadable(endDate))

		return model.EventList.fromSelectors(self.conn, startDate, endDate,
			args.get("tag", []), args.get("notag", []))

	def _grabMetaFromContext(self, args):
		eventlist.EventListBase._grabMetaFromContext(self, args)

	def data_archivemonths(self, request, tag):
		"""returns a list of (year, ["year/month"]) tuples around
		the self's year.
		"""
		central_year = self.year
		curYear, curMonth = base.now().year, base.now().month
		startShowYear = max(self.startYear, central_year-self.yearSpan)
		endShowYear = min(curYear, central_year+self.yearSpan)

		res = []
		for year in range(startShowYear, endShowYear):
			endShowMonth = curMonth + 1 if year == curYear else 13
			res.append((str(year), [(str(month), "%s/%s"%(year, month))
				for month in range(1,endShowMonth)]))
		return res

	@template.renderer
	def ifyearbeforeexists(self, request, tag):
		before = int(tag.slotData[0][0]) - 1
		if before < self.startYear:
			return ""
		else:
			return tag

	@template.renderer
	def yearbefore(self, request, tag):
		before = int(tag.slotData[0][0]) -1
		return tag["/archive/"+str(before)]

	@template.renderer
	def ifyearafterexists(self, request, tag):
		curYear = base.now().year
		after = int(tag.slotData[-1][0]) + 1
		if after >= curYear:
			return ""
		else:
			return tag

	@template.renderer
	def yearafter(self, request, tag):
		before = int(tag.slotData[-1][0]) +1
		return tag["/archive/"+str(before)]

	loader = nevowc.XMLFile(base.getResource("templates/archive.html"))


_yearPat = re.compile(r"\d\d\d\d$")
_monthPat = re.compile(r"\d\d?")

def locateChild(conn, request, segments):
	conn = conn.getArchiveConnection()
	if not segments or segments[0]=='':
		return ArchiveList(conn, request)
	elif _yearPat.match(segments[0]):
		if len(segments)==1:
			return ArchiveList.makeForYear(int(segments[0]), conn, request)
		elif len(segments)==2 and _monthPat.match(segments[1]):
			return ArchiveList.makeForMonth(int(segments[0]), int(segments[1]),
				conn, request)
	raise base.PageNotFound("This doesn't lead to any archive.")

