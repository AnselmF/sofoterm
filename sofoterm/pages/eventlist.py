"""
A base class for dealing with event lists (pg_list, pg_archive).
"""

from twisted.web import template

from sofoterm import base
from sofoterm import model
from sofoterm.pages import common
from sofoterm.pages import event
from sofoterm.base import _


class EventListBase(common.EventPage):
	"""A base page for both EventLists and ArchiveLists.

	These pages are constructed with the context and take from them various
	items (like title, stylesheet, startDate, endDate, nDays, tag, notag).
	From them, a query is constructed, and that's the event list rendered.

	The two concrete classes differ by template, metadata.
	"""
	evList = None
	title = None

	def __init__(self, conn, request):
		common.EventPage.__init__(self, conn, request)
		self.args = base.debytifyArgs(request.args)
		self._grabMetaFromContext(self.args)

	def _produceDocument(self, request):
		if self.evList is None:
			self.evList = self._computeList()
		return common.EventPage._produceDocument(self, request)

	def _computeList(self):
		return model.EventList.fromQueryDict(self.conn, self.args, 
			sponsor=self.sponsor)

	def _grabMetaFromContext(self, args):
		if self.title is None:
			self.title = base.getfirst(args, "title", "")
		if not self.title:
			self.title = _("Termine vom %s")%base.now().strftime("%d.%m.%Y")

	@template.renderer
	def doctitle(self, request, tag):
		return tag[self.title]

	@template.renderer
	def fullshort(self, request, tag):
		"""returns a short event representation with a date for the event
		in tag.slotData.
		"""
		return tag[event.ShortlistItem(tag.slotData, self, withdate=True)]

	@template.renderer
	def timelineitem(self, request, tag):
		"""renders tag.slotData as an event.TimelineItem.
		"""
		return tag[event.TimelineItem(tag.slotData, self)]

	@template.renderer
	def listshort(self, request, tag):
		"""renders tag.slotData as an event.TimelineItem.
		"""
		return tag[event.ShortlistItem(tag.slotData, self)]

	def data_evlist(self, request, tag):
		return self.evList.iterGrouped()
