"""
Plugin handling code.

Plugins reside in special directories (so far: sofoterm/plugins) and are just
python source files defining a plugin_init(root) function.  They receive the
root page as an argument (and can thus access the connection at root.conn).

The modules can also define a function getTests(rootPage) returning
a dictionary (usually, locals()); the TestCase-derived values within
this dictionary make up a testsuite generated in pluginstest.py.
"""

import glob
import functools
import imp
import os
import warnings

from sofoterm import base
from sofoterm.pages import common

PLUGINS = {}
EXT_PAGES = {}

def registerExtPage(name, page):
	EXT_PAGES[name] = page


def _loadPlugin(fName):
	modName = os.path.splitext(os.path.basename(fName))[0]
	modNs = imp.load_source(modName, fName)
	PLUGINS[modName] = modNs
	return modNs


def loadPluginByName(pluginName):
	fullPath = os.path.join(base.getResource("plugins"),
		os.path.basename(pluginName))
	return _loadPlugin(fullPath)


@functools.lru_cache(maxsize=None)
def getPlugins():
	"""returns a dictionary of plugin names to their namespaces.

	No initialization is done.
	"""
	pluginDir = base.getResource("plugins")
	if not os.path.isdir(pluginDir):
		return
	namespaces = {}
	for fName in glob.glob(os.path.join(pluginDir, "*.py")):
		try:
			ns = _loadPlugin(fName)
			namespaces[ns.__name__] = ns
		except SyntaxError as ex:  # ignore bad plugins -- log anything here?
			warnings.warn("Bad plugin %s: %s"%(fName, str(ex)))
	return namespaces


def loadPlugins(rootPage):
	namespaces = getPlugins()
	_ModuleInitializer(namespaces).initAll(rootPage)


class _ModuleInitializer(object):
	def __init__(self, namespaces):
		self.namespaces = namespaces
		self.modsInited = set()
		self.modsFailed = set()

	def _initOne(self, name, rootPage):
		if name in self.modsInited:
			return True
		if name in self.modsFailed:
			return False

		ns = self.namespaces[name]
		for depName in getattr(ns, "depends", []):
			if not self._initOne(depName, rootPage):
				self.modsFailed.add(name)
				return False
		try:
			ns.init(rootPage)
		except AttributeError: # not a (complete) plugin, ignore
			warnings.warn("Plugin could not be initialized: %s"%name)
			import traceback; traceback.print_exc()
			self.modsFailed.add(name)
			return False
		self.modsInited.add(name)
		return True

	def initAll(self, rootPage):
		for name in self.namespaces:
			self._initOne(name, rootPage)
		#print "Loaded: %s, Failed: %s"%(self.modsInited, self.modsFailed)


class ExtensionPage(common.EventPage):
	"""A base class for registred extension pages.
	"""
	def __init__(self, conn, request, segments):
		self.segments = segments
		common.EventPage.__init__(self, conn, request)
