"""
Adding and removing custom tags.
"""

import urllib.request, urllib.parse, urllib.error

from sofoterm import base
from sofoterm.formal import nevowc
from sofoterm.pages import common
from sofoterm.base import _


class TagEdit(common.EventPage):
	loader = nevowc.XMLFile(base.getResource("templates/tagedit.html"))

	def data_extratags(self, request, tag):
		if request.isSponsor:
			return sorted(self.conn.stconf.getConfigs(self.sponsor+"::tag"))
		else:
			return []


def locateChild(conn, request, segments):
	if not request.isSponsor:
		raise base.Forbidden(_("Das ist nur für Sponsoren sinnvoll"))

	if not segments:
		return TagEdit(conn, request)
	elif segments[0]=='add':
		newTag = request.args.get(b"tag", [b""])[0].decode("utf-8")
		if newTag:
			conn.stconf.addKeyValue(request.sponsor+"::tag", newTag)
	elif segments[0]=="remove":
		if len(segments)>1:
			conn.stconf.rmKeyValue(request.sponsor+"::tag", 
				urllib.parse.unquote(segments[1]))
	else:
		raise common.PageNotFound(_("Keine gültige Tagedit-Operation"))

	raise base.Redirect(base.completeURL("/tagedit", conn))
