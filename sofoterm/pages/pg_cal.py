"""
"Calendars", i.e., layouts of a month with events.
"""

import calendar

from twisted.web import template
from twisted.web.template import tags as T

from sofoterm import base
from sofoterm import model
from sofoterm.base import _
from sofoterm.formal import nevowc
from sofoterm.pages import common
from sofoterm.pages import eventlist


def getLocalMonthName(year, month):
	return "%s %s"%(common.getMonthName(month), year)


class Calendar(eventlist.EventListBase):
	def __init__(self, conn, request, year, month):
		self.year, self.month = year, month

		if not "title" in request.args:
			request.args["title"] = [(_("Kalender für %s")%getLocalMonthName(
				self.year, self.month))]
		self.days = list(calendar.Calendar().monthdatescalendar(
			self.year, self.month))
		eventlist.EventListBase.__init__(self, conn, request)
		self._setLastChange(conn.lastUpdate)

	def _computeList(self):
		monthDates = self.days
		startDate, endDate = monthDates[0][0], monthDates[-1][-1]
		today = base.now().date()
		selectors = {
				"startDate": startDate,
				"endDate": endDate,
				"wantedTags": self.args.get("tag", []),
				"unwantedTags": self.args.get("notag", [])}

		if endDate<today:
			return model.EventList.fromSelectors(
				self.conn.getArchiveConnection(), **selectors)
		elif startDate>today:
			return model.EventList.fromSelectors(
				self.conn, **selectors)
		else: # current month
			fromArchive = model.EventList.fromSelectors(
				self.conn.getArchiveConnection(), **selectors)
			fromQuery = model.EventList.fromSelectors(
				self.conn, **selectors)
			return model.EventList(fromArchive.evList+fromQuery.evList)

	@template.renderer
	def daylabel(self, request, tag):
		# data is an item of a calendar week (i.e., a pair of day and list)
		if self.request.isSponsor:
			return tag[T.a(title=_("Neuen Termin für diesen Tag anlegen"),
				href="/edit?bdate=%s"%(tag.slotData[2].isoformat())
					)[str(tag.slotData[0])]]
		else:
			return tag[str(tag.slotData[0])]

	def data_calendar(self, request, tag):
		# A calendar is a list of weeks, which are lists of days.  Each
		# day is a tuple of (1) the day and (2) a list of events on that
		# day, sorted by time.
		dateDict = dict(self.evList.iterGrouped())
		return [[("%02d"%(d.day), dateDict.get(d, []), d) for d in week]
			for week in self.days]

	def data_multiday(self, request, tag):
		today = base.now().date()
		needArchive = (today.year>self.year
			or (today.year==self.year and today.month>self.month))
		if needArchive:
			return model.EventList.multidayForMonth(
				self.conn.getArchiveConnection(),
				self.year, self.month)
		else:
			return model.EventList.multidayForMonth(self.conn,
				self.year, self.month)

	@template.renderer
	def microev(self, request, tag):
		event = tag.slotData
		tag(href="/event/%s"%event.id)[event.teaser]
		return tag(title=event.teaser)

	def data_diffLink(self, nMonths):
		diff = int(nMonths)
		def data(request, tag):
			args = ""
			if b"tag" in request.args:
				args = "?"+(b"&".join(b"tag="+t for t in request.args[b"tag"])
					).decode("utf-8")
			
			year, month = common.addMonths(self.year, self.month, diff)
			return f"/cal/{year}/{month}{args}", getLocalMonthName(year, month)
		return data

	loader = nevowc.XMLFile(base.getResource("templates/cal.html"))


def locateChild(conn, request, segments):
	year, month = None, None
	if not segments or segments[0]=='':
		t = base.now()
		year, month = t.year, t.month
	elif len(segments)==2:
		try:
			year, month = list(map(int, segments))
		except ValueError:
			pass
	if year is None:
		raise base.PageNotFound("There is no such calendar.")
	return Calendar(conn, request, year, month)
