"""
Templated user pages served from /dyn, without the html.
"""

import os

from twisted.web import template

from sofoterm import base
from sofoterm.base import _
from sofoterm import formal
from sofoterm.pages import common
from sofoterm.pages import eventlist


class DynPage(formal.ResourceWithForm, eventlist.EventListBase):
	def __init__(self, loader, conn, request):
		self.loader = loader
		common.EventPage.__init__(self, conn, request)
		self._setLastChange(conn.lastUpdate)

	@staticmethod
	def getTag(elements, tagName):
		for el in elements:
			if getattr(el, "tagName", None)==tagName:
				return el

	@template.renderer
	def doctitle(self, request, tag):
		# try to find the title tag without going overboard.  Return a
		# (silly, at this point) default otherwise.
		tpl = self.loader._loadedTemplate
		try:
			head = self.getTag(tpl[0].children, "head")
			if head:
				title = self.getTag(head.children, "title")
				if title:
					return tag[title.children]
		except Exception:
			import traceback; traceback.print_exc()
			# probably and empty document -- don't bother
			pass
		return tag["SoFo: Termine in Heidelberg"]


def locateChild(conn, request, segments):
	if len(segments)!=1:
		raise base.PageNotFound(_("Du rätst.  Nicht gut."))
	path = base.getResource("dyn/"+segments[0]+".html")
	if not os.path.exists(path):
		raise base.PageNotFound(_("Diese Seiten sollten eigentlich nicht"
			" einfach verschwinden.  Beschwer dich."))
	return DynPage(formal.XMLFile(path), conn, request)
