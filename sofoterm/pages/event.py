"""
Pages and fragments displaying or manipulating single pages.
"""

import weakref

from twisted.web import template
from twisted.web.template import tags as T

from sofoterm import base
from sofoterm import formal
from sofoterm.base import _
from sofoterm.pages import common


class _EventBasedRenderers(common.SimpleRenderMixin):
	"""A mixin with render methods for elements having a self.event.
	"""
	@template.renderer
	def place(self, request, tag):
		"""renders an event's place in long form.
		"""
		if not self.event.place:
			return ""
		return tag[common.renderPlace(self.event.place, True)]

	@template.renderer
	def placeWithoutIcon(self, request, tag):
		"""renders an event's place, but ommitting the osmIcon.
		"""
		if not self.event.place:
			return ""
		return tag[common.renderPlace(self.event.place)]
	
	@template.renderer
	def attachmentLink(self, request, tag):
		"""renders a link to this event's attachment into tag.

		This will return nothing if the event does not have an attachment.
		In case you need to predicate on attachments further up the DOM,
		use render="ifany attachments".
		"""
		if self.event.attachments:
			return tag(href="/event/%s/attached.pdf"%self.event.id)
		return ""

	@template.renderer
	def logos(self, request, tag):
		"""renders a sequence of logos for the tags for the event.
		"""
		evTags = getattr(self.event, "tags", ())
		for tagName in evTags:
			logoURL = common.getLogoURL(tagName)
			if logoURL:
				tag[T.li(class_="logoitem")[T.img(src=logoURL)]]
		return tag

	@template.renderer
	def moreLink(self, request, tag):
		"""renders a link to more information of the event.

		That will normally be a fullev rendering, but ir there's no fulltext
		but an attachment, that will stand in.
		"""
		if self.event.fulltext:
			if self.event.fulltext.startswith("http://"):
				return tag(href=self.event.fulltext,
						render=self.proclink)
			else:
				return tag(href=self.parent.makeURL("/event/%s"%self.event.id),
						render=self.proclink)
		else:
			if self.event.attachments:
				return tag(href=self.parent.makeURL(
						"/event/%s/attached.pdf"%self.event.id),
						render=self.proclink)
		return ""

	@template.renderer
	def proclink(self, request, tag):
		"""renders a link according to parent's paper and breakout options.
		"""
		return self.parent.proclink(request, tag)

	@template.renderer
	def date(self, request, tag):
		"""renders the event's date without a time into tag.
		"""
		tag[T.i(class_="icon date")]
		if self.event.bdate:
			if self.event.fdate:
				tag[
					T.span(class_="weekday")[base.weekdays[self.event.fdate.weekday()]],
					T.span(class_="separator")[", "],
					T.span(class_="date")[self.event.fdate.strftime("%d.%m.%Y"), " "],
					T.span(class_="joiner")[" bis "]]
			tag[
				T.span(class_="weekday")[base.weekdays[self.event.bdate.weekday()]],
				T.span(class_="separator")[", "],
				T.span(class_="date")[self.event.bdate.strftime("%d.%m.%Y"), " "]]
		else:
			tag[
				T.span(class_="nodate")["kein Datum angegeben"]]
		return tag

	@template.renderer
	def time(self, request, tag):
		"""renders the event's time into tag.
		"""
		if self.event.btime:
			tag[
				T.i(class_="icon time"),
				T.span(class_="time")[self.event.btime.strftime("%H.%M Uhr")]]
		else:
			tag[
				T.span(class_="notime")]
		return tag

	@template.renderer
	def teaser(self, request, tag):
		"""renders the event's title into tag.
		"""
		return tag[T.span(class_="teaser")[self.event.teaser]]

	@template.renderer
	def description(self, request, tag):
		"""renders the event's fulltext into tag.
		"""
		if self.event.fulltextCompiled:
			return tag[T.xml(self.event.fulltextCompiled)]
		return ""

	@template.renderer
	def editLink(self, request, tag):
		"""renders an edit link for the event into tag.
		"""
		if request.isSponsor:
			if self.event.inPast:
				return tag(href="/edit/%s/clone"%self.event.id)["Clone"]
			else:
				return tag(href="/edit/%s"%self.event.id)["Edit"]
		return ""

	@template.renderer
	def privateTag(self, request, tag):
		"""renders a "privat" into tag if the event actually is private.
		"""
		if request.isSponsor is not None and self.event.private:
			return tag(
				class_="private-marker",
				title=_("Privater Termin (sichtbar für Sponsoren und mit Tags)"))[
					"Privat"]
		return ""

	@template.renderer
	def ifcomplete(self, request, tag):
		"""returns tag if the event is complete (has a bdate and
		a title), empty otherwise.
		"""
		if self.event.bdate and self.event.teaser:
			return tag
		else:
			return ""

	@template.renderer
	def ifincomplete(self, request, tag):
		"""the inverse of ifcomplete.
		"""
		if self.event.bdate and self.event.teaser:
			return ""
		else:
			return tag

	@template.renderer
	def ifany(self, attNames):
		"""renders a tag if any of attNames is non-empty in event.

		attNames is a space-separated list of items from bdate, btime,
		teaser, sponsor, place, fulltext, attachments, fdate, private (and
		perhaps others).
		"""
		attNames = attNames.split()

		def render(request, tag):
			for attName in attNames:
				if getattr(self.event, attName):
					return tag
			return ""

		return render

	def data_tags(self, request, tag):
		return sorted(getattr(self.event, "tags", ()))


class _EventFragment(_EventBasedRenderers,
		formal.CommonRenderers,
		template.Element):
	"""a base class for Elements rendering events.

	It is constructed with an event and has a render method as usual
	stan elements.  You need to add a loader with the appropriate
	template, though.

	These also need to be constructed with their parents, which in
	turn have to have ifflag/ifnoflag renderers.
	"""
	def __init__(self, event, parent):
		self.event = event
		self.parent = weakref.proxy(parent)
		template.Element.__init__(self)

	@template.renderer
	def ifflag(self, flags):
		return self.parent.ifflag(flags)

	@template.renderer
	def ifnoflag(self, flags):
		return self.parent.ifnoflag(flags)


class FullEv(_EventFragment):
	"""A rendered event in full form.
	"""
	loader = formal.XMLFile(base.getResource("templates/fullevent.html"))


class TimelineItem(_EventFragment):
	"""A rendered event for timeline inclusion.
	"""
	loader = formal.XMLFile(base.getResource("templates/timelineitem.html"))


class ShortlistItem(_EventFragment):
	"""A rendered event for inclusion in short lists.

	Construct with the optional withdate to include a date (which we
	do in listings of multiday events).
	"""
	def __init__(self, event, parent, withdate=False):
		self.withdate = withdate
		_EventFragment.__init__(self, event, parent)

	@template.renderer
	def perhapsDate(self, request, tag):
		"""renders a date into tag if we are withdate.
		"""
		if self.withdate:
			if self.event.fdate:
				tag[self.event.fdate.strftime("%d.%m"), "-"]
			tag[self.event.bdate.strftime("%d.%m.%Y")]
			return tag
		else:
			return ""

	loader = formal.XMLFile(base.getResource("templates/shortlistitem.html"))


class EventPageWithEvent(_EventBasedRenderers, common.EventPage):
	"""An abstract base for pages embedding events.
	"""
	def __init__(self, conn, request, event):
		common.EventPage.__init__(self, conn, request)
		self.event = event
		self._setLastChange(event.lastChanged)

	@template.renderer
	def fullev(self, request, tag):
		return FullEv(self.event, self)

	@template.renderer
	def listshort(self, request, tag):
		"""renders an Event in tag.slotData in short form.
		"""
		return tag[ShortlistItem(self.event, self)]

	@template.renderer
	def doctitle(self, request, tag):
		if self.event.bdate:
			return tag[_("Termin für %s")%self.event.bdate]
		else:
			return tag[_("Neuer Termin")]
	
	def data_event(self, request, tag):
		return self.event

	def data_evurl(self, request, tag):
		return "/event/%d"%self.event.id

	def data_editurl(self, request, tag):
		return "/edit/%d"%self.event.id
