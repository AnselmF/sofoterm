"""
Static pages, served from /static.  And a helper for apache-like
file system publication.

Very common static pages (css, js, etc.) should be attached to the
PageFactory directly to save on CPU.
"""

from sofoterm.pages import common

def locateChild(conn, request, segments):
	return common.makeStatic(segments)
