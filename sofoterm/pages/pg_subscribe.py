"""
Executing requests to the mailman API.

I'm lazy here and just use requests because it's probably there anyway.
"""

from twisted.web import template
from twisted.web.template import tags as T

import requests

from sofoterm import base
from sofoterm import formal
from sofoterm.pages import common
from sofoterm.base import _


EXPECTED_CHA = "sofo"


class Subscribe(formal.ResourceWithForm, common.EventPage):
	loader = template.TagLoader(T.html[
		T.head(render="stdhead")[
			T.title["Abo: Ergebnis"]],
		T.body(render="withsidebar")[
			T.p[_("Der Abo-Kram hat gesagt:"), T.br,
				T.strong(render="aboresult")],
			T.p[_("Wenns geklappt hat, solltest du demnächst Mail bekommen.")]
		]])

	@template.renderer
	def aboresult(self, request, tag):
		return tag[self._result]

	def render(self, request):
		self._result = _("Ich kann keine Mailadresse erkennen und abonniere"
			" drum auch niemanden")

		cha = base.debytify(request.args.get(b"cha", [None])[0])

		if cha==EXPECTED_CHA:
			toSubscribe = base.debytify(request.args.get(b"email", [None])[0])
		else:
			self._result = _("Du musst %s ins textcha schreiben, damit ich dich"
				" auf die Liste setze (sorry, ist antispam-Kram).")%EXPECTED_CHA
			toSubscribe = None

		if toSubscribe:
			try:
				# we're doing this synchronously, which is really risky, because
				# we might block on all kinds of network mishaps.  To mitigate,
				# we're setting a tight timeout.  Doing a full-fledged async client
				# thing just for this is too much stress.
				getConfig = self.conn.stconf.getValue
				if getConfig("mailmanAPI")=="TEST INSTRUMENTATION":
					class result:
						status_code = 403
						@staticmethod
						def json():
							return {"title": "just testing", "description": "and breaking"}

				else:
					result = requests.post(
						getConfig("mailmanAPI")+"/members", {
							'list_id': getConfig("mailmanListname"),
							'subscriber': toSubscribe,
							'pre_verified': False,
							'pre_confirmed': False,
							'pre_approved': True,},
						auth=(getConfig("mailmanAPIUser"),
							getConfig("mailmanAPIPassword")),
						timeout=1)
				if result.status_code==201:
					self._result = "Der Mailman hat dich schon mal angenommen."
				else:
					# must be an error, I'd hope; if there's no json, we'll
					# get an incomprehensible error, but in that case things
					# are probably so botched that users can't do much but
					# complain anyway.
					try:
						vars = {"title": "Ups",
							"description": "Könnte schief gegangen sein"}
						vars.update(result.json())
						self._result = "{title} ({description})".format(**vars)
					except ValueError:
						# old mailmans sometimes end plain text rather than json
						self._result = result.text

			except Exception as msg:
				self._result = _(
					"Das ist was fundamental schiefgelaufen: %s"%repr(msg))

		return common.EventPage.render(self, request)


def locateChild(conn, request, segments):
	if not segments:
		return Subscribe(conn, request)
	
	return None
