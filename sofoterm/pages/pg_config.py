"""
User config (custom lists, login, etc).
"""

from sofoterm import base
from sofoterm.formal import nevowc
from sofoterm.pages import common


class Configurator(common.LoginMixin, common.EventPage):
	loader = nevowc.XMLFile(base.getResource("templates/config.html"))


def locateChild(conn, request,  segments):
	if segments:
		raise base.PageNotFound("/config hat keine Unterseiten")
	return Configurator(conn, request)
