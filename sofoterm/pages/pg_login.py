"""
User config (custom lists, login, etc).
"""

from sofoterm import base
from sofoterm import formal
from sofoterm.pages import common


class LoginPage(common.LoginMixin, formal.ResourceWithForm,
		common.EventPage):
	loader = formal.XMLFile(base.getResource("templates/login.html"))


def locateChild(conn, request, segments):
	if segments:
		raise base.PageNotFound("No children on login")
	return LoginPage(conn, request)
