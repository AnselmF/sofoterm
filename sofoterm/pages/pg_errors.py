"""
Error raising pages for tests and similar.
"""

from sofoterm import base

def locateChild(conn, request, segments):
	if segments:
		v = segments[0]
		if v=="ie":
			raise base.InvalidEvent("You ordered it")
		if v=="re":
			raise base.Redirect("road/to/nowhere")
		if v=="fb":
			raise base.Forbidden("not you")
	raise base.Error("unspecified")
