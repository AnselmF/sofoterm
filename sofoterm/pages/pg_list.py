"""
The event lists
"""

from sofoterm import base
from sofoterm import formal
from sofoterm import model
from sofoterm.pages import common
from sofoterm.pages import eventlist
from sofoterm.pages.common import _


class CurrentEventList(eventlist.EventListBase):
	def __init__(self, conn, request):
		eventlist.EventListBase.__init__(self, conn, request)
		self._setLastChange(conn.lastUpdate)


class EventList(CurrentEventList):
	"""Lists of upcoming events.
	"""
	loader = formal.XMLFile(base.getResource("templates/evlist.html"))


class TextEventList(CurrentEventList):
	"""An event list rendered in plain text.
	"""
	def _produceDocument(self, request):
		content = model.textifyEventList(self.conn, self._computeList())
		self.request.setHeader("Content-Type", "text/plain;charset=utf-8")
		return content.encode("utf-8")


class ICalEventList(CurrentEventList):
	"""An event list rendered as iCal.
	"""
	def _produceDocument(self, request):
		content = model.exportAsICal(self.conn, self._computeList())
		self.request.setHeader("Content-Type", "text/calendar;charset=utf-8")
		self.request.setHeader("content-disposition", 
			'attachment; filename="sofo.ics"')
		return content.encode("utf-8")


class EventListForDay(CurrentEventList):
	"""An event list for a day.

	The day is passed as a datetime.datetime instance.  Parsing this
	from the URI path is done in locateChild below.
	"""
	def __init__(self, conn, request, day):
		CurrentEventList.__init__(self, conn, request)
		self.evList = model.EventList.fromSelectors(conn, startDate=day,
			endDate=day, sponsor=self.sponsor)
		self.title = _("Termine für ")+day.strftime("%d.%m.%Y")

	loader = formal.XMLFile(base.getResource("templates/evlist.html"))


def _parseFreeformDay(freeformLiteral):
	"""returns date instance for a rather freely specified date.
	"""
	if not freeformLiteral:
		return base.now()
	try:
		date = common.parseFreeformDate(freeformLiteral)
		if date is None:
			raise ValueError()
		return date
	except (ValueError, UnicodeDecodeError):
		raise base.PageNotFound("Kein Datum")


def doDefaultList(conn, request, segments):
	if not segments or (len(segments)==1 and segments[0]==""):
		return EventList(conn, request)


def doDayList(conn, request, segments):
	if segments and segments[0]=="day":
		return EventListForDay(conn, request, _parseFreeformDay(
			"/".join(segments[1:])))


def doTextList(conn, request, segments):
	if len(segments)==1 and segments[0]=="text":
		return TextEventList(conn, request)


def doICalList(conn, request, segments):
	if len(segments)==1 and segments[0]=="ical":
		return ICalEventList(conn, request)

locateChild = common.makeSerialLocator(
	doDefaultList,
	doICalList,
	doTextList,
	doDayList,
)
