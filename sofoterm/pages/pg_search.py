"""
Full-text search based on sqlite fts5.
"""

from sofoterm import base
from sofoterm import formal
from sofoterm.pages import common
from twisted.web import template


# strings to make dicts from search() result rows
RESULT_FIELDS = ["id", "snippet", "title", "bdate", "src"]


def search(conn, pattern, matchLimit=1000):
	"""returns a list of matches (id, title, snippet, src) for events matching
	pattern.

	pattern is searched in full text, and teaser.  We probably should
	index places, too, but we'd have to expand abbreviations to make that
	non-confusing.

	The list is ordered by rowid (the unix timestamp) descending, i.e.,
	most recent events first.

	The src item is either '' or 'archive', depending on which connection
	the item was found in.
	"""
	query = (
		"SELECT rowid, snippet(event_index, -1, '❯', '❰', '…', 15),"
		"  teaser, bdate, {{}} as src"
		" FROM event_index "
		" WHERE fulltext MATCH ? OR teaser MATCH ?"
		" ORDER BY rowid DESC"
		" LIMIT {}".format(int(matchLimit)))
	args = [pattern, pattern]

	results = list(conn.execute(query.format("''"), args))
	results.extend(conn.getArchiveConnection().execute(
		query.format("'archive'"), args))
	return results


class SearchPage(common.LoginMixin, formal.ResourceWithForm,
		common.EventPage):
	loader = formal.XMLFile(base.getResource("templates/search.html"))
	pattern = None
	matches = ()
	matchLimit = 1000
	overflow = False
	
	def form_fulltext(self, request):
		form = formal.Form()
		form.addField("q", formal.String(required=True), label="Suche nach",
			description="Nerds können hier sqlite FTS5-Anfragen reinschreiben."
				"  Spaltennamen: teaser und fulltext")
		form.addAction(self.do_search, label="Ok")
		return form

	def do_search(self, request, form, data):
		self.pattern = data["q"]
		self.matches = search(self.conn, self.pattern, self.matchLimit)
		self.overflow = len(self.matches)>=self.matchLimit

	def data_match_dicts(self, request, data):
		return  [dict(zip(RESULT_FIELDS, row)) for row in self.matches]

	@template.renderer
	def ifnoresults(self, request, tag):
		# true if there is a pattern but no results
		if self.pattern and not self.matches:
			return tag
		else:
			return ""

	@template.renderer
	def ifoverflow(self, request, tag):
		if self.overflow:
			return tag
		else:
			return ""


def locateChild(conn, request, segments):
	if segments:
		raise base.PageNotFound("No children on search")
	return SearchPage(conn, request)
