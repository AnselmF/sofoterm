
"""
Custom widgets and types for event input.
"""

from twisted.python import components
from twisted.web.template import tags as T
from zope.interface import implementer

from sofoterm import base
from sofoterm import formal
from sofoterm.formal import iformal
from sofoterm.formal import validation
from sofoterm.pages import common


class ScalingTextArea(formal.TextArea):
	"""is a text area that scales with the width of the window.
	"""
	def _renderTag(self, request, key, value, readonly):
		tag=T.textarea(name=key, id=formal.render_cssid(key), rows=self.rows,
			style="width:100%%; height: %dex"%(self.rows*2))[value or '']
		if readonly:
			tag(class_='readonly', readonly='readonly')
		return tag


# So, I'm not a huge fan of the interface/adapter mess of nevow formal's
# conversions.  Still, the basic plan is: I define a widget that expects
# input in some defined form.  That form is formalized by an interface
# (typically IWhateverConvertible).  In the processInput method of
# the widget, it comes up with whatever the interface's toType method
# wants.  It then adapts original to the interface and is done.
#
# Explicit is better than implicit my ass.

class IFreeformConvertible(iformal.IConvertible):
	"""Things that freeform wants to convert from/to.
	"""


class FreeformDateAdapter(object):
	"""datetime.dates to dd.mm.yyyy strings.

	We allow all kinds of messy specs in toType, though.
	"""
	def __init__(self, original):
		self.original = original

	def fromType(self, value):
		if value is None or value is base.Undefined:
			return None
		try:
			return value.strftime("%d.%m.%Y")
		except ValueError:  # strftime cannot format all datetimes
			return value.isoformat().split('T')[0]

	def toType(self, value):
		try:
			return common.parseFreeformDate(value)
		except ValueError as ex:
			raise validation.FieldValidationError(str(ex))

components.registerAdapter(FreeformDateAdapter, formal.Date, 
	IFreeformConvertible)


@implementer(IFreeformConvertible)
class FreeformTimeAdapter(object):
	"""datetime.times to hh:mm strings.

	On input, we try to be lenient.
	"""
	def __init__(self, original):
		self.original = original

	def fromType(self, value):
		if value is None:
			return None
		return value.strftime("%H:%M")
	
	def toType(self, value):
		try:
			return common.parseFreeformTime(value)
		except ValueError as ex:
			raise validation.FieldValidationError(str(ex))


components.registerAdapter(FreeformTimeAdapter, formal.Time, 
	IFreeformConvertible)


class FreeformInput(formal.TextInput):
	"""An input widget for "freeform" things.

	This is basically just strings, but there are special converters
	registred in this module to IFreeformConvertible.
	"""
	def _renderTag(self, request, literal, name, readonly):
		tag = T.input(type="text", id=formal.render_cssid(name), name=name, 
			value=literal)
		if readonly:
			tag(class_='readonly', readonly='readonly')
		return tag

	def render(self, request, key, args, errors):
		converter = IFreeformConvertible(self.original)
		if errors:
			value = args.get(key, [''])[0]
		else:
			value = converter.fromType(args.get(key))
		return self._renderTag(request, value, key, False)

	def renderImmutable(self, request, key, args, errors):
		converter = IFreeformConvertible(self.original)
		val = converter.fromType(args.get(key))
		return self._renderTag(request, val, self._namer(key), True)

	def processInput(self, request, key, args):
		value = args.get(key, [b''])[0].decode("utf-8")
		if value is None:
			return None
		return self.original.validate(
			IFreeformConvertible(self.original).toType(value))


class FreeformDateInput(FreeformInput):
	"""A FreeformInput that uses javascript to give you existing dates
	on a double click.
	"""
	def _renderTag(self, *args):
		return FreeformInput._renderTag(self, *args)(
			ondblclick='window.open("/list/day/"+encodeURI(this.value))')
