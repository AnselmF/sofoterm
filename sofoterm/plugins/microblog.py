"""
A plugin that sends out edited records to identi.ca.
"""

import urllib.request, urllib.parse, urllib.error

from sofoterm import base

depends = ["shorturl"]


class FailedQuery(Exception):
	pass


class AppURLopener(urllib.request.FancyURLopener):
  version = "sofoterm Microblogging plugin"
  _credentials = None

  def addCredentials(self, realm, user, password):
    if self._credentials is None:
      self._credentials = {}
    self._credentials[realm] = user, password

  def prompt_user_passwd(self, host, realm):
    if realm in self._credentials:
      return self._credentials[realm]
    raise FailedQuery("%s requires creds for %s, but we don't have any."%
      host, realm)


def makeIdenticaPoster(conn):
	user = conn.stconf.getValue("identica.user")
	pw = conn.stconf.getValue("identica.password")
	opener = AppURLopener()
	opener.addCredentials("Identi.ca API", user, pw)
	apiURL = "http://identi.ca/api/statuses/update.xml"

	def postToMicroblog(tx):
		# XXX TODO: use twisted http client here.
		def post():
			opener.open(apiURL,
				urllib.parse.urlencode({"status": tx})).read()
		base.forkToCall(post)

	return postToMicroblog


def makeEditHandler(rootPage, postToMicroblog):
	shortPathBroker = rootPage.child_s

	def formatForMicroblog(ev, width=140):
		teaser = ev.teaser
		if not teaser or not teaser.strip():
			return None
		msg = ("%s -- %s"%(
			base.completeURL(shortPathBroker.getShortForFull("/event/%s"%ev.id),
				rootPage.conn),
			teaser.strip()))
		if len(msg)>width:
			msg = msg[:137]
			msg += "..."
		return msg

	def editHandler(conn, event):
		if event is None:
			return
		tx = formatForMicroblog(event)
		if tx is not None:
			postToMicroblog(tx)
	
	return editHandler


def init(rootPage):
	# don't run this for now
	return
	try:
		editHandler = makeEditHandler(rootPage,
			makeIdenticaPoster(rootPage.conn))
		rootPage.conn.subscribe("edit", editHandler)
	except AttributeError: # most likely no shorturl plugin; log this?
		#warnings.warn("Cannot load microblog plugin.  No shorturl?")
		pass
	except KeyError:
		#warnings.warn("Cannot load microblog plugin.  No credentials?")
		pass


def getTests(rootPage):
	import datetime
	import sofoterm
	import testhelpers

	class MicroblogTest(testhelpers.VerboseTest):
		def _assertMicroblogPost(self, ev, expected):
			class FakeMicroblog(object):
				response = None
				def send(self, resp):
					self.response = resp
			fmb = FakeMicroblog()
			editHandler = makeEditHandler(rootPage, fmb.send)
			editHandler(rootPage.conn, ev)
			self.assertEqual(fmb.response, expected)

		def testBasic(self):
			ev = sofoterm.Event(id="abc", bdate=datetime.date(2003, 2, 3),
				btime=datetime.time(12, 00),
				teaser="Sample plays ex", sponsor="God")
			self._assertMicroblogPost(ev,
				'http://localhost:8888/s/1 -- Sample plays ex')

		def testIncomplete(self):
			ev = sofoterm.Event(id="abc", bdate=datetime.date(2003, 2, 3),
				sponsor="God")
			self._assertMicroblogPost(ev, None)
		
		def testShortened(self):
			ev = sofoterm.Event(id="abc", bdate=datetime.date(2003, 2, 3),
				btime=datetime.time(12, 00),
				teaser="This will be a very nice event, for men, women, children,"
					" not to mention outer space aliens and transsexuals.  It will"
					" feature lots of gore, violence and mindless sex.", sponsor="God")
			self._assertMicroblogPost(ev,
				'http://localhost:8888/s/1 -- This will be a very nice event, for men,'
				' women, children, not to mention outer space aliens and'
				' transsexuals...')

	return locals()
