"""
extra renderers for the mailing of the GEW Rhein-Neckar

This is for restoring obfuscated mail Addresses, and extracting "Veranstalter"
"""

import re

from bs4 import BeautifulSoup
from twisted.web.template import tags as T

from sofoterm.pages import common


def _mungeContent(event):
	"""adds attributes used by our renderes to event.

	This is gewScrubbedContent (with "Veranstalter" extracted and mail 
	addresses unschrouded) and gewVeranstalter.
	"""
	if hasattr(event, "gewVeranstalter"):
		return
	if not event.fulltextCompiled:
		event.gewVeranstalter = "?"
		event.gewScrubbedContent = "?"
		return

	soup = BeautifulSoup.BeautifulSoup(event.fulltextCompiled)
	dtt = soup.find("dt", text="Veranstalter:")
	if dtt:
		dt = dtt.parent
		dd = dt.findNextSibling("dd")
		dt.extract()
		dd.extract()
		event.gewVeranstalter = str(dd)[4:-5]
	else:
		event.gewVeranstalter = "?"
	
	content = str(soup)
	event.gewScrubbedContent = re.sub("\s[([]at[])]\s", "@", content)


def render_veranstalter(self, ctx, data):
	"""returns an organizer gleaned from the formatted HTML (if available).

	This wants an event in data.
	"""
	_mungeContent(data)
	return ctx.tag[T.xml(data.gewVeranstalter)]


def render_scrubbedContent(self, ctx, data):
	"""returns a somewhat print-adapted version of the event content.

	This wants an event in data.
	"""
	_mungeContent(data)
	return ctx.tag[T.xml(data.gewScrubbedContent)]


def init(rootPage):
	common.EventPage.addExtRenderer("veranstalter", render_veranstalter)
	common.EventPage.addExtRenderer("scrubbedContent", render_scrubbedContent)
