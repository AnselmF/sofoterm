"""
A plugin that adds an atom feed.

Apart from the /rss path that's added, it also introduces an declareRss
ext renderer destined for a common header.

You should set the rss:title, rss:authors, and rss:email config items.
"""

import time

from xml.etree import ElementTree as ET

from twisted.web import http
from twisted.web import resource
from twisted.web.template import tags as T

from sofoterm import base
from sofoterm import pages
from sofoterm import model
from sofoterm.formal import nevowc
from sofoterm.pages import common


def asISOUTC(secs):
	return time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime(secs))


############## A very simple stan-inspired DOM

class _AutoInstance(type):
	"""is a metaclass that constructs an instance of itself on getitem.

	We want this so we save a parentheses pair on Elements without
	attributes.
	
	As an added feature, it also checks for an attribute childSequence
	on construction.  If it is present, it generates an allowedChildren
	attribute from it.
	"""
	def __init__(cls, name, bases, dict):
		type.__init__(cls, name, bases, dict)
		cls.name = cls.__name__.split(".")[-1]
		if cls.namespace is None:
			cls.tagName = cls.name
		else:
			cls.tagName = ET.QName(cls.namespace, cls.name)

	def __getitem__(cls, items):
		return cls()[items]


_generator_t = type((x for x in ()))


class Tag(object, metaclass=_AutoInstance):
	"""a crazy attempt at making something like stan but less
	code-intensive, synchronous, and namespace-aware.
  """

	namespace = None
	mayBeEmpty = False

	def __init__(self, **kwargs):
		self.element = ET.Element(self.tagName)
		self(**kwargs)

	def __call__(self, **kwargs):
		for k, v in kwargs.items():
			self.element.attrib[k] = v
		return self

	def __getitem__(self, content):
		self.addChild(content)
		return self

	def addChild(self, content):
		if content is None:
			pass
		elif isinstance(content, str):
			if content.strip():
				self.element.text = content.strip()
		elif isinstance(content, (list, tuple, _generator_t)):
			for el in content:
				self.addChild(el)
		elif isinstance(content, Tag):
			if content.mayBeEmpty or not content.isEmpty():
				self.element.append(content.element)
		else:
			raise ValueError("Cannot add %s to a Tag"%repr(content))

	def isEmpty(self):
		return not ((self.element.text and self.element.text.strip())
			or len(self.element))

	def render(self):
		return ET.tostring(self.element)


################### The Atom namespace

class Atom(object):
	class AtomEl(Tag):
		namespace = "http://www.w3.org/2005/Atom"

	class author(AtomEl): pass
	class content(AtomEl): pass
	class email(AtomEl): pass
	class entry(AtomEl): pass
	class feed(AtomEl): pass
	class id(AtomEl): pass
	class link(AtomEl):
		a_href = None
		mayBeEmpty = True
	class name(AtomEl): pass
	class summary(AtomEl): pass
	class title(AtomEl): pass
	class updated(AtomEl): pass


def makeEntryForEvent(ev, conn, cheatWithDate=False):
	dateString = "%d.%d.%d"%(ev.bdate.day, ev.bdate.month, ev.bdate.year)
	if ev.fdate:
		dateString ="%d.%d.%d-%s"%(
			ev.fdate.day, ev.fdate.month, ev.fdate.year, dateString)
	if ev.btime:
		dateString = "%s, %d:%02d"%(dateString, ev.btime.hour, ev.btime.minute)

	if cheatWithDate:
		# RSS wants a UTC timezone marker here.  Since only the sequence
		# matters here, I don't convert the time but just add the marker.
		ts = ev.timestamp.isoformat()+"Z"
	else:
		ts = asISOUTC(ev.lastChanged)

	content = ev.fulltextCompiled or ""
	if ev.place:
		content += '\n<div class="rssplace">Ort: {}</div>'.format(
			nevowc.flattenSyncToString(pages.renderPlace(ev.place, False)))

	return Atom.entry[
		Atom.content(type="html")[content],
		Atom.id[base.completeURL("/event/%d"%ev.id, conn)],
		Atom.link(href=base.completeURL("/event/%d"%ev.id, conn),
			rel="alternate", type="text/html"),
		Atom.updated[ts],
		Atom.title[dateString+" – "+ev.teaser]]


def makeRSSHandler(rootPage, rssConfig):
	conn = rootPage.conn
	class RSSHandler(resource.Resource):
		lastModified = None
		_cache = None

		AtomElements = Atom

		def __init__(self):
			self.additionalMakers = []
			super()
			conn.subscribe("edit", self._processEdit)
	
		def _processEdit(self, conn, event=None):
			# there's a bogus edit around midnight, so this is enough
			# to keep the cache up to date.
			self._cache = None

		def addEntryMaker(self, maker):
			"""adds a function returning an atom item.
			"""
			self.additionalMakers.append(maker)
			self.lastModified = None

		def _makeDoc(self, request):
			args = base.debytifyArgs(request.args)
			evList = model.EventList.fromQueryDict(conn, args)
			multiDay = model.EventList.fromQueryDict(
				conn, args, multi=True)

			# Hack Aug 2020: stura-plugin sorts by update time; work around
			# by using the event dates as change dates.
			cheatWithDate = b"cheat_date" in request.args

			A = Atom
			doc = A.feed[
				A.author[
					A.name[rssConfig["authors"]],
					A.email[rssConfig["email"]]],
				A.id[rssConfig["feedURL"]],
				A.link(rel="self", href=rssConfig["feedURL"]),
				A.link(rel="alternate", href=base.completeURL("/", conn),
					type="text/html"),
				A.title()[rssConfig["title"]],
				A.updated[asISOUTC(conn.lastUpdate)],
				[f() for f in self.additionalMakers],
				[makeEntryForEvent(ev, conn, cheatWithDate) for ev in evList],
				[makeEntryForEvent(ev, conn) for ev in multiDay]]
			self.lastModified = time.time()
			return doc.render()

		def render_GET(self, request):
			request.setHeader("content-type", "application/atom+xml")
			# tell people to re-validate in an hour
			request.setHeader("expires",
				base.formatHTTPDate(time.time()+3600))
#			open("/home/sofo/rsslog", "a").write("%s %s\n"%(
#				self.lastModified, conn.lastUpdate))
			if not request.args:
				if not self._cache:
					self._cache = self._makeDoc(request)
				res = request.setLastModified(conn.lastUpdate)
				if res==http.CACHED:
					return b""
				return self._cache
			else:
				return self._makeDoc(request)

	return RSSHandler()


def makeRSSLinkRenderer(conn, rssConfig):
	def render(self, ctx, data):
		return T.link(rel="alternate", type="application/atom+xml",
			title=rssConfig["title"], href=rssConfig["feedURL"])
	return render


def init(rootPage):
	conn = rootPage.conn
	rssConfig = {
		"title": conn.stconf.getValue("rss:title",
			"rss:title needs to be configured"),
		"authors": conn.stconf.getValue("rss:authors", None),
		"email": conn.stconf.getValue("rss:email", None),
		"feedURL": base.completeURL("/rss", conn),
	}
	common.EventPage.addExtRenderer("rssLink",
		makeRSSLinkRenderer(conn, rssConfig))
	rssHandler = makeRSSHandler(rootPage, rssConfig)
	rootPage.child_rss = rssHandler


def getTests(rootPage):
	import testhelpers

	class _FakeRequest(object):
		def __init__(self, args={}):
			self.args = args

	class DocTest(testhelpers.VerboseTest):
		doc = rootPage.child_rss._makeDoc(_FakeRequest())

		def testNS(self):
			self.assertTrue(b'"http://www.w3.org/2005/Atom"' in self.doc)

		def testEscaping(self):
			self.assertTrue(
				b"title>17.2.2010, 11:55 &#8211; Super-VA (jedeR wei&#223;, wo)."
				in self.doc)

		def testDateForFullDayEvs(self):
			self.assertTrue(b"title>19.2.2010 &#8211; Denkw" in self.doc)

		def testNoEmptyElements(self):
			self.assertFalse(b"email" in self.doc)

		def testIdCreated(self):
			self.assertTrue(b"id>http://localhost:8888/event/2" in self.doc)

		def testContentIncluded(self):
			self.assertTrue (b'content type="html">&lt;p&gt;Na ja,'
				b' da war mal was.&lt;/p&gt;' in self.doc)

		def testMultidayPresent(self):
			self.assertTrue(b"title>12.2.2010-16.2.2010 &#8211; Aktionswoche"
				in self.doc)

		def testPlaceRendered(self):
			self.assertTrue(
					b'&lt;div class="rssplace"&gt;Ort: &lt;i'
					b' class="icon place"&gt;&lt;/i&gt;Autonomes'
				in self.doc)

	return locals()


if __name__=="__main__": # pragma: no cover
	import sys
	sys.path.append("../../tests")
	import testhelpers
	import res_testserver
	testhelpers.main(getTests(res_testserver.SERVER)["DocTest"], "test")
