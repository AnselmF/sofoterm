"""
A sofoterm plugin for "Today XY years ago.."-type displays ("memory
of the day").

The event definition is done in flat files; see the ingest function below
for the expected format.
"""

import re
import operator
import os
import sys
import urllib.parse

from twisted.web import template
from twisted.web.template import tags as T

from sofoterm import _
from sofoterm import base
from sofoterm.formal import nevowc
from sofoterm import model
from sofoterm import pages


depends = ["rss"]


DATE_PATTERN = re.compile(r"(\d\d?\.\d\d?\.[12]\d\d\d)")


BONUSES_FOR_ANNIVERSARIES = [
	(1000,5,),
	(250,4,),
	(100,3,),
	(50,3,),
	(25,2,),
	(10,2,),
	(5,1,),
]



DATE_TEMPLATE = """<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:n="http://nevow.com/ns/nevow/0.1">
<head n:render="stdhead">
	<title>Am <n:invisible n:render="thisdate"/>…</title>
	<style type="text/css">
		div.memforday {
			margin-bottom: 3ex;
			width: 80%;
			margin-left: 10%;
		}
	</style>
</head>
<body n:render="withsidebar">
	<h1>Am <n:invisible n:render="thisdate"/>…</h1>
	<div class_="dates" n:data="candidates">
		<n:invisible n:render="sequence">
			<div class="memforday" n:pattern="item" n:render="memoryText"/>
		</n:invisible>
	</div>
	<hr/>
	<p><a href="/ext/memOTD">Mehr Geschichte</a></p>
	<p>Wir sammeln Jahrestage entweder mit Bezug auf Heidelberg oder
	von generellem Interesse für linksorientierte Menschen.  Rohdateien
	pflegen wir
	<a href="https://codeberg.org/AnselmF/memoryoftheday">bei codeberg</a>
	getrennt nach Themen (d.h., wer will, kann den Heidelberg-Kram auch
	ignorieren und nur die linken Sachen haben).  Wir freuen uns,
	wenn Menschen mitsammeln wollen und
	geben dazu gerne Schreibrechte.</p>
</body>
</html>
"""

TEMPLATE = """<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:n="http://nevow.com/ns/nevow/0.1">
<head n:render="stdhead">
	<title>sofo-hd Jahrestage</title>
</head>
<body n:render="withsidebar">
	<h1>Jahrestage auf sofo-hd.de</h1>
	<div class_="dates" n:data="candidates">
		<n:invisible n:render="ifdata">
			<h2>Am <span n:render="thisdate"/>…</h2>
			<ul n:render="sequence">
				<li n:pattern="item" n:render="oneMemory"/>
			</ul>
		</n:invisible>
	</div>
	<hr/>
	<form method="GET" action="/ext/memOTD"
			style="padding-top:2ex;padding-bottom:4ex">
		<p>Jahrestage für
		<input type="text" name="day" size="2">
			<n:attr name="value" n:render="string" n:data="day"/>
		</input>.
		<input type="text" name="month" size="2">
			<n:attr name="value" n:render="string" n:data="month"/>
		</input>.
		<input type="text" name="year" size="4">
			<n:attr name="value" n:render="string" n:data="year"/>
		</input> <input type="submit" value="berechnen"/></p>
	</form>
	<hr/>
	<p>Wir sammeln Jahrestage entweder mit Bezug auf Heidelberg oder
	von generellem Interesse für linksorientierte Menschen.  Rohdateien
	pflegen wir in einem
	<a href="https://codeberg.org/AnselmF/memoryoftheday.git">subversion Repository</a>
	getrennt nach Themen (d.h., wer will, kann den Heidelberg-Kram auch
	ignorieren und nur die linken Sachen haben).  Wir freuen uns,
	wenn Menschen mitsammeln wollen und
	geben dazu gerne Schreibrechte.</p>
</body>
</html>
"""


# a text role for references by date in RST documents; these become links
# into the plugin.
from docutils import nodes
from docutils import utils as rstutils
from docutils.parsers.rst import roles

def _daterefRolefunc(name, rawText, text, lineno, inliner,
		optins={}, content=[]):
	text = rstutils.unescape(text)
	args = urllib.parse.urlencode(
		dict(list(zip(["day", "month", "year"], text.split(".")))))
	url = "/ext/memForDate?"+args
	return [nodes.reference(text, text, internal=False, refuri=url)
		], []
roles.register_local_role("dateref", _daterefRolefunc)


class MemOTDTable(model.TableDef):
	c_000_year = 'INTEGER'
	c_010_month = 'INTEGER'
	c_020_day = 'INTEGER'
	c_030_saliency = 'INTEGER'
	# evtext is compiled HTML (the input comes in RST)
	c_040_evtext = 'TEXT'
	# base name of the file this event came from (when re-importing a file,
	# all events orginating from that file are removed.  So, when renaming
	# a file, you may need to clean up manually).
	c_050_srcname = 'TEXT'
	px_000_monthdayindex = (
		'CREATE INDEX IF NOT EXISTS memotd_md ON memotd (month, day)')


class MemOTD(object):
	"""The manager for the Memory of the Day table.
	"""
	_cachedDate = None

	def __init__(self, conn):
		self.conn = conn

	def getForDate(self, year, month, day):
		"""returns a (possibly empty) list of events for the given date.
		"""
		with self.conn.autoCursor() as cursor:
			return list(
				cursor.execute('select * from memOTD'
					' where year=? and month=? and day=?',
					(year, month, day)))

	def getCandidates(self, month, day):
		with self.conn.autoCursor() as cursor:
			return list(
				cursor.execute('select * from memOTD where month=? and day=?',
					(month, day)))

	def getScoreForMemory(self, memory, forYear=None):
		if forYear is None:
			forYear = base.now().year
		memYear, score = memory[0], memory[3]
		for div, bonus in BONUSES_FOR_ANNIVERSARIES:
			if not (forYear-memYear)%div:
				score += bonus
		return score

	def getScored(self, candidates):
		"""returns a sorted list of tuples (score, event) for a list
		of memOTD records canditadates.

		The list is sorted descending by score.
		"""
		scoredCands = sorted([(self.getScoreForMemory(mem), mem)
			for mem in candidates])
		scoredCands.reverse()
		return scoredCands

	def getDiffPhrase(self, rawMemory, refYear):
		yearDiff = refYear-rawMemory[0]
		if yearDiff==0:
			return _("passierte")
		elif yearDiff==1:
			return _("vor einem Jahr")
		elif yearDiff==-1:
			return _("in einem Jahr")
		elif yearDiff<0:
			return _("in %s Jahren")%(-yearDiff)
		else:
			return _("vor %s Jahren")%yearDiff

	def _chooseMemory(self):
		today = base.now()
		try:
			memOTD = self.getScored(
				self.getCandidates(today.month, today.day))[0][-1]
			return self.getDiffPhrase(memOTD, today.year), memOTD
		except IndexError:
			return None

	@property
	def memOTD(self):
		"""returns today's memory of choice.  This is cached for the day.

		It may be None if no memory exists for today.
		"""
		if base.now().date!=self._cachedDate:
			self._cachedMemory = self._chooseMemory()
			self._cachedDate = base.now().date
		return self._cachedMemory

	def formatMemory(self, phraseAndMemory):
		diffPhrase, (year, month, day, __, tx, __) = phraseAndMemory
		return T.p(class_='heute')["…",
			T.strong[diffPhrase],
			" – %d.%d.%d: "%(day, month, year),
			T.xml(tx)]

	@template.renderer
	def render_memOTD(self, request, tag):
		memOTD = self.memOTD
		if self.memOTD is None:
			return ""
		return tag(style="font-size: 80%;padding-left:4pt;padding-right:4pt;"
			"padding-top:1pt;padding-bottom:1pt;"
			"padding-right:4pt;background-color:#DDDDBB;-webkit-hyphens:auto;hyphens:auto",
			lang="de") [
			self.formatMemory(memOTD),
			T.p[T.a(href="/ext/memOTD")[_("Mehr Geschichte")]]]


def _ingestFile(conn, inFName):
	"""helps cmd_memingest by pulling in a single IF-file.
	"""
	parsed = []
	srcName = os.path.basename(inFName)
	with open(inFName, encoding="utf-8") as f:
		material = f.read()

	for rec in filter(operator.truth, [r.strip() for r in
			material.split("%%")]):
		try:
			rawDate, pri, tx = rec.split(";", 2)
			day, month, year = rawDate.split('.')

			tx = DATE_PATTERN.sub(r":dateref:`\1`", tx)

			parsed.append((int(day), int(month), int(year), int(pri),
				base.compileRSTX(tx.strip()+"\n"), srcName))
		except (ValueError, base.RSTError) as msg:
			sys.stderr.write("Bad Record: %s in %s: %s\n"%(rec, inFName, msg))

	with conn.autoCursor() as cursor:
		cursor.execute("delete from memOTD where srcname=?", (srcName,))
		cursor.executemany("insert into memOTD (day, month, year, saliency,"
			" evtext, srcname) VALUES (?, ?, ?, ?, ?, ?)",
			parsed)


class _MemOTDBase(pages.ExtensionPage):
	def __init__(self, conn, request, segments):
		pages.ExtensionPage.__init__(self, conn, request, segments)
		args = base.debytifyArgs(request.args)
		year, month, day = base.now().timetuple()[:3]
		self._year = self._parseFromArgs(args, "year", year)
		self._month = self._parseFromArgs(args, "month", month)
		self._day = self._parseFromArgs(args, "day", day)

	def _parseFromArgs(self, args, key, default):
		try:
			return int(args[key][0])
		except (KeyError, IndexError, ValueError):
			return default

	@template.renderer
	def oneMemory(self, request, tag):
		phraseAndMemory = tag.slotData
		return tag[self.manager.formatMemory(phraseAndMemory)]

	@template.renderer
	def memoryText(self, request, tag):
		return tag[T.xml(tag.slotData[4])]

	@template.renderer
	def thisdate(self, request, tag):
		return "%d.%d.%d"%(self._day, self._month, self._year)

	def data_year(self, request, tag):
		return self._year

	def data_month(self, request, tag):
		return self._month

	def data_day(self, request, tag):
		return self._day



def makeMemOTDPage(mgr):

	class MemOTDPage(_MemOTDBase):
		manager = mgr

		def data_candidates(self, request, tag):
			return [(self.manager.getDiffPhrase(m[1], self._year), m[1])
				for m in self.manager.getScored(
					self.manager.getCandidates(self._month, self._day))]

		loader = nevowc.XMLString(TEMPLATE)

	return MemOTDPage


def makeMemForDatePage(mgr):

	class MemForDatePage(_MemOTDBase):
		manager = mgr

		def data_candidates(self, request, tag):
			return self.manager.getForDate(self._year, self._month, self._day)

		loader = nevowc.XMLString(DATE_TEMPLATE)

	return MemForDatePage


def main(conn, args):
	"""-- imports a file for the memory of the day table.

	Each imported file must contain events separated by %% on a single
	line, where each event is defined by
	<day>.<month>.</year>;<saliency>;<restructured text>,
	where saliency is a small integer.
	"""
	from sofoterm import model
	model.ensureTable(conn, MemOTDTable("memOTD"))
	for arg in args:
		_ingestFile(conn, arg)


def init(rootPage):
	model.ensureTable(rootPage.conn, MemOTDTable("memOTD"))
	manager = MemOTD(rootPage.conn)
	pages.EventPage.addExtRenderer("memOTD",
		lambda other, request, tag: manager.render_memOTD(request, tag))
	pages.registerExtPage("memOTD", makeMemOTDPage(manager))
	pages.registerExtPage("memForDate", makeMemForDatePage(manager))

	altURL = base.completeURL("/ext/memOTD", rootPage.conn)
	A = rootPage.child_rss.AtomElements
	def memOTDItem():
		mem = manager.memOTD
		if not mem:
			return None
		diffPhrase, (year, month, day, __, tx, __) = mem
		content = "<p>%s – %d.%d.%d:</p><p>%s</p>"%(diffPhrase,
			day, month, year, tx)
		return A.entry[
			A.content(type="html")[content],
			A.id["%s/%s"%(altURL, id(mem))],
			A.link(href=altURL, type="text/html", rel="alternate"),
			A.title["Heute vor… "]]
	rootPage.child_rss.addEntryMaker(memOTDItem)
