"""
A sofoterm plugin for generation and handling of shortened URLs for events.

Long paths are arbitrary strings.  Short paths are simply integers in
some not too fancy serialization.
"""

import string

from twisted.web import resource

from sofoterm import _
from sofoterm import base
from sofoterm.model import dbinter
from sofoterm.pages import common


class ShortURLTable(dbinter.TableDef):
	# We use the OID as the short path int.
	c_000_fullpath = 'TEXT'
	pc_000_fullpathindex = (
		'CREATE INDEX IF NOT EXISTS shorturls_full ON shorturls (fullpath)')


def pathToInt(path):
	return int(path, 36)


digits = string.digits+string.ascii_lowercase

def intToPath(number):
	res = []
	while number>0:
		curRem = number%36
		res.append(digits[curRem])
		number = number//36
	res.reverse()
	return "".join(res)


def getfirst(conn, query, args):
	curs = conn.execute(query, args)
	res = curs.fetchall()
	if not res:
		raise ValueError("getfirst from empty result")
	return res[0][0]


class ShortPathBroker(resource.Resource):
	"""A facade to mapping short and long URLs, and a nevow resource to
	expose the functionality.

	As a page, it will redirect to a page based on a short URL if there's
	exactly one segment, or it will return plain text (for use with
	the revealShortURL render function) for the short URL from a long
	path in the q argument for a long path.
	"""
	def __init__(self, conn):
		self.conn = conn
		resource.Resource.__init__(self)

	def getShortForFull(self, fullPath):
		"""returns a short path for a fullPath or creates a new shortPath.
		"""
		try:
			res = getfirst(self.conn, 
				"SELECT OID FROM shorturls WHERE fullPath=?",
				(fullPath,))
		except ValueError:  # no short URL assigned yet
			curs = self.conn.execute(
				"INSERT INTO shorturls VALUES (?)", 
				(fullPath,))
			curs.close()
			self.conn.commit()
			return self.getShortForFull(fullPath)
		return "/s/"+intToPath(res)

	def getFullForShort(self, shortPath):
		"""returns a full path for a shortPath or None if shortPath is unknown.
		"""
		try:
			return getfirst(self.conn, 
				"SELECT fullpath FROM shorturls WHERE OID=?",
				(pathToInt(shortPath),))
		except ValueError:
			raise base.PageNotFound("'%s' is not a shortened path id"%shortPath)

	def render_GET(self, request):
		try:
			fullPath = request.args[b"q"][0]
		except (KeyError, ValueError):
			raise base.PageNotFound("No short URL")
		request.setHeader("content-type", "text/plain; charset=utf-8")
		return (_("Kurze URL für diesen Termin: %s")%(
			base.completeURL(self.getShortForFull(fullPath), self.conn))
		).encode("utf-8")

	def getChild(self, name, request):
		raise base.Redirect(base.completeURL(
			self.getFullForShort(base.debytify(name)), self.conn))


def render_revealShortURL(self, request, tag):
	return tag(class_="toBeReplaced",
		onclick="replaceTextWithDoc(this, '/s?q=%%2fevent%%2fn%%2f%s')"%
		self.event.id)


def init(rootPage):
	dbinter.ensureTable(rootPage.conn, ShortURLTable("shorturls"))
	rootPage.child_s = ShortPathBroker(rootPage.conn)
	common.EventPage.addExtRenderer("revealShortURL", render_revealShortURL)
