"""
Raw sqlite interface for sofoterm
"""

import contextlib
import datetime
import re
import time

import sqlite3 as sqlite
from sqlite3 import IntegrityError #noflake: for registration

from sofoterm import base


MODEL_VERSION = 5

class _VersionMismatch(Exception):
	"""used internally to tell getConnection to run an update.
	"""
	def __init__(self, dbVersion):
		self.dbVersion = dbVersion


class TableDef(object):
	"""a definition of a table in an SQLite DB.

	The schema is defined in attributes in c_nnn_whatever names, post
	creation commands in attributes using pc_nnn_whatever names.  All
	this is to make inheritance useful for these table defs.

	Attributes include:

	* name -- the table name
	* schema -- a list of column name, column type pairs
	* columns -- a list of the column names
	"""
	def __init__(self, name):
		self.name = name
		self.schema = self._makeSchema()
		self.columns = [n for n, t in self.schema]

	_toNameRE = re.compile(r"^c_\d+_(.*)")

	def _makeSchema(self):
		return [(mat.group(1), getattr(self, mat.group()))
		 	for mat in (self._toNameRE.match(name)
				for name in sorted(dir(self)))
			if mat]

	def _iterPostCreateStatements(self):
		for cmdAttr in (s for s in sorted(dir(self)) if s.startswith("pc_")):
			yield getattr(self, cmdAttr)

	def iterBuildStatements(self):
		macros = self.__dict__.copy()
		macros["ddl"] = ", ".join("%s %s"%t for t in self.schema)
		yield "CREATE TABLE IF NOT EXISTS %(name)s (%(ddl)s)"%macros
		for statement in self._iterPostCreateStatements():
			yield statement%macros


class EventTable(TableDef):
	"""The table storing the primary information on events.
	"""
	c_000_id = 'INTEGER PRIMARY KEY'
	c_010_bdate = 'DATE'
	c_020_btime = 'TIME'
	c_030_teaser = 'TEXT'
	c_040_sponsor = 'TEXT'
	c_050_place = 'TEXT'
	c_060_fulltext = 'TEXT'
	c_070_fulltextCompiled = 'TEXT'
	c_080_attachments = 'INTEGER DEFAULT 0'
	c_090_fdate = 'DATE'
	c_100_private = 'BOOL DEFAULT 0'
	c_110_lastChanged = 'REAL'     # This is interpreted in UTC
	c_120_lastChanger = 'TEXT'

	pc_000_datesindex = (
		'CREATE INDEX IF NOT EXISTS events_dates ON events (bdate)')
	pc_010_tagstrigger = (
		'CREATE TRIGGER IF NOT EXISTS remove_tags AFTER DELETE ON events BEGIN'
			' DELETE FROM tags WHERE tags.id=OLD.id; END')


class TagsTable(TableDef):
	"""the table storing tags for events.
	"""
	c_000_id = 'INTEGER'
	c_010_tag = 'TEXT'

	pc_010_idindex = 'CREATE INDEX IF NOT EXISTS tags_id ON tags (id)'
	pc_020_tagsindex = 'CREATE INDEX IF NOT EXISTS tags_tag ON tags (tag)'


class ConfigTable(TableDef):
	"""the table containing simple configuration.
	"""
	c_010_key = 'TEXT'
	c_020_value = 'TEXT'

	pc_010_keyindex = 'CREATE INDEX IF NOT EXISTS config_key ON %(name)s (key)'


eventsTD = EventTable("events")
tagsTD = TagsTable("tags")
configTD = ConfigTable("config")


def ensureTable(conn, tableDef):
	"""makes sure the TableDef instance tableDef exists on the database.
	"""
	curs = conn.cursor()
	for stmt in tableDef.iterBuildStatements():
		curs.execute(stmt)
	curs.close()


def ensureTables(conn):
	"""creates tables if necessary.
	"""
	for tableDef in [eventsTD, tagsTD, configTD]:
		ensureTable(conn, tableDef)
	
	# for now, I special-case the index table because it's full of
	# odd syntax; if you change the schema here, don't forget to update
	# the cli.cmd_reindex
	conn.execute("CREATE VIRTUAL TABLE IF NOT EXISTS"
		" event_index"
		" USING fts5(teaser, place, fulltext, bdate, content='events', content_rowid='id')")
	conn.execute("CREATE TRIGGER IF NOT EXISTS ev_add"
		" AFTER INSERT ON events BEGIN"
  	"   INSERT INTO event_index (rowid, teaser, place, fulltext, bdate)"
  	"     VALUES (new.id, new.teaser, new.place, new.fulltext, new.bdate);"
		" END")
	conn.execute("CREATE TRIGGER IF NOT EXISTS ev_del"
		" AFTER DELETE ON events BEGIN"
  	"   INSERT INTO event_index (event_index, rowid, teaser, place, fulltext, bdate)"
  	"      VALUES('delete', old.id, old.teaser, old.place, old.fulltext, old.bdate);"
		" END")
	conn.execute("CREATE TRIGGER IF NOT EXISTS ev_update"
		" AFTER UPDATE ON events BEGIN"
  	"   INSERT INTO event_index (event_index, rowid, teaser, place, fulltext, bdate)"
  	"      VALUES('delete', old.id, old.teaser, old.place, old.fulltext, old.bdate);"
   	"   INSERT INTO event_index (rowid, teaser, place, fulltext, bdate)"
  	"     VALUES (new.id, new.teaser, new.place, new.fulltext, new.bdate);"
		" END")


def _convertTime(s):
	return datetime.time(*time.strptime(base.debytify(s), "%H:%M")[3:5])
sqlite.register_converter("TIME", _convertTime)
def _adaptTime(t):
	return t.strftime("%H:%M")
sqlite.register_adapter(datetime.time, _adaptTime)

_BOOL_LITERALS = {
	b"True": True,
	b"False": False,
}

def _convertBool(s):
	if s in _BOOL_LITERALS:
		return _BOOL_LITERALS[s]
	return bool(int(s))
sqlite.register_converter("BOOL", _convertBool)
def _adaptBool(t):
	return int(t)
sqlite.register_adapter(bool, _adaptBool)


class WrappedConnection(sqlite.Connection):
	"""An sqlite connection we can add attributes to.

	(we need this so archive connections can have stconfs, too).

	Also, this has a method autoCursor that does automatic transaction
	management in a context manager.
	"""
	@contextlib.contextmanager
	def autoCursor(self):
		cursor = self.cursor()
		try:
			yield cursor
		except:
			self.rollback()
			raise
		self.commit()


class ConnectionWithConfig(WrappedConnection):
	"""A connection carrying our config object with it.

	I know it's not pretty, but better just pass around one object
	instead of two.

	The connection also has an idea when the last change was performed.
	It is, of course, the application's job to notify the connection of
	changes (i.e., it does not inspect queries for ones that may effect
	changes).
	"""
	def __init__(self, dsn, *args, **kwargs):
		sqlite.Connection.__init__(self, dsn, *args, **kwargs)
		ensureTables(self)
		self.stconf = base.Config(self, dsn)
		self.lastChange = time.time()

	def getArchiveConnection(self):
		if not hasattr(self, "_archiveConnection"):
			self._archiveConnection = sqlite.connect(
				self.stconf.archiveDSN,
				detect_types=sqlite.PARSE_DECLTYPES|sqlite.PARSE_COLNAMES,
				factory=WrappedConnection)
			self._archiveConnection.stconf = self.stconf
			ensureTables(self._archiveConnection)
		return self._archiveConnection


class AppConnection(ConnectionWithConfig, base.HookMixin):
	"""The DB connection passed around within sofoterm.

	It's ConnectionWithConfig (with all the reservations), but in
	addition checks if the versions of the DB tables and the
	model match and raise a _VersionMismatch if not.

	There's a lastUpdate attribute giving the UTC unix time stamp of the last
	"effective" change to the database.  It is "bumped" on Edit and Reload
	events.  Post an edit event with a None event to bump it manually.
	"""
	def __init__(self, dsn, *args, **kwargs):
		ConnectionWithConfig.__init__(self, dsn, *args, **kwargs)
		self.initHooks()
		self.lastUpdate = time.time()
		self.subscribe("reload", self._processReload)
		self.subscribe("edit", self._processEdit)
		try:
			if self.stconf.modelVersion!=MODEL_VERSION:
				self.close()
				raise _VersionMismatch(self.stconf.modelVersion)
		except AttributeError:  # DB has no version key, probably just
			                      # created.
			self.stconf.addKeyValue("modelVersion", MODEL_VERSION)

	def _processReload(self, conn):
		conn.stconf.clearCaches()
		conn.lastUpdate = time.time()

	def _processEdit(self, conn, event):
		self.lastUpdate = time.time()


def runUpdate(dsn, dbVersion):
	"""makes backups of our db tables and then runs the necessary
	backup actions.
	"""
	if dbVersion>MODEL_VERSION:
		raise base.ReportableError("This database is too new for this"
			" software.")
	base.backup(dsn)
	base.backup(base.computeArchiveDSN(dsn))
	conn = sqlite.connect(dsn,
		detect_types=sqlite.PARSE_DECLTYPES|sqlite.PARSE_COLNAMES,
		factory=ConnectionWithConfig)

	from sofoterm.model import updates
	updates.update(conn, dbVersion, MODEL_VERSION)
	updates.update(conn.getArchiveConnection(), dbVersion, MODEL_VERSION)
	conn.getArchiveConnection().commit()
	conn.commit()


def getConnection(dsn, failOnVersion=False):
	try:
		return sqlite.connect(dsn,
			detect_types=sqlite.PARSE_DECLTYPES|sqlite.PARSE_COLNAMES,
			factory=AppConnection)
	except _VersionMismatch as ex:
		if failOnVersion:
			raise
		runUpdate(dsn, ex.dbVersion)
		return getConnection(dsn, failOnVersion=True)
