"""
Code for formatting our data in iCal (RFC 5545)

Docs at https://tools.ietf.org/html/rfc5545
Validator at https://icalendar.org/validator.html

TODO: ATTACH, Link to event

Example from Wikipedia:

BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//hacksw/handcal//NONSGML v1.0//EN
BEGIN:VEVENT
UID:uid1@example.com
DTSTAMP:19970714T170000Z
ORGANIZER;CN=John Doe:MAILTO:john.doe@example.com
DTSTART:19970714T170000Z
DTEND:19970715T035959Z
SUMMARY:Bastille Day Party
END:VEVENT
END:VCALENDAR
"""

import re
import time
import traceback
import warnings

from sofoterm import base


def localToUTC(datetime):
	"""returns UTC for a naive datetime object interpreted as whatever
	the machine thinks is its local time.
	"""
	return time.gmtime(time.mktime(datetime.timetuple()))


def addShell(tx):
	"""adds the VCALENDAR shell and the global properties to tx.

	tx itself has to be legal iCalendar material.
	"""
	return "\r\n".join(["BEGIN:VCALENDAR",
		"VERSION:2.0",
		"PRODID:http://sofo-hd.de",
		tx,
		"END:VCALENDAR"])

SAFE_VAL = re.compile('[!#-*.-9<-Z^-~-]{0,73}$')


def quoteICalMaterial(stuff):
	"""returns stuff quoted according to iCal standards.
	"""
	return '"%s"'%stuff.replace("\\", "\\\\"
		).replace('"', '\\"'
		).replace("\n", "\\n")


def breakICalMaterial(stuff, maxChars=55):
	"""returns a string stuff broken such that each line is shorter 
	than maxChars chars.

	This assumes unix line separators on stuff and will not change other
	whitespace (which, we hope, will let us transmit rstx unscathed.
	The return value will have iCal line separators.

	(in material with much non-ASCII this will still overflow iCal's
	75 char limit, but that's a SHOULD only).
	"""
	if stuff is None:
		return None

	if SAFE_VAL.match(stuff):
		return stuff

	stuff = quoteICalMaterial(stuff)
	result = []

	for breakpoint in range(0, len(stuff), maxChars):
		result.append(" "+stuff[breakpoint:breakpoint+maxChars])
	result[0] = result[0][1:]
	return "\r\n".join(result)


def _makePropFactory(propName, formatter=breakICalMaterial):
	def factory(val, ev):
		return "%s:%s"%(propName, formatter(val))
	return factory


def makeEventDate(val, ev):
	return time.strftime(
		"DTSTART:%Y%m%dT%H%M%SZ", localToUTC(ev.timestamp))


def makeLastModifiedValue(val, ev):
	return "DTSTAMP:%s"%(
		time.strftime("%Y%m%dT%H%M%SZ",time.gmtime(val)))


def ignore(val, ev):
	pass


ICAL_FORMATTERS = {
	"id": _makePropFactory("UID", 
		lambda v: str(v)+"@sofo-hd.de"),
	"sponsor": ignore,
	"private": ignore,
	"bdate": makeEventDate,
	"btime": ignore,
	"teaser": _makePropFactory("SUMMARY"),
	"place": _makePropFactory("LOCATION"),
	"fulltext": _makePropFactory("DESCRIPTION"),
	"fulltextCompiled": ignore,
	"attachments": ignore,
	"lastChanged": makeLastModifiedValue,
	"lastChanger": ignore,
}


def formatAsICal(ev, conn):
	"""returns iCalendar material for an Event ev.
	"""
	record = []
	for key, val in ev.iterParts():
		if val is None:
			continue

		res = ICAL_FORMATTERS[key](val, ev)
		if res is not None:
			record.append(res)
	
	record.append("URL:"+breakICalMaterial(
		base.completeURL(f"/event/{ev.id}", conn)))

	return "\r\n".join(["BEGIN:VEVENT"
		]+record+[
		"END:VEVENT"])


def exportAsICal(conn, evList):
	"""returns evList serialised into iCal.

	The result is a unicode string.
	"""
	result = []
	for ev in evList:
		try:
			result.append(formatAsICal(ev, conn))
		except:
			# one date couldn't be formatted, ignore it
			warnings.warn("ev %s could not be formatted in iCal"%repr(ev))
			traceback.print_exc()
	return addShell("\r\n".join(result))+"\r\n"
