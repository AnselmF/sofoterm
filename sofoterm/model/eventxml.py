"""
Import and export of events to and from XML.
"""

import datetime
from xml.etree import ElementTree as ET

from sofoterm import base
from sofoterm.model import event


def _makeAttrDefaults():
	sample = event.TaggedEvent()
	return dict(sample.iterAttributes())


_privateKeys = set(["lastchanger", "sponsor"])

def serializeEvent(ev, defaults=_makeAttrDefaults(), private=False):
	"""returns an ElementTree object containing XML for an Event.

	If private is true, sponsor keys will be dumped as well.
	"""
	root = ET.Element("event")
	for key, val in ev.iterParts():
		if not private and key in _privateKeys:
			continue
		if val!=defaults.get(key):
			el = ET.SubElement(root, key)
			el.text = str(val)
	return root


def serializeEventList(evList, private=False):
	"""returns an ElementTree object containing XML for an EventList.
	"""
	root = ET.Element("eventList")
	for ev in evList:
		root.append(serializeEvent(ev, private=private))
	return root


def dump(ob, destFile, private=False):
	"""dumps XML for ob to destFile.

	ob can be an Event or an EventList.
	"""
	if isinstance(ob, event.EventList):
		serFunction = serializeEventList
	else:
		serFunction = serializeEvent
	et = serFunction(ob, private=private)
	ET.ElementTree(et).write(destFile, "utf-8")


class PushbackIterator(object):
	def __init__(self, iter):
		self.iter = iter
		self.pushedBack = []

	def __iter__(self):
		while True:
			if self.pushedBack:
				yield self.pushedBack.pop()
			else:
				yield next(self.iter)

	def pushBack(self, el):
		self.pushedBack.append(el)


def _defaultParser(val):
	return val

_parsers = {
	"bdate": ("bdate", lambda val: datetime.date(*list(map(int, val.split("-"))))),
	"btime": ("btime", lambda val: datetime.time(*list(map(int, val.split(":"))))),
	"attachments": ("attachments", int),
	"tag": ("tags", _defaultParser),
}
def _parseEvent(elIter, evClass=event.Event):
	kws = {"tags": set()}
	for _, el in elIter:
		if el.tag=="event":
			break
		else:
			kw = el.tag
			kw, parser = _parsers.get(kw, (kw, _defaultParser))
			oldval = kws.get(kw)
			if isinstance(oldval, set):
				kws[kw].add(parser(el.text))
			else:
				kws[kw] = parser(el.text)
	ev = evClass(**kws)
	if kws.get("fulltext"):
		try:
			ev.compileFulltext()
		except base.RSTError: # XXX TODO: silent failure here is bad.
			pass
	return ev


def parseEvent(srcFile, evClass=event.Event):
	"""reads and event from srcFile.
	"""
	return _parseEvent(ET.iterparse(srcFile), evClass)


def parseEventList(srcFile, evClass=event.Event):
	events = []
	elIter = PushbackIterator(ET.iterparse(srcFile))
	for evType, el in elIter:
		if el.tag=="eventList":
			break
		else:
			elIter.pushBack((evType, el))
			events.append(_parseEvent(elIter, evClass))
	return event.EventList(events)
