"""
Formatting things in plain text.
"""

import re
import textwrap

from sofoterm import base
from sofoterm.model.places import PLACES


def _continueText(parts, separator, addition):
	"""helps _formatEvent (and is not good for much else, I guess).
	"""
	if parts:
		parts.extend((separator, addition))
	else:
		parts.append(addition)


def _deHTMLify(s):
	"""returns s with all tag content killed.
	"""
	return re.sub("<[^>]+>", "", s)


def _formatShortplace(ev):
	if ev.place in PLACES:
		return _deHTMLify(PLACES[ev.place][0])
	else:
		return ev.place


def formatEvent(conn, ev, dateMod=None):
	res = []
	if ev.btime:
		res.append(ev.btime.strftime("%H.%M"))
	if ev.place:
		_continueText(res, ", ", _formatShortplace(ev))
	_continueText(res, ": ", ev.teaser)
	res = textwrap.wrap("".join(res))
	if ev.fulltext and ev.fulltext.startswith("http://"):
		res.append(ev.fulltext.split()[0])
	else:
		res.append(base.completeURL("/event/%s"%ev.id, conn))

	if dateMod:
		res[:0] = [dateMod]

	return "* "+("\n  ".join(s for s in res))


def formatStandaloneEvent(conn, ev):
	res = []
	if ev.fdate:
		res.append(ev.fdate.strftime("%d.%m.%Y")+"-")
	res.append(ev.bdate.strftime("%d.%m.%Y"))
	return formatEvent(conn, ev, "".join(res))


def formatEventList(conn, evList):
	res = []
	for curDate, evs in evList.iterGrouped():
		res.append(">>> %s, %s <<<"%(base.weekdays[curDate.weekday()],
			curDate.strftime("%d.%m.%Y")))
		res.append("")
		for ev in evs:
			res.append(formatEvent(conn, ev))
			res.append("")
		res.extend(["", ""])
	return "\n".join(res)


def formatStandaloneEventList(conn, evList):
	res = []
	for ev in evList:
		res.append(formatStandaloneEvent(conn, ev))
	return "\n\n".join(res)
