"""
Predefined places.  First inline, maybe read from some file later.
"""

from sofoterm.base import _


_RAW_PLACES = {
	'_test':[_( 'Cherry Lane, <a href="http://www.xanadu.org">Xanadu</a>')],
	'_testGeo':[_( 'Cherry Lane, <a href="http://www.xanadu.org">Xanadu</a>'),
		-40, 20],
	'dai':[_('Deutsch-Amerikanisches Institut, Sophienstr. 12'),
		8.6940, 49.4078],
	'Gumbelraum':[_('Emil-Julius-Gumbelraum, Karlstorbahnhof'),
		8.7190, 49.4146],
	'ZFB':[_('Zentrales Fachschaften-Büro, Albert-Ueberle-Straße 3-5'),
		8.6947, 49.4144],
	'sand':[_('StuRa-Räume, Sandgasse 7'),
		8.70487, 49.41102],
	'stura':[_('StuRa-Büro, Albert-Ueberle-Straße 3-5'),
		8.6947, 49.4144],
	'vhs':[_('Volkshochschule, Bergheimer Str. 76'), 8.6806, 49.4080],
	'khg':[_('Katholische Hochschulgemeinde, Neckarstaden 32'),
		8.7026, 49.4128],
	'gegendruck':[_('Cafe Gegendruck, Fischergasse 2'), 8.7104, 49.4131],
	'ebert':[_('Ebert-Gedenkstätte, Pfaffengasse 18'), 8.7082, 49.4124],
	'sinti':[_('Dokumentations- und Kulturzentrum Deutscher Sinti und Roma,'
		' Bremeneckgasse 2'), 8.7113, 49.4110],
	'himmelheber':[_('Buchhandlung Himmelheber, Theaterstraße 16'),
		8.7040, 49.4097],
	'rk':[_('Theater im Romanischen Keller, Seminarstraße 3 (Ecke'
		' Kettengasse)'), 8.7083, 49.4102],
	'juz':[_('JUZ, Käthe-Kollwitz-Straße 2-4, Mannheim am Neuen Meßplatz'),
		8.4715, 49.5056],
	'esg':[_('Evangelische Studierendengemeinde, Plöck 66'), 8.7052, 49.4094],
	'ibw':[_('Institut für Bildungswissenschaft, Akademiestr. 4'),
		8.6979, 49.4095],
	'nuni':[_('Neue Uni, Uniplatz'), 8.7065, 49.4105],
	'dalang':[_('Caf&eacute; da lang, IBW, Akademiestraße 3'),
		8.6980, 49.4100],
	'stabu':[_('Stadtbücherei, Poststraße 15'), 8.6865, 49.4067],
	'teufel':[_('Zum Teufel, Kirchheimer Weg 2'), 8.6771, 49.3954],
	_('häll'):[_('Häll, Kirchheimer Weg 2'), 8.6771, 49.3954],
	'weltladen':[_('Weltladen, Heugasse 2'), 8.7077, 49.4114],
	'igmgew':(_('GEW/IGM-Haus, Friedrich-Ebert-Anlage 24 '),
		8.698317, 49.408129),
	'prisma': (_('Prisma Mehrgenerationenhaus, Richard-Wagner-Straße 6'),
		8.686514, 49.424365,),
	'ZEP': (_('ZEP, Zeppelinstraße 1'), 8.685771, 49.421038),
	'villa': (_('Villa Nachttanz, Wieblinger Weg'), 8.64318, 49.4129),
	'malecon': (_('Malecón, Mittelbadgasse 3'), 8.710704, 49.411594),
	'zeitungsleser': (_('Zeitungsleser, St. Anna-Gasse'), 8.693979, 49.409743),
	'igh': (_('Internationale Gesamtschule Heidelberg, Baden-Badener-Str. 14'), 8.67922, 49.37746),
	'hsstura': (_('„Neuer“ Hörsaal Physik, oberhalb Albert Ueberle-Str 3-5'),
		8.69561, 49.41434),
	'bergheim': (_('Campus Bergheim, Bergheimer Str. 58'),
		8.68420, 49.40842),
	'gerontologie': (_('Institut für Gerontologie, Bergheimer Str. 20'),
		8.69126, 49.40908),
	'forumampark': ('Forum am Park, Poststr. 11', 8.68945, 49.40698),
	'welthaus': ('Welthaus Heidelberg (im Hauptbahnhof)',
		8.67643, 49.40369),
	'appelei': (_("Appel un' Ei, unter dem Mensavordach INF 305,"
		" östlich vom Cafe Botanik"), 8.67075, 49.41518),
	'breidenbach': (_("Breidenbach Studios, Hebelstraße 18"),
		8.67850, 49.39672),
	'laden': (_("Laden für Kultur und Politik, Kaiserstraße 62"),
		8.68414, 49.40344),
	'rabatz': (_("Rabatz in der Hagebutze, Rheinstr. 4"),
		8.68221, 49.38887),
	'karlstorkino': (_("Karlstorkino, Marlene-Dietrich-Platz 3"),
		49.38684, 8.68092),
	"ca": ("Collecium Academicum, Mendelejewplatz 1",
		49.37574, 8.68630),
}

OUTDATED = frozenset(["fsk", _('häll'), "teufel", "malecon", "malecon",
	"teufel", "dalang", "Gumbelraum", "ZFB", "himmelheber"])

PLACES = dict((k, v)
	for (k,v) in _RAW_PLACES.items())

PLACES_SELECT = ", ".join(s for s in sorted(PLACES)
	if not s.startswith("_") and not s in OUTDATED)
