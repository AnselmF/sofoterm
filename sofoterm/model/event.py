"""
Event classes.

Events are identified through an id, the uniqueness of which is
guaranteed through a the database that uses it as a primary key.

Otherwise, the event structure is given in *two* places: The database
schema (DDL) in dbinter and the class definitions in this module, and the
two are linked through the toDB and fromId methods of the classes.
This inconvenience saves us an ORM, which I think is worth it.
"""



import calendar
import datetime
import os
import time

from sofoterm import base
from sofoterm.model import dbinter


class Event(base.AutoNode):
	"""The base class of an event, basically what comes out of the database.
	"""
	def _get_bdate(self):
		return self.__bdate

	def _set_bdate(self, val):
		if val is None:
			self.__bdate = base.Undefined
		elif hasattr(val, "date"):  # it's a datetime
			self.__bdate = val.date()
		else:
			self.__bdate = val

	_a_id = base.Undefined      # the event's unique id
	# bdate is the (end) date of an event.  You can also pass in datetimes,
	# but their time part will be ignored.
	_a_bdate = property(_get_bdate, _set_bdate)
	_a_btime = None                # time may be missing for day-long events
	_a_teaser = base.Undefined  # a title, more or less
	_a_sponsor = base.Undefined # group that entered the date
	_a_place = None               # where it's at, may be an abbrev
	_a_fulltext = None            # fulltext, as restructured text
	_a_fulltextCompiled = None    # fulltext, compiled to div-able HTML
	_a_attachments = 0            # Attachment info (1=PDF available)
	_a_fdate = None               # for multiday events, first date
	_a_private = False   # Hide in EventLists without tag restrictions
	_a_lastChanged = None         # seconds since epoch of last change
	_a_lastChanger = None         # sponsor key that caused the last change

	# timestamp when no date is given
	invalidTimestamp = datetime.datetime(1985, 8, 27)
	# time assumed for all-day events
	timeForAllDay = datetime.time(0, 5)

	@classmethod
	def fromTuple(cls, valTuple):
		"""returns an event from a tuple taken from the DB.
		"""
		fields = dict(list(zip(dbinter.eventsTD.columns, valTuple)))
		return cls(**fields)

	@classmethod
	def fromId(cls, id, conn, sponsor=None):
		"""gets an event with id from the database.
		"""
		cursor = conn.execute("SELECT * FROM events WHERE id=?", (id,))
		res = cursor.fetchall()
		if not res:
			raise base.EventNotFound(id)
		ev = cls.fromTuple(res[0])
		if sponsor:
			ev.lastChanger = sponsor
		return ev

	@classmethod
	def create(cls, sponsor, conn, curId=None):
		"""creates and returns a new event with a valid id and sponsor filled in.

		If you must, you can wish for an id, but that's usually not a good idea,
		and you may get an event with a different id anyway.
		"""
		if curId is None:
			curId = int(time.mktime(base.now().timetuple()))
		cursor = conn.cursor()
		for i in range(100):
			try:
				cursor.execute("INSERT INTO events (id, sponsor) VALUES (?, ?)",
					(curId, sponsor))
			except dbinter.IntegrityError: # id already taken, try next
				curId += 1
			else:
				break
		else:
			raise base.InternalError("All ids seem to be taken")
		return cls.fromId(curId, cursor, sponsor)

	@classmethod
	def createWith(cls, sponsor, conn, **kwargs):
		"""creates a new event like create, but adds attributes from the keyword
		arguments.
		"""
		curId = None
		if "id" in kwargs:
			curId = kwargs["id"]
		ev = cls.create(sponsor, conn, curId=curId)
		ev = ev.change(**kwargs)
		ev.compileFulltext()
		ev.toDB(conn)
		return ev

	@classmethod
	def fromEvent(cls, sponsor, conn, original):
		"""returns a clone of original.

		self shares all attributes from otherEvents except id, which is new.
		"""
		clone = cls.create(sponsor, conn)
		clone.modify(**dict(
			(k,v) for k,v in original.iterAttributes() if k!="id"))
		clone.attachments = 0
		clone.toDB(conn)
		return clone

	def _setupNode(self):
		self.inPast = bool(self.bdate and self.bdate<base.now().date())
		self._setupNodeNext(Event)

	@property
	def timestamp(self):
		if not self.bdate:
			return self.invalidTimestamp
		if self.btime:
			return datetime.datetime.combine(self.bdate, self.btime)
		return datetime.datetime.combine(self.bdate, self.timeForAllDay)

	def assertValid(self):
		"""raises an InvalidEvent exception if self is invalid.
		"""
		for k, val in self.iterAttributes():
			if val is base.Undefined:
				raise base.InvalidEvent("'%s' is a mandatory attribute"%k)

	def isComplete(self):
		"""returns True if self has enough data to be formatted.

		This, right now, is bdate and teaser.
		"""
		return bool(self.bdate and self.teaser)
			
	def toDB(self, conn):
		"""updates this event in the database.

		This raises an error if the event has not been in the db yet.

		Only attributes from the original events table are dumped here.
		Any additional information must be handled by subclasses.
		"""
		self.assertValid()
		self.lastChanged = base.now(utc=True).timestamp()
		assigns = ", ".join("%s=?"%name for name in dbinter.eventsTD.columns)
		values = [getattr(self, name) for name in dbinter.eventsTD.columns]
		values.append(self.id)
		cursor = conn.execute("UPDATE events SET %s WHERE id=?"%assigns, values)
		if cursor.rowcount!=1:
			raise base.InvalidEvent("Can only toDB events already in DB")

	def save(self, conn):
		"""saves this event to the database.

		This is not usually what you want -- normally, you allocate an event
		using create and then use toDB to update it.  This method is mainly
		for entering unmarshalled data.

		If an event with the respective id already exists, you'll
		get an sqlite3.IntegrityError.
		"""
		conn.execute("INSERT INTO events (id) VALUES (?)", (self.id,))
		self.toDB(conn)

	def getFormData(self):
		"""returns self's attributes ready for formal forms.

		See pages.Editor.
		"""
		base = dict(self.iterAttributes())
		return base

	def processFormData(self, data):
		"""extracts changes from a formal form data.

		See pages.Editor.
		"""
		for k,v in data.items():
			if hasattr(self, "_a_"+k):
				setattr(self, k, v)
		self.compileFulltext()
	
	def modify(self, **kwargs):
		"""updates event attributes from keyword arguments.

		In contrast to change, this happens in-place.

		Although the changes happen in-place, the event is returned.  This
		is for convenience so you can say change().toDB().
		"""
		if "id" in kwargs:
			raise ValueError("Cannot modify id in-place, use change.")
		for k, v in kwargs.items():
			if not hasattr(self, "_a_"+k):
				raise ValueError("Invalid event attribute: '%s'"%k)
			setattr(self, k, v)
			if k=="fulltext":
				self.compileFulltext()
		return self

	def iterParts(self):
		"""iterates over key-value pairs defining this event.
		"""
		for key in dbinter.eventsTD.columns:
			val = getattr(self, key, None)
			if val is not None and val is not base.Undefined:
				yield key, getattr(self, key)

	def compileFulltext(self):
		"""updates the compiled fulltext.

		This does not happen automatically at assignment because that
		would open a huge can of worms.  Instead, it needs to be run
		manually.

		processFromData does it automatically, though.  As a convenience,
		in returns the object, so you can just insert the call in some
		append() or somesuch.
		"""
		if self.fulltext:
			self.fulltextCompiled = base.compileRSTX(self.fulltext)
		else:
			self.fulltextCompiled = None
		return self

	def delete(self, conn):
		"""deletes this Event.
		"""
		# associated tags are being deleted by a trigger within the DB.
		conn.execute("DELETE FROM events WHERE id=?", (self.id,))

	def getAttachmentName(self):
		return os.path.join(base.getAppPath(), "attachments",
			"%s.pdf"%(self.id))

	def setAttachment(self, conn, content):
		"""saves content to this event's attachment.

		Previously existing attachments are overwritten.
		"""
		fName = self.getAttachmentName()
		if not os.path.isdir(os.path.dirname(fName)):
			os.makedirs(os.path.dirname(fName))
		with open(fName, "wb") as f:
			f.write(content)
		self.attachments = 1
		self.toDB(conn)

	def removeAttachment(self, conn):
		"""deletes the attachment for this event.

		It is not an error to remove non-existing attachments.
		"""
		fName = self.getAttachmentName()
		if os.path.exists(fName):
			os.unlink(fName)
		self.attachments = 0
		self.toDB(conn)


class TaggedEvent(Event):
	"""An event having tags.

	This is a class of its own since there's a new table coming into
	play.
	"""
	_a_tags = set()

	@classmethod
	def fromId(cls, id, conn, sponsor=None):
		ev = super(TaggedEvent, cls).fromId(id, conn, sponsor)
		cursor = conn.execute("SELECT tag FROM tags WHERE id=?", (ev.id,))
		ev.tags = set(r[0] for r in cursor.fetchall())
		return ev
	
	def toDB(self, conn):
		Event.toDB(self, conn)
		cursor = conn.execute("DELETE FROM tags WHERE id=?", (self.id,))
		if self.tags:
			cursor.executemany("INSERT INTO tags (id, tag) VALUES (?,?)",
				[(self.id, tag) for tag in self.tags])
		else:
			cursor.execute("DELETE FROM tags WHERE id=?", (self.id,))

	def getTagsFromDB(self, conn):
		cursor = conn.execute("SELECT tag FROM tags WHERE id=?", (self.id,))
		self.tags = set(t[0] for t in cursor.fetchall())

	def iterParts(self):
		for pair in Event.iterParts(self):
			yield pair
		for t in self.tags:
			yield "tag", t

	def processFormData(self, data):
		incoming = data.pop("tags", None)
		if incoming is None:
			self.tags = set()
		else:
			self.tags = set(incoming)
		return Event.processFormData(self, data)


class EventList(object):
	"""a list of events created from the database.

	An event list can be constructed with a list of events you already
	have, but you'd usually use the alternative constructor fromSelectors.
	"""
	def __init__(self, evList):
		self.evList = sorted(evList, key=lambda ev: ev.timestamp)
	
	def __iter__(self):
		return iter(self.evList)

	def __len__(self):
		return len(self.evList)

	def __bool__(self):
		return bool(self.evList)

	@classmethod
	def fromQuery(cls, conn, query, vals, evClass=Event):
		"""returns an EventList containing events matching query with vals.

		evClass can be TaggedEvent, but that's slow.
		"""
		if evClass is TaggedEvent:
			tagsCursor = conn.cursor()
			def evBuilder(t):
				ev = TaggedEvent.fromTuple(t)
				ev.getTagsFromDB(tagsCursor)
				return ev
		else:
			evBuilder = evClass.fromTuple
		cursor = conn.execute(query,  vals)
		return cls([evBuilder(t) for t in cursor.fetchall()])

	@staticmethod
	def _getDateCondition(startDate, endDate, maxFirstDate):
		# see fromSelectors
		frags, vals = [], []
		if startDate:
			frags.append("bdate>=?")
			vals.append(startDate)
		if endDate is not None:
			frags.append("bdate<=?")
			vals.append(endDate)
		if maxFirstDate is None:
			frags.append("fdate IS NULL")
		else:
			frags.append("fdate<?")
			vals.append(maxFirstDate)
		return " AND ".join(frags), vals

	@staticmethod
	def _getTagsRestrictionQuery(wantedTags, unwantedTags):
		"""returns a subquery that selects all events with one of wantedTags
		but none of unwantedTags.

		The method returns None if not tags restrictions exist.
		"""
		if wantedTags:
			wantedTags = list(wantedTags)
		if unwantedTags:
			unwantedTags = list(unwantedTags)

		selectWanted, vals = None, []
		if wantedTags:
			selectWanted = "SELECT DISTINCT id FROM tags WHERE %s"%(
				" OR ".join('tag=?' for v in wantedTags))
			vals.extend(wantedTags)

		selectUnwanted = None
		if unwantedTags:
			selectUnwanted = "SELECT DISTINCT id FROM tags WHERE %s"%(
				" OR ".join('tag=?' for v in list(unwantedTags)))
			vals.extend(unwantedTags)

		privateCond = ""
		if selectWanted:
			if selectUnwanted:
				subquery = "%s EXCEPT %s"%(selectWanted, selectUnwanted)
			else:
				subquery = selectWanted
		else:
			if selectUnwanted:
				subquery = "SELECT DISTINCT id FROM tags EXCEPT %s"%selectUnwanted
				# if no tags are speficically selected, we'd return private
				# events, too when there's unwanted tags.  Let's not do this
				# (we're ignoring sponsorness here).
				privateCond = " AND private=0"
			else:
				subquery = None
	
		if subquery:
			return ("(SELECT * FROM events WHERE"
				f" id IN ({subquery}){privateCond}) AS evs", vals)

	@staticmethod
	def _getDefaultRestrictionQuery(sponsor):
		"""returns a subquery that selects events to be shown in a default
		table.

		These are the non-private events and for sponsors those owned by them.

		This subquery is used when no tag restrictions are given.
		"""
		if sponsor:
			conds, args = ["1=1"], []
		else:
			conds, args = ["private=0"], []
		res = "(SELECT * FROM events WHERE %s)"%(" OR ".join(conds)), args
		return res

	@staticmethod
	def _getTableQueried(wantedTags, unwantedTags, sponsor):
		res = EventList._getTagsRestrictionQuery(wantedTags, unwantedTags)
		if not res: # no tags selected at all
			res = EventList._getDefaultRestrictionQuery(sponsor)
		return res

	@classmethod
	def fromSelectors(cls, conn, startDate=None, endDate=None,
			wantedTags=None, unwantedTags=None, maxFirstDay=None, evClass=Event,
			sponsor=None):
		"""returns an EventList matching certain criteria:

		* startDate is a datetime.date(time)
		* endDate is None or a datetime.date(time)
		* maxFirstDay is None or a datetime.date, giving a maximum first
		  day; this selects multiday events.
		* wantedTags is a list of tags events must have to be included
		* unwantedTags is a list of tags that causes events to be excluded
		"""
		cond, vals = cls._getDateCondition(startDate, endDate, maxFirstDay)
		tableQueried, tableVals = cls._getTableQueried(
			wantedTags, unwantedTags, sponsor)
		if cond:
			cond = "WHERE "+cond
		return cls.fromQuery(conn,
			"SELECT * FROM %s %s"%(tableQueried, cond),
			tableVals+vals, evClass=evClass)

	@classmethod
	def multidayForMonth(cls, conn, year, month,
			wantedTags=None, unwantedTags=None,
			evClass=Event, sponsor=None):
		"""returns an EventList of multiday events overlapping month/year.
		"""
		tableQueried, tableVals = cls._getTableQueried(
			wantedTags, unwantedTags, sponsor)
		cond = "WHERE fdate IS NOT NULL AND (fdate<=? AND bdate>=?)"
		firstOfMonth = datetime.date(year, month, 1)
		lastOfMonth = datetime.date(year, month,
			calendar.monthrange(year, month)[1])
		return cls.fromQuery(conn,
			"SELECT * FROM %s %s"%(tableQueried, cond),
			tableVals+[lastOfMonth, firstOfMonth],
			evClass=evClass)

	@classmethod
	def fromQueryDict(cls, conn, args, sponsor=None, multi=False):
		"""returns an event list from a dictionary representing a query string.

		args maps keys to lists of arguments, as in cgi.FieldStorage or
		nevow request.args.  Valid keys include:

		* startDate -- first used, ISO format, default now
		* nDays -- endDate is startDate+nDays, default defaultTimeDelta
		* endDate -- first used, ISO format, default see nDays
		* tag -- all used, tags required
		* notag -- all used, tags forbidden
		"""
		startDate = base.getfirst(args, "startDate", None)
		if startDate:
			startDate = base.parseISODate(startDate).date()
		else:
			startDate = base.now().date()

		endDate = base.getfirst(args, "endDate", None)
		if endDate:
			endDate = base.parseISODate(endDate).date()
		else:
			nDays = base.parseInt(base.getfirst(args, "nDays", -1))
			if nDays<0:
				endDate = startDate+conn.stconf.defaultTimeDelta
			elif nDays>0:
				endDate = startDate+datetime.timedelta(days=nDays)
			else:
				endDate = None

		fDate = None
		if multi:
			fDate = endDate

		return cls.fromSelectors(conn, startDate, endDate,
			args.get("tag", []), args.get("notag", []), sponsor=sponsor,
			maxFirstDay=fDate)

	def iterGrouped(self):
		"""iterates over the dates given in my list with the events on that dates.

		What's actually returned are pairs of the datetime and a list of
		events belonging to it.
		"""
		if not self.evList:
			return
		curDate, evsForDate = self.evList[0].bdate, []
		for ev in self.evList:
			if ev.bdate==curDate:
				evsForDate.append(ev)
			else:
				yield curDate, evsForDate
				curDate, evsForDate = ev.bdate, [ev]
		if evsForDate:
			yield curDate, evsForDate


def weed(conn):
	"""deletes all invalid events.

	invalid events have no bdate or no title.
	"""
	conn.execute("DELETE FROM events WHERE bdate IS NULL OR teaser IS NULL")
	conn.commit()
	conn.execute("VACUUM")


def archive(conn):
	"""moves all events that were due before today to the archive.
	"""
	archiveConn = conn.getArchiveConnection()
	yesterday = base.now().date()+datetime.timedelta(days=-1)
	toArchive = EventList.fromQuery(conn,
		"SELECT * FROM events WHERE bdate<=?",
		(yesterday,), evClass=TaggedEvent)
	for ev in toArchive:
		ev.save(archiveConn)
		ev.delete(conn)
	archiveConn.commit()
	conn.commit()
