"""
Update management for the databases.

The basic idea is that changes to the data model (jointly defined
by model and dbinter) are defined in terms of Updater objects.

If dbinter.getConnection notices a difference between the model version
running and the model version in the db, it will update the db by locating
all necessary updates and applying them to the tables in the dbs.

Currently, updates are run both against the current DB and the archive DB.
If we ever need updates to only one of the tables, I guess another
attribute on Updater would be the way to go.
"""


class Updater(object):
	"""A specification to update from some model version to another model 
	version.

	Updaters live as naked classes.  Their version attribute gives the version
	their instructions update *to*.

	Instructions are (usually) given in u_nnn_name attributes like for dbinter
	TableDefs, but you could also override the iterStatements class method.

	The update method automatically VACUUMs the DB after an update has run.
	"""
	version = None

	@classmethod
	def iterStatements(cls):
		for cmdAttr in (s for s in sorted(dir(cls)) if s.startswith("u_")):
			yield getattr(cls, cmdAttr)
		

class UpdateTo2(Updater):
	version = 2
	u_000_addAttachColumn = ("ALTER TABLE events ADD COLUMN"
		" attachments INTEGER DEFAULT 0")


class UpdateTo3(Updater):
	version = 3
	u_000_addAttachColumn = ("ALTER TABLE events ADD COLUMN"
		" fdate DATE")


class UpdateTo4(Updater):
	version = 4
	u_000_addPrivateColumn = ("ALTER TABLE events ADD COLUMN"
		" private BOOL DEFAULT 0")


class UpdateTo5(Updater):
	version = 5
	u_000_addChangedColumn = ("ALTER TABLE events ADD COLUMN"
		" lastChanged REAL")
	u_010_addChangerColumn = ("ALTER TABLE events ADD COLUMN"
		" lastChanger TEXT")


def iterUpdaters():
	for ob in list(globals().values()):
		if (isinstance(ob, type) 
				and issubclass(ob, Updater) 
				and ob is not Updater):
			yield ob


def getUpdateStatements(srcVersion, destVersion, updaters=None):
	if updaters is None:
		updaters = iterUpdaters()
	toRun = []
	for updater in updaters:
		if srcVersion<updater.version<=destVersion:
			toRun.append(updater)
	toRun.sort(key=lambda updater:updater.version)
	for updater in toRun:
		for statement in updater.iterStatements():
			yield statement


def update(conn, srcVersion, destVersion):
	"""executes all updates known to bring the DB behind conn from srcVersion
	to destVersion.

	The config table within the DB will be updated to reflect the db's
	modelVersion, and the database will be VACUUMed.
	"""
	curs = conn.cursor()
	for statement in getUpdateStatements(srcVersion, destVersion):
		curs.execute(statement)
	curs.execute("DELETE FROM config WHERE key=?", ('modelVersion',))
	curs.execute("INSERT INTO config (key, value) VALUES (?,?)", 
		('modelVersion', destVersion))
	curs.execute("VACUUM")
	curs.close()

