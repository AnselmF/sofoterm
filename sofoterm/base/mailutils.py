"""
Simple templating and utilities for creating and sending mail.
"""

import re
import subprocess
import sys
import time
from email import charset
from email import utils as emailutils
from email.parser import Parser
from email.mime.nonmultipart import MIMENonMultipart

from sofoterm.base import common


class SendNoMail(Exception):
	"""can be raised by a mail template if no mail should be sent.
	"""


class Template(object):
	"""a *very* basic and ad-hoc template engine for plain text.

	Where this is formatted from a file, the filse must 
	be encoded in utf-8 encoded. The first
	paragraph (concluded by two consecutive LFs) is executed as
	python code to generate more local variables.  The rest is
	interpreted as follows:

	* ${key} -- value for key, escaped for double-quoted att values
	* $!expr! -- replace with the result of the python expr (there is no 
		way to include bangs in that code right now)
	* $$ -- a $ char.

	key is taken from the locals passed to render, expr can use
	names in both locals and globals.
	
	The template can be constructed with a literal unicode string or
	using the fromFile constructor that accepts either a file name or
	a file object.
	"""
	def __init__(self, source):
		self.source = source
	
	@classmethod
	def fromFile(cls, srcFile):
		if isinstance(srcFile, str):
			srcFile = open(srcFile, encoding="utf-8")
			finalize = lambda: srcFile.close()
		else:
			finalize = lambda: False

		try:
			return cls(srcFile.read())
		finally:
			finalize()
	
	def render(self, locals, globals):
		"""returns a string with the template filled using vars.

		vars is a dictionary mapping keys to unicode-able objects.
		"""
		code, template = self.source.split("\n\n", 1)
		exec(code, locals, globals)
		return 	re.sub(r"\$\{([a-zA-Z0-9_]+)\}", 
				lambda mat: locals.get(mat.group(1), ""),
			re.sub(r"\$!([^!]*)!", 
				lambda mat: eval(mat.group(1), locals, globals),
			template)).replace(
			"$$", "$")


def formatMail(templateSource, locals, globals):
	"""returns a mail with headers and content properly formatted as
	a bytestring and MIME.
	"""
	locals["Date"] = emailutils.formatdate(time.time(), 
		localtime=False, usegmt=True)

	try:
		mailText = Template.fromFile(templateSource).render(locals, globals)
	except SendNoMail:
		sys.stderr.write("No mail sent on template request.\n")
		sys.exit(0)

	rawHeaders, rawBody = mailText.split("\n\n", 1)

	cs = charset.Charset("utf-8")
	cs.body_encoding = charset.QP
	cs.header_encoding = charset.QP
	msg = MIMENonMultipart("text", "plain", charset="utf-8")
	msg.set_payload(rawBody, charset=cs)

	for key, value in Parser().parsestr(rawHeaders.strip()).items():
		msg[key] = value

	return msg.as_string()


def sendMail(templateSource, locals, globals, destFile=None):
	"""formats templateSource with var and hands the result over to
	sendmail.

	templateSource is a file name or an opened file.

	globals needs to be a dictionary containing all "important" items
	necessary in the templates (base, model,...)

	locals can be an empty dictionary; some keys are added in place, so
	don't pass anything that can break.

	Instead of piping the formatted mail to sendmail, the function
	will write it to destFile (binary mode!) if you pass that.

	Keys added here include:

	  - rfcdate -- the current date and time in RFC xy format.
	"""
	locals["rfcdate"] = emailutils.formatdate()

	mailBytes = formatMail(templateSource, locals, globals
		).encode("ascii", "ignore")
	if destFile is None:
		pipe = subprocess.Popen(["/usr/sbin/sendmail", "-oi", "-t"],
			stdin=subprocess.PIPE)
		pipe.stdin.write(mailBytes)
		pipe.stdin.close()

		if pipe.wait():
			common.ReportableError("Sendmail failed.  Mail not sent.")
	
	else:
		destFile.write(mailBytes)
