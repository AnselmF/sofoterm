"""
Support code for sofoterm event management
"""

import datetime
import inspect
import os
import time

import pkg_resources


debug = False


class Error(Exception):
	"""Base class of our errors.
	"""


class InternalError(Exception):
	"""These errors should not be seen by users (we won't I18N them, too).
	"""


class ReportableError(Error):
	"""Such errors' messages should and can be passed on the user.
	"""


class InvalidEvent(Error):
	"""raised as a kind of ValueError for event objects.
	"""


class EventNotFound(Error):
	def __init__(self, id):
		self.id = id
	
	def __str__(self):
		return "Event %s not found"%self.id


class BadArgument(Error):
	"""is raised when bad input comes from the user.
	"""
	def __init__(self, type, literal):
		self.type, self.literal = type, literal
	
	def __str__(self):
		return "Bad %s: '%s'"%(self.type, self.literal)


class WebException(Exception):
	"""The base class for all exceptions mapping to HTTP codes.

	All these are constructed with a message; the error renderers allow
	this to be either a string or stan.
	"""
	def __init__(self, msg):
		self.msg = msg


class PageNotFound(WebException): pass

class Redirect(WebException): pass

class Forbidden(WebException): pass

class NotModified(WebException): pass


class _UndefinedClass(object):
	"""class of the undefined sentinel (a singleton) below.
	"""
	def __str__(self):
		raise ValueError("%s cannot be stringified"%self.__class__.__name__)

	__unicode__ = __str__

	def __repr__(self):
		return "common.Undefined"

	def __bool__(self):
		return False

Undefined = _UndefinedClass()


def getAppPath():
	"""returns the toplevel directory for our application file.

	Thats the content of SOFODB or ~/sofoterm.
	"""
	return os.environ.get("SOFOFILES", 
		os.path.join(os.environ.get("HOME", "/"), "sofoterm"))


def getDSN():
	"""returns the connection string for the default database.
	"""
	return os.path.join(getAppPath(), "events")


def getLoggingDir():
	"""returns (and creates if necessary) a directory for logging.
	"""
	logPath = os.path.join(getAppPath(), "logs")
	if not os.path.isdir(logPath):
		os.makedirs(logPath)
	return logPath


def getResource(resPath):
	"""returns a named "resource". 
	
	Only use this function to access built-in resources, since this will
	later allow users to override them.
	"""
	return pkg_resources.resource_filename('sofoterm', resPath)


def getfirst(args, key, default=Undefined):
	"""like cgi's getfirst for dicts of lists (like nevow args).

	If you pass a default, it will be returned on missing keys, otherwise
	you get a key error.

	>>> getfirst({"a": [2, None, 3], "b": []}, "a")
	2
	>>> getfirst({"a": [2, None, 3], "b": []}, "c", "jaja")
	'jaja'
	>>> getfirst({"a": [2, None, 3], "b": []}, "c")
	Traceback (most recent call last):
	KeyError: 'c'
	"""
	try:
		return args[key][0]
	except IndexError:
		None
	except KeyError:
		if default is Undefined:
			raise
		else:
			return default


def now(utc=False):
	"""returns the current datetime.

	In debug mode, this function is overwritten, so don't call 
	datetime.now yourself.
	"""
	if utc:
		return datetime.datetime.utcnow()
	else:
		return datetime.datetime.now()


def dateHence(nDays, utc=False):
	"""returns a datetime.date() for tomorrow.
	"""
	return now(utc).date()+datetime.timedelta(days=nDays)


def isSecure():
	"""returns True if it thinks we're handling an https request,
	false otherwise.

	This works by looking for a request object upstack.  If we
	find that, we believe we're handling https if isSecure is true
	or if there is an x-original-port header valued 443 (this
	needs cooperation from a reverse proxy).

	If we don't find out anything, we return false.
	"""
	frame = inspect.currentframe().f_back.f_back
	while frame:
		if "request" in frame.f_locals :
			request = frame.f_locals["request"]
			if hasattr(request, "isSecure"):
				break
		frame = frame.f_back
	else:
		return False

	if (request.isSecure()
		or request.requestHeaders.getRawHeaders("x-original-port", [None])[0]
			=="443"):
		return True
	
	return False


def completeURL(path, conn):
	"""returns a URL for an internal URL path.

	path must start with a slash.  We accept strings and bytes but always
	return strings.
	"""
	path = debytify(path)
	if path.startswith("http://") or path.startswith("https://"):
		return path
	if conn.stconf.port and conn.stconf.port!=80:
		port = ":%s"%conn.stconf.port
	else:
		port = ""
	
	if isSecure():
		scheme = "https"
	else:
		scheme = "http"

	if not path.startswith("/"):
		path = "/"+path

	return "%s://%s%s%s"%(
		scheme, conn.stconf.hostName, port, path)


def debytify(b):
	if isinstance(b, bytes):
		return b.decode("utf-8")
	return b


def debytifyList(l):
	return [debytify(b) for b in l]


def debytifyArgs(args):
	return dict((debytify(k), debytifyList(l)) for k, l in args.items())


def bytify(s):
	"""utf-8 encodes s if it's a string.

	>>> bytify(b"abc")
	b'abc'
	>>> bytify(23)
	23
	>>> bytify("Krämer")
	b'Kr\xc3\xa4mer'
	"""
	if isinstance(s, str):
		return s.encode("utf-8")
	return s


# We may one day want to have proper gettext support.  Meanwhile, don't
# bother
def _(s):
	return s


weekdays = [
	"Montag",
	"Dienstag",
	"Mittwoch",
	"Donnerstag",
	"Freitag",
	"Samstag",
	"Sonntag",]


def setupDebug(debugDate=datetime.datetime(2010, 2, 14, 14, 5, 0)):
	"""sets up a debug environment.  Really only used for testing.
	"""
	global now
	global debug

	_nowLocal = debugDate
	_nowUTC = datetime.datetime.fromtimestamp(
		time.mktime(debugDate.timetuple()), tz=datetime.timezone.utc)
	def _now(utc=False):
		if utc:
			return _nowUTC
		else:
			return _nowLocal
	now = _now
	# also overwrite base.now (nasty, but IMHO nicer than the alternatives)
	from sofoterm import base
	base.now = _now
	debug = True


if __name__=="__main__": # pragma: no cover
	import doctest
	doctest.testmod()
