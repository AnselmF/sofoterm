"""
Autonodes are classes with (somewhat) managed attributes.

This stuff is used here for modelling the events.
"""

from sofoterm.base import common #noflake: For registration

class AutoNodeType(type):
	"""A metaclass for AutoNodes..

	The idea here is to define children in a class definition and make sure they
	are actually present.
	
	AutoNodes are supposed to be immutable; the are defined during construction.
	Currently, nothing keeps you from changing them afterwards, but that may
	change.
	
	The classes' constructor is defined to accept all attributes as arguments
	(you probably want to use keyword arguments here).  It is the constructor
	that sets up the attributes, so AutoNodes must not have an __init__ method.
	However, they may define a method _setupNode that is called just before the
	artificial constructor returns.
	
	To define the attributes of the class, add _a_<attname> attributes
	giving a default to the class.  The default should normally be either
	None for 1:1 or 1:0 relations or an empty tuple for 1:n relations.
	The defaults must return a repr that constructs them, since we create
	a source fragment.
	"""
	def __init__(cls, name, bases, dict):
		cls._collectAttributes()
		cls._buildConstructor()
	
	def _collectAttributes(cls):
		cls._nodeAttrs = []
		for name in dir(cls):
			if name.startswith("_a_"):
				cls._nodeAttrs.append((name[3:], getattr(cls, name)))
				if isinstance(getattr(cls, name), property):
					setattr(cls, name[3:], getattr(cls, name))
	
	def _buildConstructor(cls):
		argList, codeLines = ["self"], []
		for argName, argDefault in cls._nodeAttrs:
			if isinstance(argDefault, property):
				argList.append("%s=None"%(argName))
			else:
				argList.append("%s=%s"%(argName, repr(argDefault)))
			codeLines.append("  self.%s = %s"%(argName, argName))
		codeLines.append("  self._setupNode()\n")
		codeLines.insert(0, "def constructor(%s):"%(", ".join(argList)))
		ns = globals()
		exec("\n".join(codeLines), ns)
		cls.__init__ = ns["constructor"]


class AutoNode(object, metaclass=AutoNodeType):
	"""An object with a certain amount of attribute management..

	AutoNodes are explained in AutoNode's metaclass, AutoNodeType.

	A noteworthy method is change -- pass in new attribute values 
	to create a new instance with the original attribute values except
	for those passed to change.  This will only work if all non-autoattribute
	attributes of the class are set in _setupNode.
	"""

	def _setupNodeNext(self, cls):
		try:
			pc = super(cls, self)._setupNode
		except AttributeError:
			pass
		else:
			pc()

	def _setupNode(self):
		self._setupNodeNext(AutoNode)

	def __repr__(self):
		return "<%s %s>"%(self.__class__.__name__, " ".join(
			"%s=%s"%(name, repr(val))
			for name, val in self.iterAttributes(skipEmpty=True)))

	def change(self, **kwargs):
		"""returns a shallow copy of self with constructor arguments in kwargs
		changed.
		"""
		if not kwargs:
			return self
		consArgs = dict(self.iterAttributes())
		consArgs.update(kwargs)
		return self.__class__(**consArgs)

	@classmethod
	def cloneFrom(cls, other, **kwargs):
		"""returns a shallow clone of other.

		other should be of the same class or a superclass.
		"""
		consArgs = dict(other.iterAttributes())
		consArgs.update(kwargs)
		return cls(**consArgs)

	def iterAttributes(self, skipEmpty=False):
		"""yields pairs of attributeName, attributeValue for this node.
		"""
		for name, _ in self._nodeAttrs:
			val = getattr(self, name)
			if skipEmpty and not val:
				continue
			yield name, val

	def __eq__(self, other):
		if not isinstance(other, self.__class__):
			return False
		if self is other:
			return True
		for name, _ in self._nodeAttrs:
			if getattr(self, name)!=getattr(other, name):
				return False
		return True
	
	def __ne__(self, other):
		return not self==other


