"""
Misc functions not really fitting anywhere.
"""

import datetime
import gc
import os
import pickle
import re
import shutil
import subprocess
import sys
import time
import traceback
import urllib.parse
from io import StringIO
from email import utils as emailutils

try:
	import coverage
except ImportError:  # pragma: no cover
	# we only need this if someone set COVERAGE_FILE; don't do that
	# if you don't have coverage.py installed.
	pass

from docutils import core as restlib

from sofoterm.base import common

class RSTError(Exception):
	pass


_severetyPat = re.compile(r"\([A-Z]+/\d\) ")

def _cleanupRSTMessage(msg):
	return _severetyPat.sub("", msg.replace("<string>:", "Line "
		).strip().replace("\n", " / "))


def compileRSTX(rstx):
	"""returns restructed text as literal HTML.
	"""
	sourcePath, destinationPath = None, None
	overrides = {
		'file_insertion_enabled': False,
		'raw_enabled': False,
		'doctitle_xform': None,
		'initial_header_level': 4,
		'warning_stream': StringIO()}

	try:
		parts = restlib.publish_parts(
			source=rstx+"\n", source_path=sourcePath,
			destination_path=destinationPath,
			writer_name='html', settings_overrides=overrides)
	except restlib.utils.SystemMessage as ex:
		raise RSTError(_cleanupRSTMessage(str(ex)))
	warnings = overrides["warning_stream"].getvalue()
	if warnings:
		raise RSTError(_cleanupRSTMessage(warnings))
	return parts["fragment"]


_isoDTRE = re.compile(r"(?P<year>\d\d\d\d)-?(?P<month>\d\d)-?(?P<day>\d\d)"
		r"(?:T(?P<hour>\d\d):?(?P<minute>\d\d):?"
		r"(?P<seconds>\d\d)(?P<secFracs>\.\d*)?Z?)?$")

def parseISODate(literal):
	"""returns a datetime object for a ISO time literal.

	There's no timezone support yet.

	>>> parseISODate("1998-12-14")
	datetime.datetime(1998, 12, 14, 0, 0)
	>>> parseISODate("1998-12-14T13:30:12")
	datetime.datetime(1998, 12, 14, 13, 30, 12)
	>>> parseISODate("1998-12-14T13:30:12Z")
	datetime.datetime(1998, 12, 14, 13, 30, 12)
	>>> parseISODate("1998-12-14T13:30:12.224Z")
	datetime.datetime(1998, 12, 14, 13, 30, 12, 224000)
	>>> parseISODate("19981214T133012Z")
	datetime.datetime(1998, 12, 14, 13, 30, 12)
	>>> parseISODate("junk")
	Traceback (most recent call last):
	sofoterm.base.common.BadArgument: Bad ISO date/time: 'junk'
	>>> parseISODate(None)
	"""
	if literal is None:
		return None
	mat = _isoDTRE.match(literal.strip())
	if not mat:
		raise common.BadArgument("ISO date/time", literal)
	parts = mat.groupdict()
	if parts["hour"] is None:
		parts["hour"] = parts["minute"] = parts["seconds"] = 0
	if parts["secFracs"] is None:
		parts["secFracs"] = 0
	else:
		parts["secFracs"] = "0"+parts["secFracs"]
	return datetime.datetime(int(parts["year"]), int(parts["month"]),
		int(parts["day"]), int(parts["hour"]), int(parts["minute"]), 
		int(parts["seconds"]), int(float(parts["secFracs"])*1000000))


def parseInt(literal):
	"""returns literal as int, turning ValueErrors into BadArgument exceptions.

	>>> parseInt("23")
	23
	>>> parseInt("3n")
	Traceback (most recent call last):
	sofoterm.base.common.BadArgument: Bad integer: '3n'
	"""
	try:
		return int(literal)
	except ValueError:
		raise common.BadArgument("integer", literal)


def backup(fqName):
	"""creates a backup of the file pointed to by fqName.

	The backup name is created by appending numbers to fqName until a
	non-existing name results.  This will race if more than one process
	to backups at the same time.

	Backing up non-existing files is a no-op.
	"""
	try:
		os.stat(fqName)
	except FileNotFoundError:
		# File doesn't exist, and hence we don't make a backup.
		# Don't use os.path.exists here because that won't notice
		# lacking privileges.
		return
	
	count = 0
	for i in range(1000):
		backupName = "%s.%03d"%(fqName, count)
		if not os.path.exists(backupName):
			break
		count += 1
	else:
		raise common.ReportableError("Trouble: Cannot backup %s. Permission"
			" problem?"%fqName)
	shutil.copy(fqName, backupName)


def parseHTTPDate(s):
	"""returns seconds since unix epoch representing UTC from the HTTP-compatible
	time specification s.
	"""
	parts = emailutils.parsedate_tz(s)
	return emailutils.mktime_tz(parts)


def formatHTTPDate(secs=None):
	"""returns an HTTP date string for seconds since unix epoch.
	"""
	if secs is None:
		secs = time.time()
	return emailutils.formatdate(secs, localtime=False, usegmt=True)


# that's test instrumentation (no idea why coverage.py doesn't pick it up)
class ForkingSubprocess(subprocess.Popen): # pragma: no cover
	"""A subprocess that doesn't exec but fork.
	"""
	def _execute_child(self, args, executable, preexec_fn, close_fds,
							 pass_fds, cwd, env,
							 startupinfo, creationflags, shell,
							 p2cread, p2cwrite,
							 c2pread, c2pwrite,
							 errread, errwrite,
							 restore_signals, 
               # post-3.7, some additional parameters were added, which
               # fortunately we don't need.
							 *ignored_args):
# stolen largely from 2.7 subprocess.  Unfortunately, I can't just override the
# exec action; so, I'm replacing the the whole exec shebang with a simple
# call to executable().
#
# Also, I'm doing some extra hack to collect coverage info if it looks
# like we're collecting coverage.
#
# The signature (and a few inside parts) is from 3.7 subprocess.  We're
# ignoring everything that 2.7 hasn't known under the bold assumption that our
# simple testing forks that doesn't matter (which ought to be true for
# start_new_session at least:-).

		sys.argv = args
		if executable is None:
				executable = args[0]

		def _close_in_parent(fd):
				os.close(fd)

		# For transferring possible exec failure from child to parent.
		# Data format: "exception name:hex errno:description"
		# Pickle is not used; it is complex and involves memory allocation.
		errpipe_read, errpipe_write = os.pipe()

		try:
						gc_was_enabled = gc.isenabled()
						# Disable gc to avoid bug where gc -> file_dealloc ->
						# write to stderr -> hang.  http://bugs.python.org/issue1336
						gc.disable()
						try:
								self.pid = os.fork()
						except:
								if gc_was_enabled:
										gc.enable()
								raise
						self._child_created = True
						if self.pid == 0:
								# Child
								try:
										# Close parent's pipe ends
										if p2cwrite!=-1:
												os.close(p2cwrite)
										if c2pread!=-1:
												os.close(c2pread)
										if errread!=-1:
												os.close(errread)
										os.close(errpipe_read)

										# When duping fds, if there arises a situation
										# where one of the fds is either 0, 1 or 2, it
										# is possible that it is overwritten (#12607).
										if c2pwrite == 0:
												c2pwrite = os.dup(c2pwrite)
										if errwrite == 0 or errwrite == 1:
												errwrite = os.dup(errwrite)

										# Dup fds for child
										def _dup2(a, b):
												# dup2() removes the CLOEXEC flag but
												# we must do it ourselves if dup2()
												# would be a no-op (issue #10806).
												if a == b:
														self._set_cloexec_flag(a, False)
												elif a!=-1:
														os.dup2(a, b)
										_dup2(p2cread, 0)
										_dup2(c2pwrite, 1)
										_dup2(errwrite, 2)

										# Close pipe fds.  Make sure we don't close the
										# same fd more than once, or standard fds.
										closed = set([-1])
										for fd in [p2cread, c2pwrite, errwrite]:
												if fd not in closed and fd > 2:
														os.close(fd)
														closed.add(fd)

										if cwd is not None:
												os.chdir(cwd)

										if "COVERAGE_FILE" in os.environ:
											cov = coverage.Coverage(data_file="forked.cov",
												source=["gavo"],
												auto_data=True)
											cov.config.disable_warnings = ["module-not-measured"]
											cov.start()

										if preexec_fn:
												preexec_fn()

										exitcode = 0

								except:
										exc_type, exc_value, tb = sys.exc_info()
										# Save the traceback and attach it to the exception object
										exc_lines = traceback.format_exception(exc_type,
																													 exc_value,
																													 tb)
										exc_value.child_traceback = ''.join(exc_lines)
										os.write(errpipe_write, pickle.dumps(exc_value))
										os.close(errpipe_write)
										os._exit(255)

								os.close(errpipe_write)
								try:
									executable()
								except SystemExit as ex:
									exitcode = ex.code

								if "COVERAGE_FILE" in os.environ:
									cov.stop()
									cov.save()
								sys.stderr.close()
								sys.stdout.close()
								os._exit(exitcode)


						# Parent
						os.close(errpipe_write)
						if gc_was_enabled:
								gc.enable()

						# Now wait for the child to come up (at which point it will
						# close its error pipe)
						errpipe_data = bytearray()
						while True:
								part = os.read(errpipe_read, 50000)
								errpipe_data += part
								if not part or len(errpipe_data)>50000:
									break
								if errpipe_data:
									child_exception = pickle.loads(errpipe_data)
									raise child_exception

		finally:
				# close the FDs used by the child if they were opened
				if p2cread!=-1 and p2cwrite!=-1:
						_close_in_parent(p2cread)
				if c2pwrite!=-1 and c2pread!=-1:
						_close_in_parent(c2pwrite)
				if errwrite!=-1 and errread!=-1:
						_close_in_parent(errwrite)

				# be sure the error pipe FD is eventually no matter what
				os.close(errpipe_read)


# that's test instrumentation (no idea why coverage.py doesn't pick it up)
def forkToCall(callable, *args, **kwargs): # pragma: no cover
	def func():
		return callable(*args, **kwargs)
	ForkingSubprocess(["sofoterm server"], executable=func)


def urlencodeArgs(args):
	"""returns nevow-style args urlencoded.

	We debytify any byte strings in the process, and what's returned is
	a native string, too.

	>>> urlencodeArgs({b"a": ["b", b"c"], "d": []})
	'a=b&a=c'
	"""
	tuples = []
	for key, values in common.debytifyArgs(args).items():
		for v in values:
			if v:
				tuples.append((key, v))
	return urllib.parse.urlencode(tuples)


if __name__=="__main__": # pragma: no cover
	import doctest
	doctest.testmod()
