"""
A simple configuration system.

Basically, we store key-value pairs in the database table config.
This interface builds a little shell around this.

In pages, the configuration rides in the stconf attribute of
the conn attribute.
"""

import datetime
import os
import warnings

from sofoterm.base import common

def p_stringSet(conn, name):
	"""makes a parser for string sets.

	Baroque feature: We strip a trailing char from name.  This is
	supposed to turn the plural in the attribute to the singular
	in the key.  Ha!
	"""
	return set(r[0] for r in conn.execute(
		"SELECT value FROM config WHERE key=?", (name[:-1],)).fetchall())

def p_undefined(conn, name):
	raise common.Error("No such predefined config: %s"%name)

def make_p_singleValue(typeFactory, default):
	"""returns a Config parse function reading producing values
	through typeFactory and entering default if nothing is available.
	"""
	def parser(conn, name):
		res = conn.execute(
			"SELECT value FROM config WHERE key=?", (name,)).fetchall()
		if res:
			res = typeFactory(res[0][0])
		else:
			if default is common.Undefined:
				raise AttributeError(name)
			res = default
		return res
	return parser

def make_p_string(default):
	return make_p_singleValue(str, default)

def make_p_int(default):
	return make_p_singleValue(int, default)


class HookMixin(object):
	"""A mixin letting an object dispatch events.

	Clients can subscribe to events and post them.  With events, you can
	pass keyword arguments.  All hooks always receive the object mixing
	this in as the first and only positional argument.

	See Hacking docs for hooks used by the built-in machinery.  This is
	mixed in by Connection, so that's the object passed as the first argument.

	Objects mixing this in must call initHooks() in their constructor.
	"""
	def initHooks(self):
		self.hooks = {}
	
	def subscribe(self, hookName, hookHandler):
		"""adds hookHandler to the sequence of functions called for hookName.

		Multiple subscriptions are legal, but each handler will only
		be called once.
		"""
		destList = self.hooks.setdefault(hookName, [])
		if hookHandler not in destList:
			destList.append(hookHandler)

	def unsubscribe(self, hookName, hookHandler):
		"""remove hookHandler from the sequence of functions called for hookName.

		It is not an error to remove a non-existing hookHandler.
		"""
		destList = self.hooks.get(hookName, [])
		try:
			destList.remove(hookHandler)
		except ValueError:
			pass
		
	def post(self, hookName, **kwargs):
		for handler in self.hooks.get(hookName, []):
			try:
				handler(self, **kwargs)
			except:
				warnings.warn("Exception while delivering %s to %s:"%(
					hookName, handler))
				#traceback.print_exc()


class Config(object):
	"""the central configuration class for sofoterm.

	The idea is that the stuff is in the config table in the database
	and is manipulated there either directly (at your own risk) or
	through the lavish methods of this class.

	Configs are constructed with a connection argument.

	There are predefined config in the class' attributes attribute.
	It maps attribute names to tuples of (currently) a parser function,
	a short description what's configured, and a boolean saying if the
	value is cached; the parser functions are defined above.

	Most predefined attributes are cached.  Caches are cleared when
	the DB is manipulated through the config object but not otherwise,
	so you'd need to call clearCaches if you bypass this object or
	anticipate external changes.

	Write and delete predefined keys through the generic methods below.
	"""
	attributes = {
		"tags": (p_stringSet, "Tags available in forms", True),

		"sponsors": (p_stringSet, "Cookies giving write privileges", True),

		"startYear": (make_p_int(2001), "Earliest year for which to produce"
			" archive pages.", True),

		"defaultTimeDelta": (make_p_singleValue(
				lambda val: datetime.timedelta(days=int(val)),
				datetime.timedelta(days=22)),
			"Days the default date lists look into the future.", True),

		"hostName": (make_p_string("localhost"), 
			"Name this host prefers to be talked to"
			" (this is important with reverse proxies)", True),

		"port": (make_p_int(8888), "Port shown for external queries"
			" (this is important with reverse proxies)", True),

		"bindport": (make_p_int(8888), "Port to bind to", True),
	
		"serverPID": (make_p_int(common.Undefined), 
			"Set by the server to its PID", False),

		"modelVersion": (make_p_int(common.Undefined),
			"Version of the model represented in the DB", True),

		"uploadLimit": (make_p_int(2**20), "Max. size of uploads in bytes",
			True),

		"mailmanAPI": (make_p_string(common.Undefined),
			"API URL of a mailman carrying a distribution list", True),
		"mailmanListname": (make_p_string(common.Undefined),
			"Name of the mailman distribution list;", 
			" That's generally the address of the list with the @ replaced by a .",
			 True),
		"mailmanAPIUser": (make_p_string(common.Undefined),
			"Username for the API management", True),
		"mailmanAPIPassword": (make_p_string(common.Undefined),
			"Password (in cleartext) for the API management (yeah, it's insane"
			" we need this just to subscribe, but that's modern IT security"
			" for you)", True),

		"dumpDir": (make_p_string(common.Undefined),
			"Directory to keep dumps in.", True),

		"lastDumpStamp": (make_p_string(common.Undefined),
			"UTC unix timestamp of last incremental dump", False),

		"user": (make_p_string(common.Undefined),
			"User the server runs as if started as root", True),

		"certPath": (make_p_string(common.Undefined),
			"Path to a PEM of concatenated private key and certificate; only"
				" needed if you want to serve https", True),

		"sslPort": (make_p_int(40443),
			"Port to listen for TLS requests; set this to 443 to serve"
				" 'normal' https.  This will be ignored unless privateKey is"
				" set (and valid)", True),
		}

	def __init__(self, conn, dsn):
		self.conn = conn
		self.cachedValues = {}
		self.archiveDSN = computeArchiveDSN(dsn)

	def clearCaches(self):
		self.cachedValues = {}

	_attDefault = (p_undefined, "Sentinel for undefined attributes", True)

	def __getattr__(self, name):
		# TODO: it's a bit stupid that there's no symmetric __setattr__.
		# Is there a good reason why it's not there?
		if name not in self.cachedValues:
			func, _, cacheThis = self.attributes.get(name, self._attDefault)
			if cacheThis:
				self.cachedValues[name] = func(self.conn, name)
			else:
				return func(self.conn, name)
		return self.cachedValues[name]

	def _countOp(self, sql, vals):
		cursor = self.conn.execute(sql, vals)
		return cursor.rowcount

	def setUniqueValue(self, key, value):
		"""sets key to value and commits, unless key, value already exist,
		in which case a ValueError is raised.

		The connection is committed as a side effect.
		"""
		self.conn.execute("BEGIN")
		try:
			if self.getValue(key, default=None) is not None:
				raise ValueError("%s is already set"%key)
			self.addKeyValue(key, value)
		finally:
			self.conn.commit()

	def rmKeyValue(self, key, value):
		"""removes key, value pair from the config DB table.
		"""
		ct = self._countOp("DELETE FROM config WHERE key=? and value=?",
			(key, value))
		if ct:
			self.clearCaches()
		return ct

	def rmKey(self, key):
		"""deletes all config items for key.
		"""
		ct = self._countOp("DELETE FROM config WHERE key=?", (key,))
		if ct:
			self.clearCaches()
		return ct

	def addKeyValue(self, key, value):
		"""adds a config item with key and value.
		"""
		self.clearCaches()
		return self._countOp("INSERT INTO config (key, value) VALUES (?,?)",
			(key, value))

	def setKeyValue(self, key, value):
		"""sets a config item with a key and value.

		This is an rmKey follwed addKeyValue.
		"""
		self.rmKey(key)
		self.addKeyValue(key, value)

	def getConfigs(self, key):
		"""returns a list of config items for key.
		"""
		cursor = self.conn.execute("SELECT value FROM config WHERE key=?", (key,))
		return [r[0] for r in cursor.fetchall()]
	
	def getValue(self, key, default=common.Undefined):
		"""returns a single config item.

		The function will raise a ValueError if more than one value is
		present, a KeyError if none is present unless you passed a default.
		"""
		cursor = self.conn.execute("SELECT value FROM config WHERE key=?", (key,))
		res = cursor.fetchall()
		if len(res)>1:
			raise ValueError("More than one value for config item %s"%key)
		elif not res:
			if default is common.Undefined:
				raise KeyError(key)
			return default
		return res[0][0]


def computeArchiveDSN(dsn):
	if dsn==":memory:":
		return ":memory:"
	else:
		return os.path.join(os.path.dirname(dsn),
			"archive")
