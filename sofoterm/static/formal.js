if(typeof(Forms) == 'undefined') {
    Forms = {};
}

if(typeof(Forms.Util) == 'undefined') {
    Forms.Util = {};
}

Forms.Util.previewShow = function(divId, frameId, u) {
    var div = document.getElementById(divId);
    var frame = document.getElementById(frameId);
    div.className = 'preview-show';
    frame.src = u;
    return false;
}

Forms.Util.previewHide = function(divId) {
    var div = document.getElementById(divId);
    div.className = 'preview-hidden';
    return false;
}


Forms.Util._basename = function(aString) {
	return aString.slice(0, aString.lastIndexOf("-"));
}

Forms.Util.addWord = function(words, word) {
// adds word to space-separated words if it's not in there already
	var items = words.split(" ");
	for (var i=0; i<items.length; i++) {
		if (items[i]==word) {
			return words;
		}
	}
	return words+" "+word;
}


Forms.Util.removeWord = function(words, word) {
	var items = words.split(" ");
	var newItems = [];

	for (var i=0; i<items.length; i++) {
		if (items[i]!=word) {
			newItems.push(items[i]);
		}
	}
	return newItems.join(" ");
}

Forms.Util.labelClick = function(label) {
// makes a checkbox change when the associated label is clicked
	var optName = Forms.Util._basename(label.attributes["id"].nodeValue);
	var box = document.getElementById(optName+"-box");
	box.checked = !box.checked;
	Forms.Util.updateLabel(box);
}

Forms.Util.updateLabel = function(box) {
// updates a label for a changed checkbox in a multichoice
	var optName = Forms.Util._basename(box.attributes["id"].nodeValue);
	var label = document.getElementById(optName+"-label");
	if (box.checked) {
		label.setAttribute("class", 
			Forms.Util.addWord(label.getAttribute("class"), "selected"));
	} else {
		label.setAttribute("class", 
			Forms.Util.removeWord(label.getAttribute("class"), "selected"));
	}
}
