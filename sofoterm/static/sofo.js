/* helper functions for sofoterm pages */

function walkChildren(root, visitor) {
/* traverses root's children. visitor is called for each child pre-order */
	for (childIndex in root.childNodes) {
		child = root.childNodes[childIndex];
		visitor(child);
		walkChildren(child, visitor);
	}
}

/* nuke checkboxes on tags if people have js activated (clicking on tags
is enough then) */
function nukeTagCheckboxes() {
	var checkboxParent = document.getElementById("termedit-tags-field");
	if (!checkboxParent) { // not an edit page
		return;
	}
	walkChildren(checkboxParent, function(node) {
		if (node.nodeName
				&& node.nodeName.toLowerCase()=="input" 
				&& node.getAttribute("class")=="multichoice") {
			node.style.display = 'none';
		}
	});
}

/* query a URL and replace this's data with the (200) result 
-- this goes together with the css styles toBeReplaced, isReplaced, notReplaced */
function replaceTextWithDoc(destObj, url) {
	destObj.onclick = "";
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("GET", url, true);
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState==4) {
			if (xmlHttp.status==200) {
				destObj.firstChild.data = xmlHttp.responseText;
				destObj.setAttribute("class", "isReplaced");
				destObj.onclick = null;
			} else {
				destObj.firstChild.data = 'Non 200-status: '+xmlHttp.status;
				destObj.setAttribute("class", "notReplaced");
			}
		}
	}
	xmlHttp.send(null);
}


function main() {
    nukeTagCheckboxes(true);
}

window.addEventListener("load", main);
