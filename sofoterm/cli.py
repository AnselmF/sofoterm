"""
Command line interface (stand-alone server and such)
"""

import datetime #noflake: used by mail templates
import os
import pwd
import random
import signal
import sys
import textwrap
import time
import warnings

from twisted.internet import reactor
from twisted.python import log
from twisted.web import server

from sofoterm import base
from sofoterm import model


############################# Specification of command line arguments

class Arg(object):
	def __init__(self, name, doc, validator=None):
		self.name, self.doc, self.validator = name, doc, validator

	def parse(self, rawArgs):
		if not rawArgs:
			sys.exit("Argument '%s' missing"%self.name)
		res = rawArgs.pop(0)
		if self.validator is not None:
			if not self.validator(res):
				sys.exit("Argument '%s': Invalid value '%s'"%(self.name, res))
		return res


class OptionalArg(Arg):
	def parse(self, rawArgs):
		if not rawArgs:
			return None
		else:
			return Arg.parse(self, rawArgs)


class RemainingArgs(Arg):
	def parse(self, rawArgs):
		res = rawArgs[:]
		rawArgs[:] = []
		if self.validator is not None:
			if not self.validator(res):
				sys.exit("Argument %s: Invalid value %s"%(self.name, res))
		return res


def arguments(*argDef):
	def deco(f):
		f.arguments = argDef
		return f
	return deco


def parseArgumentsFor(cmd, rawArgs):
	parsedArgs = {}
	for arg in cmd.arguments:
		parsedArgs[arg.name] = arg.parse(rawArgs)
	if rawArgs:
		sys.exit("Too many arguments: %s"%rawArgs)
	return parsedArgs


def makeArgdoc(cmd):
	if not cmd.arguments:
		return "Arguments:\n  None"
	return "Arguments:\n * %s"%("\n * ".join(
		"%s -- %s"%(arg.name, arg.doc) for arg in cmd.arguments))


############################# Actions

@arguments()
def cmd_start(conn, args):
	"""-- starts the server.
	"""
	try:
		runningPID = conn.stconf.serverPID
	except AttributeError:
		pass # no server's running, fine
	else:
		sys.exit("It seems there's already a server (pid %s) running."
			" Try %s stop."%(
				runningPID,
				sys.argv[0]))

	# Check if we need and can drop privileges as long as we've not yet
	# detached
	if os.getuid()==0 and not conn.stconf.getValue("user", None):
		sys.exit("If you're running sofoterm as root, you must set the user"
			" config variable first.")
	logFile = open(os.path.join(base.getLoggingDir(), "current.output"), "w")
	daemonize(logFile, lambda: runServer(conn, args))


@arguments()
def cmd_debug(conn, args):
	"""-- starts the server without detatching.
	"""
	try:
		from sofoterm.pages import root  #noflake: for registration
		runServer(conn, args, False)
	except ValueError:
		sys.exit("Server is already running with PID %d"%conn.stconf.serverPID)


def startLogging():
# XXX TODO: make this command line switchable.
#	theLog = logfile.LogFile("main", base.getLoggingDir())
	theLog = open("/dev/null")
	log.startLogging(theLog)
	def rotator():
		theLog.shouldRotate()
		reactor.callLater(86400, rotator)
	rotator()


def _dropPrivileges(conn):
	"""changes to whatever user is configured if we're running as root.

	If no user is configured, bail out (don't run as root, it's too
	tricky). If not running as root, don't do anything.
	"""
	if os.getuid(): # not root, do nothing
		return

	from twisted.python import log
	try:
		destUser = conn.stconf.getValue("user")
	except KeyError:
		log.err("Server started as root but no 'user'"
			" defined.  Refusing to run as root, bailing out.")
		sys.exit(1)
	try:
		uid = pwd.getpwnam(destUser)[2]
	except KeyError:
		log.err("Server user %s does not exist on this"
			" system.  Refusing to run as root, bailing out."%destUser)
		sys.exit(1)
	os.setuid(uid)	


def _perhapsEnableSSL(conn, factory):
	"""lets the reactor listen to TLS requests, too, if conn's configuration
	tells it to.

	If anything goes wrong here, we're just emitting a diagnostic and don't
	fail.
	"""
	certPath = conn.stconf.getValue("certPath", None)
	if not certPath:
		return
	try:
		from twisted.internet import ssl
		with open(certPath, "rb") as f:
			certificate = ssl.PrivateCertificate.loadPEM(f.read())
		sslPort = conn.stconf.getValue("sslPort", 40443)
		reactor.listenSSL(sslPort, factory, certificate.options())
	except Exception as msg:
		log.err("Not turning on TLS because: "+str(msg))


def runServer(conn, args, logToFile=True):
	"""actually runs the server.

	This is usually called from cmd_start.

	The function also installs a signal handler for SIGHUP that clears
	the cached config.
	"""
	from sofoterm.pages import PageFactory
	from sofoterm.formal import twistedpatch

	if logToFile:
		try:
			startLogging()
		except:  # don't die because of failed logging
			import traceback
			traceback.print_exc()
	else:
		log.startLogging(sys.stderr)

	try:
		signal.signal(signal.SIGHUP, lambda sig, stack: conn.post("reload"))

		factory = server.Site(PageFactory(conn))
		factory.requestFactory = twistedpatch.MPRequest
		factory.requestFactory.defaultContentType = "text/html;charset=utf-8"
		reactor.listenTCP(int(conn.stconf.getValue("serverPort", 8888)), factory)
		
		_perhapsEnableSSL(conn, factory)
		
		_dropPrivileges(conn)
		conn.stconf.setUniqueValue("serverPID", str(os.getpid()))
		conn.commit()
	except Exception as msg:
		log.err(str(msg))
		raise
	try:
		reactor.run()
	finally:
		conn.stconf.rmKey("serverPID")
		conn.commit()


def _waitForServerExit(conn, timeout=5):
	"""waits for server process to terminate.
	
	It does so by polling the database for the serverPID key and exiting
	if it is gone.  Because I'm lazy, the function exits with an error message
	if it doesn't vanish,
	"""
	for i in range(int(timeout*10)):
		try:
			pid = conn.stconf.serverPID
		except AttributeError:
			break
		time.sleep(0.1)
	else:
		sys.exit("The server with pid %d refuses to die.  Please try manually"%
			pid)


def _stopServer(conn):
	pid = conn.stconf.serverPID
	try:
		os.kill(pid, signal.SIGTERM)
	except os.error as ex:
		if ex.errno==3: # no such process
			conn.stconf.rmKey("serverPID")
			conn.commit()
			warnings.warn("Unclean server shutdown.  Removed serverPID.")
			return
		else:
			raise
	_waitForServerExit(conn)


@arguments()
def cmd_stop(conn, args):
	"""-- attempts to stop the server.
	"""
	try:
		_stopServer(conn)
	except AttributeError:
		sys.exit("Server doesn't seem to run, nothing killed.")
	except Exception as msg:
		sys.exit("Could not kill server (%s)."%msg)


@arguments()
def cmd_restart(conn, args):
	"""-- attempts to restart the server.
	"""
	try:
		_stopServer(conn)
	except AttributeError: # no server running yet, no worries
		pass
	cmd_start(conn, args)


@arguments()
def cmd_reload(conn, args):
	"""-- causes a running server to re-read its configuration from the DB.
	"""
	os.kill(conn.stconf.serverPID, signal.SIGHUP)


@arguments()
def cmd_help(conn, args):
	""" -- outputs help to stdout.
	"""
	_PARSER.print_help(file=sys.stdout)
	sys.stdout.write("\nCommands include:\n\n")
	for name in sorted(n for n in globals() if n.startswith("cmd_")):
		cmd = globals()[name]
		sys.stdout.write("%s %s\n"%(name[4:],
			cmd.__doc__.strip()))
		sys.stdout.write(makeArgdoc(cmd)+"\n\n")


def _doCountedOp(func, args):
	total = 0
	for arg in args:
		total += func(arg)
	print("%d item(s) changed"%total)


@arguments()
def cmd_ls(conn, args):
	""" -- lists all avaliable config keys.
	"""
	curs = conn.execute("SELECT DISTINCT key FROM config")
	sys.stdout.write(
		"\n".join(r[0] for r in curs.fetchall())+"\n")


@arguments(Arg("key", "a key name"),
	RemainingArgs("values", "values to set"))
def cmd_add(conn, args):
	""" -- adds to a config key.
	Arguments: a key and optionally as many values as you with.
	
	Use this for tag, sponsor, etc.
	"""
	key, values = args["key"], args["values"]
	_doCountedOp(lambda val: conn.stconf.addKeyValue(key, val), values)


@arguments(Arg("key", "a key name"),
	Arg("value", "the new value"))
def cmd_set(conn, args):
	""" -- sets a config key.

	*Never* use this for key, sponsor, etc.  This will remove any
	previous values for key.
	"""
	conn.stconf.rmKey(args["key"])
	conn.stconf.addKeyValue(args["key"], args["value"])


@arguments(Arg("key", "a key name"))
def cmd_show(conn, args):
	""" -- prints the contents of a config key.
	"""
	sys.stdout.write(
		"\n".join(sorted(conn.stconf.getConfigs(args["key"])))+"\n")


@arguments(Arg("key", "a key name"),
	RemainingArgs("values", "One or more values", lambda res: bool(res)))
def cmd_remove(conn, args):
	""" -- removes from a config key.
	"""
	key, values = args["key"], args["values"]
	_doCountedOp(lambda val: conn.stconf.rmKeyValue(key, val),
		values)


@arguments(Arg("key", "the key fo remove"))
def cmd_rmAll(conn, args):
	""" -- removes all values for a key.
	"""
	key = args["key"]
	print("%d item(s) deleted"%conn.stconf.rmKey(key))


@arguments(OptionalArg("template", "Template to use for the mail."),
	RemainingArgs("options",
		"Additional keyword=value pairs for the template"))
def cmd_mail(conn, args, overrideDest=None):
	""" -- sends out a mail formatted according to a template.
	"""
	# overrideDest is test instrumentation
	from sofoterm.base import mailutils
	if args["template"] is None:
		args["template"] = base.getResource("templates/default.mail")
	moreKeys = dict(p.split("=", 1) for p in args["options"])
	moreKeys["conn"] = conn
	moreKeys["SendNoMail"] = mailutils.SendNoMail

	mailutils.sendMail(args["template"], moreKeys, globals(), overrideDest)


class Dumping(object):
	"""is a namespace of all functions related to "new" dumps.

	I should use a module for that.  I will, if it gets more complex.

	The "new" dump works by inspecting a "dumpDir" config item and using
	the last file in there to produce a time stamp.   A new file is allocated,
	and an event list containing events changed since the time stamp
	is returned together with it.

	While there's a slight race in here, it should do a simple backup/logging
	mechanism.
	"""
	@staticmethod
	def getDumpFile(dumpDir):
		destFName = os.path.join(dumpDir, base.now().isoformat())
		tmpName = os.path.join(dumpDir, "newDump")
		open(tmpName, "w").close()
		os.rename(tmpName, destFName) # bail if someone else already writes there
		return open(destFName, "wb")

	@staticmethod
	def getLastDate(conn):
		newLastDD = time.time()
		try:
			lastDD = conn.stconf.lastDumpStamp
		except AttributeError:
			lastDD = 0
		conn.stconf.setKeyValue("lastDumpStamp", newLastDD)
		return lastDD

	@staticmethod
	def get(conn):
		try:
			dumpDir = conn.stconf.dumpDir
		except AttributeError:
			raise base.ReportableError("No dumpDir defined, cannot dump new.")
		if not os.path.exists(dumpDir):
			os.makedirs(dumpDir)
		dumpFile = Dumping.getDumpFile(dumpDir)
		lastDate = Dumping.getLastDate(conn)
		evList = model.EventList.fromQuery(conn,
			"SELECT * FROM events WHERE lastChanged>=?", [lastDate],
			evClass=model.TaggedEvent)
		return evList, dumpFile


@arguments(
	OptionalArg("what", "leave out for all, 'new', or 'archive",
		lambda arg: arg in set(["new", "archive"])))
def cmd_dump(conn, args):
	""" -- writes XML for events
	"""
# This has two quite different functions -- take them apart?
	outFile = sys.stdout.buffer
	if args["what"]=="archive":
		evList = model.EventList.fromQuery(conn.getArchiveConnection(),
			"SELECT * FROM events", [], evClass=model.TaggedEvent)
	elif args["what"] is None:
		evList = model.EventList.fromQuery(conn, "SELECT * FROM events", [],
			evClass=model.TaggedEvent)
	elif args["what"]=="new":
		evList, outFile = Dumping.get(conn)
	model.dump(evList, outFile)
	outFile.write(b"\n")


@arguments(Arg("source", "File to read from, - for stdin"))
def cmd_load(conn, args):
	""" -- reads XML serialized events from a file.
	"""
	inName = args["source"]
	if inName=='-':
		inFile = sys.stdin
	else:
		inFile = open(inName)
	evList = model.parseEventList(inFile, evClass=model.TaggedEvent)
	cursor = conn.cursor()
	for ev in evList:
		if ev.id is base.Undefined:
			ev.id = 0
		if ev.sponsor is base.Undefined:
			ev.sponsor = "operator"
		while True:
			try:
				ev.save(cursor)
			except model.IntegrityError:
				ev.id = random.randint(500000000, 1000000000)
				continue
			except base.InvalidEvent:
				pass
			break


@arguments()
def cmd_reindex(conn, args):
		""" -- re-creates the full-text index.
		"""
		statements = [
			"INSERT INTO event_index (event_index) VALUES('delete-all')",	
			"INSERT INTO event_index (rowid, teaser, place, fulltext, bdate)"
				" SELECT id, teaser, place, fulltext, bdate FROM events"]
		for c in [conn, conn.getArchiveConnection()]:
			for s in statements:
				c.execute(s)
			c.commit()


@arguments()
def cmd_archive(conn, args):
	""" -- archives expired events.
	"""
	model.archive(conn)
	model.weed(conn)
	conn.commit()
	if conn.stconf.getConfigs("serverPID"):
		cmd_reload(conn, ())

	# it turns out that fts5 is rather fragile (or is just hard to get
	# right).  Whatever it is, while I'm here I'm checking.
	conn.execute(
		"INSERT INTO event_index(event_index, rank) VALUES('integrity-check', 1)")
	conn.getArchiveConnection().execute(
		"INSERT INTO event_index(event_index, rank) VALUES('integrity-check', 1)")


@arguments(Arg("plugName", "Plugin name"),
	RemainingArgs("pluginArgs", "plugin specific arguments"))
def cmd_plugin(conn, args):
	"""executes a plugin's main method.
	"""
	from sofoterm.pages import plugin
	plugNS = plugin.loadPluginByName(args["plugName"]+".py")
	plugNS.main(conn, args["pluginArgs"])

############################ helpers

def daemonize(logFile, callable):
	# We translate TERMs to INTs to ensure finally: code is executed
	signal.signal(signal.SIGTERM,
		lambda a,b: os.kill(os.getpid(), signal.SIGINT))
	pid = os.fork()
	if pid == 0:
		os.setsid()
		pid = os.fork()
		if pid==0:
			os.close(0)
			os.close(1)
			os.close(2)
			os.dup(logFile.fileno())
			os.dup(logFile.fileno())
			os.dup(logFile.fileno())
			callable()
		else:
			os._exit(0)
	else:
		os._exit(0)


def bailOnExc(opts, msg):
	import traceback
	if opts.dumpExc:
		traceback.print_exc()
	sys.stderr.write(textwrap.fill(msg, replace_whitespace=True,
		initial_indent='', subsequent_indent="  ")+"\n")
	sys.exit(1)


def parseArgs():
	opts, args = _PARSER.parse_args()
	if not args:
		_PARSER.print_help(file=sys.stderr)
		sys.exit(1)
	return opts, args[0], args[1:]


def _makeParser():
	from optparse import OptionParser
	parser = OptionParser("%prog <action> {<action-args>}\nUse help to"
		" see actions available.")
	parser.add_option("-e", "--dump-exception", help="Dump exceptions.",
		dest="dumpExc", default=False, action="store_true")
	return parser

_PARSER = _makeParser()


def realMain(conn):
	opts, cmd, args = parseArgs()
	try:
		worker = globals()["cmd_"+cmd]
	except KeyError:
		bailOnExc(opts, "Unknown command: %s."%cmd)
	worker(conn, parseArgumentsFor(worker, args))


def testMain():
	# run on a test server -- for testing exclusively, no state is preserved
	sys.path.append("../tests")  # ok, use inspect as necessary
	import res_testserver
	realMain(res_testserver.SERVER.conn)


def main():
	dsn = base.getDSN()
	conn = model.getConnection(dsn)
	realMain(conn)
	conn.commit()


if __name__=="__main__":
	testMain()
