from setuptools import setup, find_packages

install_requires = []

setup(name="sofoterm",
	description="package for running sofo-hd event management",
	url="http://www.tifu.de",
	license="GPL",
	author="Markus Demleitner",
	author_email="m@tfiu.de",
	packages=find_packages(),
	py_modules=["ez_setup"],
	include_package_data = True,
	install_requires=install_requires,
	entry_points={
		"console_scripts":
			["sofoterm=sofoterm.cli:main"],
	},
	version="1.0")
