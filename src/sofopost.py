#!/usr/bin/python
"""
A bulk poster for sofoterm.

Input format:

one-line tags: Datum:Zeit:Ort:Titel:Tags:Attach: (case-insensitve).

long text is expected in {{{...}}}

Tags are blank-separated.

Multiple events are separated by at least 10 closing parens.

You need to set the SOFO_COOKIE env var to your sponsor key to make this
work (encode in utf-8 if necessary).
"""

import base64
import os
import re
import sys
import webbrowser

import requests


#BASE_URL = "http://localhost:8888"
BASE_URL = "http://sofo-hd.de"

class ReportableError(Exception):
	pass


def iter_raw_records(in_file):
	"""yields "records" from in_file.

	A "record" is a chunk of material that's delimited by a sequence
	of closing parens at least 10 chars long.
	"""
	return re.split(r"\){10,}", in_file.read())


ITEM_PAT = re.compile(
	"(?P<k>datum|zeit|ort|titel|tags|attach):(?P<v>[^\n]*)"
	"|(?P<l>\{\{\{)(?P<f>.*)\}\}\}", re.I|re.S)
LABEL_MAP = {
	"datum": "bdate",
	"zeit": "btime",
	"titel": "teaser",
	"ort": "place",
	"tags": "tags",
	"attach": "attach"}
MANDATORY_LABELS = frozenset(["bdate", "teaser"])
RECOMMENDED_LABELS = frozenset(["btime", "place", "fulltext"])


def parse_record(raw_rec, rec_no):
	"""returns a dictionary of parsed items from a raw record.

	The markup is discussed in the module docstring.
	"""
	rec = {}
	for mat in ITEM_PAT.finditer(raw_rec):
		if mat.group("l")=="{{{":
			rec["fulltext"] = mat.group("f").strip()
		else:
			label = mat.group("k").lower()
			if not label in LABEL_MAP:
				raise ReportableError("Unknown label %s"%label)
			rec[LABEL_MAP[label]] = mat.group("v").strip()

	if MANDATORY_LABELS-set(rec):
		raise ReportableError(
			"Mandatory items missing on record {}: {}".format(
				rec_no+1, " ".join(MANDATORY_LABELS-set(rec))))
	
	return rec
			

def upload_one(record):
	"""uploads a single record to what's defined in API_URL.

	tags are parsed and brought into shape before formatting the
	upload payload.
	"""
	record = record.copy()
	record["__nevow_form__"] = "termedit"

	if ',' in record["tags"]:
		raise ReportableError("tags are blank-separated")

	attachment = None
	if "attach" in record:
		attachment = record.pop("attach").strip()
		if not os.path.isfile(attachment):
			raise ReportableError("Attachment %s is no file"%attachment)

	record["tags"] = record["tags"].split()
	cookies = {"sponsor": base64.b64encode(
		os.environ["SOFO_COOKIE"].encode("utf-8")).decode("ascii")}
	resp = requests.get(BASE_URL+"/edit", cookies=cookies)
	event_url = resp.url

	resp = requests.post(event_url, data=record, cookies=cookies)
	resp.raise_for_status()
	# TODO: noch nach <div class="errors" suchen (zB. kaputter volltext)

	if attachment:
		payload = {
			"__q": "attach",
			"_charset_": ""}
		with open(attachment, "rb") as f:
			resp = requests.post(event_url,
				data=payload,
				files = {
					"upload": f},
				cookies=cookies)
		resp.raise_for_status()

	webbrowser.open(BASE_URL+"/event/"+event_url.split("/")[-1])


def main():
	if len(sys.argv)==1:
		in_file = sys.stdin
	elif len(sys.argv)==2:
		in_file = open(sys.argv[1])
	else:
		raise ReportableError(
			"Usage: %s [input file] -- upload light markup to a sofoterm site.\n"%
			sys.argv[0])
	
	to_upload = []
	for rec_no, raw_record in enumerate(iter_raw_records(in_file)):
		to_upload.append(parse_record(raw_record, rec_no))

	all_fine = True
	for parsed_record in to_upload:
		if RECOMMENDED_LABELS-set(parsed_record):
			sys.stderr.write(
				"Recommended items missing on record '{}': {}\n".format(
					parsed_record["teaser"],
					" ".join(RECOMMENDED_LABELS-set(parsed_record))))
			all_fine = False
		
	
	if not all_fine:
		while True:
			res = input("Upload anyway (y/n)?")
			if res in "yn":
				break
		if not res=="y":
			sys.stderr.write("Aborting all uploads.\n")
			return

	for parsed_record in to_upload:
		upload_one(parsed_record)


if __name__=="__main__":
	try:
		main()
	except ReportableError as exc:
		sys.stderr.write(str(exc)+"\n")
		sys.exit(1)
