#!/usr/bin/python3



from html.parser import HTMLParser
import os
import sys
from os.path import join, getsize
import re

classes = []
tags = []
pseudoclasses = ["before", "after", "active", "focus", "focus-within", "disabled", "not"]
attags = ["media", "-webkit-keyframes", "keyframes", "supports", "page"]

class MyHTMLParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        tags.append(tag)
        for attr in attrs:
            if attr[0] == "class":
                for cls in attr[1].split():
                    classes.append(cls)


for root, dirs, files in os.walk('../sofoterm'):
    for filename in files:
        print((root + os.sep + filename))
        if filename.endswith("html"):
            parser = MyHTMLParser()
            parser.feed(open(root + os.sep + filename).read())

classes = sorted(classes)
tags = sorted(tags)

fragments = classes + tags + pseudoclasses + attags

print(("Found fragments: ", fragments))

cssfile = sys.argv[1]

csscode = open(cssfile).read()
strippedCssCode = ""

def evaluateSelector(selectorstr):
    oneSelectorMatches = False
    selectors = selectorstr.split(',')
    for selector in selectors:
        selectorMatches = True
        list = re.findall('[\w-]+', selector)
        for selectorpart in list:
            if not selectorpart in fragments:
                selectorMatches = False
        if selectorMatches:
            oneSelectorMatches = True
            break
    print(f"Evaluating '{selectorstr}': {oneSelectorMatches}")
    return oneSelectorMatches


def stripCss(code):
    selectorstr = ""
    skip = False
    curIndex = 0
    sectionBeginIndex = 0
    sectionEndIndex = 0
    includeSection = False
    braceDepth = 0
    strippedCode = ""
    isAtSelector = False
    possibleCommentStart = False
    possibleCommentEnd = False
    for char in code:
        curIndex = curIndex + 1 # already one further now
        if possibleCommentStart and char != '*':
            possibleCommentStart = False
        if possibleCommentEnd and char != '/':
            possibleCommentEnd = False
        if char == '}':
            skip = skip - 1
            if skip > 0:
                continue
            if isAtSelector:
                strippedInner = stripCss(code[sectionBeginIndex:curIndex - 1]) + '}'
                print(f"Including: `{strippedInner}`")
                strippedCode += strippedInner
                isAtSelector = False
            elif includeSection:
                print(f"Including: `{code[sectionBeginIndex:curIndex]}`")
                strippedCode += code[sectionBeginIndex:curIndex]
            else:
                print(f"Excluding: `{code[sectionBeginIndex:curIndex]}`")
            sectionBeginIndex = curIndex
            continue
        if char == '(':
            skip = skip + 1
            continue
        if char == ')':
            skip = skip - 1
            continue
        if char == '{':
            skip = skip + 1
            if skip > 1:
                continue
            if isAtSelector:
                print(f"Including: `{code[sectionBeginIndex:curIndex]}`")
                strippedCode += code[sectionBeginIndex:curIndex]
                sectionBeginIndex = curIndex
            else:
                includeSection = evaluateSelector(selectorstr)
            selectorstr = ""
            continue
        if char == '*':
            if possibleCommentStart:
                sectionBeginIndex = curIndex - 2
                possibleCommentStart = False
                skip = skip + 1
            else:
                possibleCommentEnd = True
        if char == '/':
            if possibleCommentEnd:
                strippedCode += code[sectionBeginIndex:curIndex]
                sectionBeginIndex = curIndex
                possibleCommentEnd = False
                skip = skip - 1
            else:
                possibleCommentStart = True
        if skip > 0:
            continue
        if char == '@':
            isAtSelector = True
        else:
            selectorstr += char
    return strippedCode

strippedCssCode = stripCss(csscode)

open(cssfile[0:-4] + ".stripped.css", 'w').write(strippedCssCode)
